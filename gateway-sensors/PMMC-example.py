#!/usr/bin/python3
# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright 2022 Raritan Inc. All rights reserved.

import copy, sys, os
sys.path.append(os.path.dirname(os.path.abspath(__file__)) + "/pdu-python-api")
from raritan.rpc import Agent, sensors
from raritan.rpc.peripheral import DeviceManager, GatewaySensorManager, ModbusCfg

ip = "10.0.42.2"
user = "admin"
pw = "raritan"

try:
    ip = sys.argv[1]
    user = sys.argv[2]
    pw = sys.argv[3]
except IndexError:
    pass # use defaults

agent = Agent("https", ip, user, pw, disable_certificate_verification=True)
mgr = DeviceManager("/model/peripheraldevicemanager", agent)
gwcfg = mgr.getGatewaySensorManager()

ClassTemperature = GatewaySensorManager.NumericSensorClass(
    classId = "Temperature",
    metadata = sensors.NumericSensor.MetaData(
        type = sensors.Sensor.TypeSpec(
            sensors.Sensor.NUMERIC,
            sensors.Sensor.TEMPERATURE,
            sensors.Sensor.DEGREE_CELSIUS
        ),
        decdigits = 1,
        accuracy = 0.0,
        resolution = 1.0,
        tolerance = 0,
        noiseThreshold = 1,
        range = sensors.NumericSensor.Range(0, 32767),
        thresholdCaps = sensors.NumericSensor.ThresholdCapabilities(True, True, True, True),
    ),
    defaultThresholds = sensors.NumericSensor.Thresholds(
        upperCriticalActive = True,
        upperCritical = 45.0,
        upperWarningActive = True,
        upperWarning = 32.0,
        lowerWarningActive = True,
        lowerWarning = 10.0,
        lowerCriticalActive = True,
        lowerCritical = 5.0,
        assertionTimeout = 3,
        deassertionHysteresis = 2.0
    ),
    preferCommonThresholds = False
)

RemoteDevicePMMC = GatewaySensorManager.RemoteModbusRTUDevice(
    deviceId = "PMMC",
    disabled = False,
    name = "PMMC PDU",
    timeoutMs = 100,
    retry = 0, # use default
    detectionIdentifiers = {
        GatewaySensorManager.MODBUS_VENDOR_NAME: "Raritan",
        GatewaySensorManager.MODBUS_PRODUCT_CODE: "iPDU"
    },
    busInterface = "sensorhub0-rs485",
    busSettings = ModbusCfg.SerialSettings(
        baud = 38400,
        parity = ModbusCfg.SerialSettings.Parity.NONE,
        dataBits = 8,
        stopBits = 1
    ),
    unitId = 247,
    interframeDelayDeciChars = 0, # use default
)

Float = GatewaySensorManager.ModbusValueEncoding32(
    encodingId = "FLOAT",
    type = GatewaySensorManager.EncodingType.IEEE754,
    invertState = False,
    interpretationRules = [
        GatewaySensorManager.InterpretationRuleIEEE754NAN(
            interpretation = GatewaySensorManager.Interpretation.UNAVAILABLE,
            ignoreTimeout = 0,
            invertCondition = False
        ),
        GatewaySensorManager.InterpretationRuleIEEE754INF(
            interpretation = GatewaySensorManager.Interpretation.NUMERIC_INVALID,
            ignoreTimeout = 0,
            invertCondition = False
        ),
    ],
    scalingFactor = 1.0,
    offset = 0.0,
    byteSwap = False,
    mask = 0, # not masked
    endianness = GatewaySensorManager.ModbusEndianness.MODBUS_BIG_ENDIAN
)

FloatAlt = copy.deepcopy(Float)
FloatAlt.encodingId = "FLOAT/ALT"
FloatAlt.interpretationRules += [
    GatewaySensorManager.InterpretationRuleModbusException(
        interpretation = GatewaySensorManager.Interpretation.UNAVAILABLE,
        ignoreTimeout = 0,
        invertCondition = False,
        exceptions = [ ModbusCfg.EXCEPTION_ILLEGAL_DATA_ADDRESS ]
    ),
]
FloatAlt.offset = 20.0

sensor1 = GatewaySensorManager.ModbusSensor(
    sensorId = "",
    disabled = False,
    deviceId = "PMMC",
    classId = "Temperature",
    encodingId = "FLOAT",
    defaultName = "Temperature 1",
    function = ModbusCfg.ModbusFunction.HOLDING_REGISTER,
    regAddr = 2050
)

sensor2 = GatewaySensorManager.ModbusSensor(
    sensorId = "",
    disabled = False,
    deviceId = "PMMC",
    classId = "Temperature",
    encodingId = "FLOAT",
    defaultName = "Temperature 2",
    function = ModbusCfg.ModbusFunction.HOLDING_REGISTER,
    regAddr = 2066
)

sensor3 = GatewaySensorManager.ModbusSensor(
    sensorId = "",
    disabled = False,
    deviceId = "PMMC",
    classId = "Temperature",
    encodingId = "FLOAT/ALT",
    defaultName = "<ILLEGAL_DATA_ADDRESS>",
    function = ModbusCfg.ModbusFunction.HOLDING_REGISTER,
    regAddr = 0x9999  # illegal address
)

cfg = gwcfg.getConfiguration()

cfg["PMMC"] = GatewaySensorManager.ConfigurationPackage(
    name = "PMMC",
    classes = [ ClassTemperature ],
    devices = [ RemoteDevicePMMC ],
    encodings = [ Float, FloatAlt ],
    sensors = [ sensor1, sensor2, sensor3 ]
)

ret = gwcfg.setConfiguration(cfg)

match ret:
    case GatewaySensorManager.ERR_CONFIG_INCONSISTENT:
        print("FAILED: Consistency check has failed (e.g. non-unique identifiers)")

    case GatewaySensorManager.ERR_CONFIG_STORAGE_FAILED:
        print("FAILED: Configuration cannot be stored.")

    case _:
        print("Configuration stored")

