#!/usr/bin/python3
# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright 2022 Raritan Inc. All rights reserved.

# see PM710_Public_Register_List_v3.150.pdf
# https://ckm-content.se.com/ckmContent/sfc/servlet.shepherd/document/download/0691H00000EFTDcQAP
# !!! documentation counts 1-based while PM710 modbus address counts 0-based !!!!


import copy, sys, os
sys.path.append(os.path.dirname(os.path.abspath(__file__)) + "/pdu-python-api")
from raritan.rpc import Agent, sensors
from raritan.rpc.peripheral import DeviceManager, GatewaySensorManager, ModbusCfg

ip = "10.0.42.2"
user = "admin"
pw = "raritan"

try:
    ip = sys.argv[1]
    user = sys.argv[2]
    pw = sys.argv[3]
except IndexError:
    pass # use defaults

agent = Agent("https", ip, user, pw, disable_certificate_verification=True)
mgr = DeviceManager("/model/peripheraldevicemanager", agent)
gwcfg = mgr.getGatewaySensorManager()

ClassVoltage = GatewaySensorManager.NumericSensorClass(
    classId = "Voltage",
    metadata = sensors.NumericSensor.MetaData(
        type = sensors.Sensor.TypeSpec(
            sensors.Sensor.NUMERIC,
            sensors.Sensor.VOLTAGE,
            sensors.Sensor.VOLT
        ),
        decdigits = 1,
        accuracy = 0.0,
        resolution = 1.0,
        tolerance = 0,
        noiseThreshold = 1,
        range = sensors.NumericSensor.Range(0, 32767),
        thresholdCaps = sensors.NumericSensor.ThresholdCapabilities(True, True, True, True),
    ),
)


ClassFrequency = GatewaySensorManager.NumericSensorClass(
    classId = "Frequency",
    metadata = sensors.NumericSensor.MetaData(
        type = sensors.Sensor.TypeSpec(
            sensors.Sensor.NUMERIC,
            sensors.Sensor.FREQUENCY,
            sensors.Sensor.HZ
        ),
        decdigits = 2,
        accuracy = 0.0,
        resolution = 0.01,
        tolerance = 0,
        noiseThreshold = 0.01,
        range = sensors.NumericSensor.Range(45, 65),
        thresholdCaps = sensors.NumericSensor.ThresholdCapabilities(True, True, True, True),
    ),
)

ClassPower = GatewaySensorManager.NumericSensorClass(
    classId = "Power",
    metadata = sensors.NumericSensor.MetaData(
        type = sensors.Sensor.TypeSpec(
            sensors.Sensor.NUMERIC,
            sensors.Sensor.POWER,
            sensors.Sensor.WATT
        ),
        decdigits = 2,
        accuracy = 0.0,
        resolution = 0.01,
        tolerance = 0,
        noiseThreshold = 0.01,
        range = sensors.NumericSensor.Range(0, 1e10),
        thresholdCaps = sensors.NumericSensor.ThresholdCapabilities(True, True, True, True),
    ),
)

ClassActiveEnergyInt = GatewaySensorManager.NumericSensorClass(
    classId = "ActiveEnergyInt",
    metadata = sensors.NumericSensor.MetaData(
        type = sensors.Sensor.TypeSpec(
            sensors.Sensor.NUMERIC,
            sensors.Sensor.ENERGY,
            sensors.Sensor.WATT_HOUR
        ),
        decdigits = 0,
        accuracy = 0.0,
        resolution = 1,
        tolerance = 0,
        noiseThreshold = 1,
        range = sensors.NumericSensor.Range(0, 1e10),
    ),
)

ClassApparentEnergyInt = copy.deepcopy(ClassActiveEnergyInt)
ClassApparentEnergyInt.classId = "ApparentEnergyInt"
ClassApparentEnergyInt.metadata.type = sensors.Sensor.TypeSpec(
        sensors.Sensor.NUMERIC,
        sensors.Sensor.ENERGY,
        sensors.Sensor.VOLT_AMP_HOUR
)

ClassReactiveEnergyInt = copy.deepcopy(ClassActiveEnergyInt)
ClassReactiveEnergyInt.classId = "ReactiveEnergyInt"
ClassReactiveEnergyInt.metadata.type = sensors.Sensor.TypeSpec(
        sensors.Sensor.NUMERIC,
        sensors.Sensor.ENERGY,
        sensors.Sensor.VOLT_AMP_REACTIVE_HOUR
)

ClassActiveEnergy = GatewaySensorManager.NumericSensorClass(
    classId = "ActiveEnergy",
    metadata = sensors.NumericSensor.MetaData(
        type = sensors.Sensor.TypeSpec(
            sensors.Sensor.NUMERIC,
            sensors.Sensor.ENERGY,
            sensors.Sensor.WATT_HOUR
        ),
        decdigits = 3,
        accuracy = 0.0,
        resolution = 0.001,
        tolerance = 0,
        noiseThreshold = 1,
        range = sensors.NumericSensor.Range(0, 1e10),
    ),
)

ClassApparentEnergy = copy.deepcopy(ClassActiveEnergy)
ClassApparentEnergy.classId = "ApparentEnergy"
ClassApparentEnergy.metadata.type = sensors.Sensor.TypeSpec(
        sensors.Sensor.NUMERIC,
        sensors.Sensor.ENERGY,
        sensors.Sensor.VOLT_AMP_HOUR
)

ClassReactiveEnergy = copy.deepcopy(ClassActiveEnergy)
ClassReactiveEnergy.classId = "ReactiveEnergy"
ClassReactiveEnergy.metadata.type = sensors.Sensor.TypeSpec(
        sensors.Sensor.NUMERIC,
        sensors.Sensor.ENERGY,
        sensors.Sensor.VOLT_AMP_REACTIVE_HOUR
)

RemoteDevice = GatewaySensorManager.RemoteModbusRTUDevice(
    deviceId = "DEV",
    name = "Schneider PM-710",
    detectionIdentifiers = {
        GatewaySensorManager.MODBUS_VENDOR_NAME: "Schneider Electric",
        GatewaySensorManager.MODBUS_PRODUCT_CODE: "PM710",
        GatewaySensorManager.MODBUS_REVISION: "V03.150"
    },
    busInterface = "USB-1",
    busSettings = ModbusCfg.SerialSettings(
        baud = 19200,
        parity = ModbusCfg.SerialSettings.Parity.EVEN,
        dataBits = 8,
        stopBits = 1
    ),
    unitId = 1,
    interframeDelayDeciChars = 0, # use default
)

ULong = GatewaySensorManager.ModbusValueEncoding32(
    encodingId = "ULong",
    type = GatewaySensorManager.EncodingType.UINT,
)

Long = GatewaySensorManager.ModbusValueEncoding32(
    encodingId = "Long",
    type = GatewaySensorManager.EncodingType.INT,
)

Float = GatewaySensorManager.ModbusValueEncoding32(
    encodingId = "Float",
    type = GatewaySensorManager.EncodingType.IEEE754,
)

Float1000 = GatewaySensorManager.ModbusValueEncoding32(
    encodingId = "Float1000",
    type = GatewaySensorManager.EncodingType.IEEE754,
    scalingFactor = 1000
)

Integer = GatewaySensorManager.ModbusValueEncoding16(
    encodingId = "Integer",
    type = GatewaySensorManager.EncodingType.INT,
)

Integer10 = GatewaySensorManager.ModbusValueEncoding16(
    encodingId = "Integer10",
    type = GatewaySensorManager.EncodingType.INT,
    scalingFactor = 0.1,
)

Integer100 = GatewaySensorManager.ModbusValueEncoding16(
    encodingId = "Integer100",
    type = GatewaySensorManager.EncodingType.INT,
    scalingFactor = 0.01,
)

S4011 = GatewaySensorManager.ModbusSensor(
    sensorId = "S4011",
    deviceId = "DEV",
    classId = "Voltage",
    encodingId = "Integer10",
    defaultName = "Voltage, L-N, 3P Average",
    function = ModbusCfg.ModbusFunction.HOLDING_REGISTER,
    regAddr = 4010
)

S4013 = GatewaySensorManager.ModbusSensor(
    sensorId = "S4013",
    deviceId = "DEV",
    classId = "Frequency",
    encodingId = "Integer100",
    defaultName = "Frequency",
    function = ModbusCfg.ModbusFunction.HOLDING_REGISTER,
    regAddr = 4012
)

S1022 = GatewaySensorManager.ModbusSensor(
    sensorId = "S1022",
    deviceId = "DEV",
    classId = "Power",
    encodingId = "Float",
    defaultName = "Active Power, Total Demand Present",
    function = ModbusCfg.ModbusFunction.HOLDING_REGISTER,
    regAddr = 1021
)

S1172 = GatewaySensorManager.ModbusSensor(
    sensorId = "S1172",
    deviceId = "DEV",
    classId = "Voltage",
    encodingId = "Float",
    defaultName = "Voltage A-N Maximum",
    function = ModbusCfg.ModbusFunction.HOLDING_REGISTER,
    regAddr = 1171
)

S4000 = GatewaySensorManager.ModbusSensor(
    sensorId = "S4000",
    deviceId = "DEV",
    classId = "ActiveEnergyInt",
    encodingId = "Long",
    defaultName = "Active Energy, Total [from Integer]",
    function = ModbusCfg.ModbusFunction.HOLDING_REGISTER,
    regAddr = 3999
)

S4002 = GatewaySensorManager.ModbusSensor(
    sensorId = "S4002",
    deviceId = "DEV",
    classId = "ApparentEnergyInt",
    encodingId = "ULong",
    defaultName = "Apparent Energy, Total [from Integer]",
    function = ModbusCfg.ModbusFunction.HOLDING_REGISTER,
    regAddr = 4001
)

S4004 = GatewaySensorManager.ModbusSensor(
    sensorId = "S4004",
    deviceId = "DEV",
    classId = "ReactiveEnergyInt",
    encodingId = "Long",
    defaultName = "Reactive Energy, Total [from Integer]",
    function = ModbusCfg.ModbusFunction.HOLDING_REGISTER,
    regAddr = 4003
)

S1000 = GatewaySensorManager.ModbusSensor(
    sensorId = "S1000",
    deviceId = "DEV",
    classId = "ActiveEnergy",
    encodingId = "Float1000",
    defaultName = "Active Energy, Total [from Float]",
    function = ModbusCfg.ModbusFunction.HOLDING_REGISTER,
    regAddr = 999
)

S1002 = GatewaySensorManager.ModbusSensor(
    sensorId = "S1002",
    deviceId = "DEV",
    classId = "ApparentEnergy",
    encodingId = "Float1000",
    defaultName = "Apparent Energy, Total [from Float]",
    function = ModbusCfg.ModbusFunction.HOLDING_REGISTER,
    regAddr = 1001
)

S1004 = GatewaySensorManager.ModbusSensor(
    sensorId = "S1004",
    deviceId = "DEV",
    classId = "ReactiveEnergy",
    encodingId = "Float1000",
    defaultName = "Reactive Energy, Total [from Float]",
    function = ModbusCfg.ModbusFunction.HOLDING_REGISTER,
    regAddr = 1003
)

cfg = gwcfg.getConfiguration()

cfg["PM710"] = GatewaySensorManager.ConfigurationPackage(
    name = "Schneider PM-710",
    classes = [ ClassVoltage, ClassFrequency, ClassPower, ClassActiveEnergyInt, ClassApparentEnergyInt, ClassReactiveEnergyInt, ClassActiveEnergy, ClassApparentEnergy, ClassReactiveEnergy ],
    devices = [ RemoteDevice ],
    encodings = [ Float, Float1000, ULong, Long, Integer, Integer10, Integer100 ],
    sensors = [ S4011, S4013, S1022, S1172, S1000, S1002, S1004, S4000, S4002, S4004 ]
)

ret = gwcfg.setConfiguration(cfg)

match ret:
    case GatewaySensorManager.ERR_CONFIG_INCONSISTENT:
        print("FAILED: Consistency check has failed (e.g. non-unique identifiers)")

    case GatewaySensorManager.ERR_CONFIG_STORAGE_FAILED:
        print("FAILED: Configuration cannot be stored.")

    case _:
        print("Configuration stored")
