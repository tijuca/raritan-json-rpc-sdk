var searchData=
[
  ['x_0',['x',['../structperipheral_1_1DeviceSlot_1_1Location.html#a016c83cfc8d5dfa0b2a39ce9a718b9dd',1,'peripheral::DeviceSlot::Location::x'],['../structwebcam_1_1Location.html#aa8ddc32bdf07e95df7ada4296518b85f',1,'webcam::Location::x']]],
  ['xerus™_20idl_20to_20json_20rpc_1',['Rules and Mechanism for Mapping Xerus™-IDL to JSON-RPC',['../md__2home_2builds_2gitlab_2builds_2kACs-oxc_20_2gitlab_2main_2firmware_2mkdist_2tmp_2build-pdu-p7b2bc9145c9544e24bd4dfc1acb3fa50.html',1,'']]],
  ['xerus™_20json_20rpc_20software_20development_20kit_2',['Xerus™ JSON-RPC Software Development Kit',['../index.html',1,'']]],
  ['xor_3',['XOR',['../structevent_1_1Engine_1_1Condition.html#a5f4b09b572315fa67a3f418f9ae7eea3ab3cc7e758ef4dd946eedd55a69aa0d8b',1,'event::Engine::Condition']]]
];
