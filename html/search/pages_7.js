var searchData=
[
  ['java_20json_20rpc_20client_20binding_0',['Java JSON-RPC Client Binding',['../md__2home_2builds_2gitlab_2builds_2kACs-oxc_20_2gitlab_2main_2firmware_2mkdist_2tmp_2build-pdu-p5300d4878a92dcab9e72a582699aaedd.html',1,'']]],
  ['json_20encoded_20datapush_20message_1',['Example of JSON-encoded Datapush message',['../md__2home_2builds_2gitlab_2builds_2kACs-oxc_20_2gitlab_2main_2firmware_2mkdist_2tmp_2build-pdu-p9996b19c7ad8aa9810e565e32f6dd554.html',1,'']]],
  ['json_20rpc_2',['Rules and Mechanism for Mapping Xerus™-IDL to JSON-RPC',['../md__2home_2builds_2gitlab_2builds_2kACs-oxc_20_2gitlab_2main_2firmware_2mkdist_2tmp_2build-pdu-p7b2bc9145c9544e24bd4dfc1acb3fa50.html',1,'']]],
  ['json_20rpc_20client_20binding_3',['JSON RPC Client Binding',['../md__2home_2builds_2gitlab_2builds_2kACs-oxc_20_2gitlab_2main_2firmware_2mkdist_2tmp_2build-pdu-p2dbdbae5c2d07cd754f8c75fbb4ce0c6.html',1,'C# / .NET JSON-RPC Client Binding'],['../md__2home_2builds_2gitlab_2builds_2kACs-oxc_20_2gitlab_2main_2firmware_2mkdist_2tmp_2build-pdu-p5300d4878a92dcab9e72a582699aaedd.html',1,'Java JSON-RPC Client Binding'],['../md__2home_2builds_2gitlab_2builds_2kACs-oxc_20_2gitlab_2main_2firmware_2mkdist_2tmp_2build-pdu-p2a4354f5553220dc808cca01b0002f34.html',1,'Perl JSON-RPC Client Binding'],['../md__2home_2builds_2gitlab_2builds_2kACs-oxc_20_2gitlab_2main_2firmware_2mkdist_2tmp_2build-pdu-pfcbab061e3a9385299ee360adb881ed4.html',1,'Python JSON-RPC Client Binding']]],
  ['json_20rpc_20example_4',['Curl JSON-RPC Example',['../md__2home_2builds_2gitlab_2builds_2kACs-oxc_20_2gitlab_2main_2firmware_2mkdist_2tmp_2build-pdu-p747626c7ac2fa0a94b2631aac115977b.html',1,'']]],
  ['json_20rpc_20software_20development_20kit_5',['Xerus™ JSON-RPC Software Development Kit',['../index.html',1,'']]]
];
