var searchData=
[
  ['data_20model_0',['PDU Data Model',['../md__2home_2builds_2gitlab_2builds_2kACs-oxc_20_2gitlab_2main_2firmware_2mkdist_2tmp_2build-pdu-p99bacc19a0d618f4b8524ee5b5a1bd79.html',1,'']]],
  ['datapush_20message_1',['Example of JSON-encoded Datapush message',['../md__2home_2builds_2gitlab_2builds_2kACs-oxc_20_2gitlab_2main_2firmware_2mkdist_2tmp_2build-pdu-p9996b19c7ad8aa9810e565e32f6dd554.html',1,'']]],
  ['default_20credentials_2',['Default Credentials',['../md__2home_2builds_2gitlab_2builds_2kACs-oxc_20_2gitlab_2main_2firmware_2mkdist_2tmp_2build-pdu-p5f2e0fa7bcd25e2ac210632312ca600b.html',1,'']]],
  ['development_20kit_3',['Xerus™ JSON-RPC Software Development Kit',['../index.html',1,'']]],
  ['device_20settings_4',['Device Settings',['../md__2home_2builds_2gitlab_2builds_2kACs-oxc_20_2gitlab_2main_2firmware_2mkdist_2tmp_2build-pdu-p02e7a2cef278ce1843a989b8e03fc9ad.html',1,'']]],
  ['devices_5',['Peripheral Devices',['../md__2home_2builds_2gitlab_2builds_2kACs-oxc_20_2gitlab_2main_2firmware_2mkdist_2tmp_2build-pdu-pddeb4016cb8b7df927c1739b52dc052d.html',1,'']]]
];
