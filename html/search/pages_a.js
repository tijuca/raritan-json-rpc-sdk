var searchData=
[
  ['mapping_20xerus™_20idl_20to_20json_20rpc_0',['Rules and Mechanism for Mapping Xerus™-IDL to JSON-RPC',['../md__2home_2builds_2gitlab_2builds_2kACs-oxc_20_2gitlab_2main_2firmware_2mkdist_2tmp_2build-pdu-p7b2bc9145c9544e24bd4dfc1acb3fa50.html',1,'']]],
  ['mechanism_20for_20mapping_20xerus™_20idl_20to_20json_20rpc_1',['Rules and Mechanism for Mapping Xerus™-IDL to JSON-RPC',['../md__2home_2builds_2gitlab_2builds_2kACs-oxc_20_2gitlab_2main_2firmware_2mkdist_2tmp_2build-pdu-p7b2bc9145c9544e24bd4dfc1acb3fa50.html',1,'']]],
  ['message_2',['Example of JSON-encoded Datapush message',['../md__2home_2builds_2gitlab_2builds_2kACs-oxc_20_2gitlab_2main_2firmware_2mkdist_2tmp_2build-pdu-p9996b19c7ad8aa9810e565e32f6dd554.html',1,'']]],
  ['model_3',['PDU Data Model',['../md__2home_2builds_2gitlab_2builds_2kACs-oxc_20_2gitlab_2main_2firmware_2mkdist_2tmp_2build-pdu-p99bacc19a0d618f4b8524ee5b5a1bd79.html',1,'']]],
  ['monitoring_4',['Server Reachability Monitoring',['../md__2home_2builds_2gitlab_2builds_2kACs-oxc_20_2gitlab_2main_2firmware_2mkdist_2tmp_2build-pdu-p4f57fc28e91612542caeb81e3aeb57b9.html',1,'']]]
];
