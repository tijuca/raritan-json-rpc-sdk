/* SPDX-License-Identifier: BSD-3-Clause */
/*
 * Copyright 2020 Raritan Inc. All rights reserved.
 */

#ifndef __SMARTLOCK_KEYPAD_MANAGER_IDL__
#define __SMARTLOCK_KEYPAD_MANAGER_IDL__

#include <Keypad.idl>
#include <Event.idl>
#include <UserEvent.idl>

/** Keypad Manager */
module smartlock {

    /** Keypad Manager Interface */
    interface KeypadManager_1_0_1 {

        /*
         * Keypad setting
         */
        structure KeypadSettings {
            string name;                ///< User-defined name
            string description;         ///< User-defined description
        };

        /**
         * Keypad base event
         */
        valueobject KeypadEvent extends idl.Event {
            Keypad keypad;              ///< Affected keypad
            Keypad.MetaData metaData;   ///< MetaData of affected keypad
        };

        /**
         * Keypad attached event
         */
        valueobject KeypadAttachedEvent extends KeypadEvent {};

        /**
         * Keypad detached event
         */
        valueobject KeypadDetachedEvent extends KeypadEvent {};

        /*
         * Event: A keypad's settings have been changed
         */
        valueobject KeypadSettingsChangedEvent extends event.UserEvent {
            Keypad keypad;                      ///< affected keypad
            KeypadSettings oldSettings;         ///< Settings before change
            KeypadSettings newSettings;         ///< Settings after change
            string position;                    ///< Position of the keypad (see Keypad.MetaData)
        };

        /**
         * Retrieve the list of connected keypads.
         *
         * @return Keypads list
         */
        vector<Keypad> getKeypads();

        /**
         * Get keypad for a specific id.
         *
         * @param  keypadId     keypad id
         *
         * @return Keypad with given id or null
         */
        Keypad getKeypadById(in string keypadId);

        /**
         * Set settings for a keypad.
         *
         * @param position      position of keypad (see Keypad.MetaData)
         * @param settings      new settings for keypad
         *
         * @return NO_ERROR                     if OK
         * @return ERR_INVALID_PARAMS           if any setting is invalid
         */
        int setKeypadSettings(in string position, in KeypadSettings setting);

        /**
         * Get settings for all keypads.
         *
         * @return Map of settings by keypad position (see Keypad.MetaData)
         */
        map<string, KeypadSettings> getAllKeypadSettings();
    };

}

#endif /* __SMARTLOCK_KEYPAD_MANAGER_IDL__ */
