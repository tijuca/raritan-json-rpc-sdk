/* SPDX-License-Identifier: BSD-3-Clause */
/*
 * Copyright 2013 Raritan Inc. All rights reserved.
 */

#ifndef __PERIPHERAL_DEVICE_PACKAGE_IDL__
#define __PERIPHERAL_DEVICE_PACKAGE_IDL__

#include <PeripheralDeviceSlot.idl>

/** Peripheral Devices */
module peripheral {
    /** Peripheral device package information */
    structure PackageInfo_7_0_0 {
        enumeration State {
            NORMAL,         ///< Device package is in normal operation
            FW_UPDATE,      ///< Device package's firmware is being updated
            INTERNAL_ERROR, ///< Device package's internal error flag is set
            CONFIG_ERROR    ///< Device package's internal configuration is invalid
        };

        structure HardwareInfo {
            string  serial;                 ///< serial number
            string  packageClass;           ///< serial number prefix for current packages
            string  model;                  ///< like 'DPX-CC2' or 'DX-D2C6'
            int     minDowngradeVersion;    ///< minimum downgrade version (or -1)
            string  revision;               ///< hardware revision
            string  address;                ///< ROM code for 1-wire devices
        };

        structure FirmwareInfo {
            structure Version {
                int majorNumber;            ///< firmware version encoded as 'major << 4 | minor'
                int minorNumber;            ///< depreceated, was minor firmware version
                int bootloaderVersion;      ///< device bootloader version
            };

            time    compileDate;    ///< Date of firmware compilation (Deprecated: always returns 0)
            Version version;        ///< Firmware version (0.0 if not applicable)
            time    updateDate;     ///< Date of device firmware update (Deprecated: always returns 0)
            string  firmwareName;   ///< firmware file name
        };

        State               state;              ///< The peripheral device package operational state
        vector<PosElement_5_0_0> position;      ///< Position within 1-wire topo
        HardwareInfo        hwInfo;             ///< Device package hardware specific information
        FirmwareInfo        fwInfo;             ///< Device package firmware specific information
    };

    interface Package_3_0_3 {
        PackageInfo_7_0_0 getPackageInfo();
    };

    interface DoorHandleControllerPackage_3_0_3 extends Package_3_0_3 {

        constant int SUCCESS = 0;
        constant int ERR_NOT_SUPPORTED = 1;
        constant int ERR_OPERATION_FAILED = 2;

        vector<string> getSupportedHandleTypes();
        vector<string> getSupportedExternalDeviceTypes();

        string getHandleType(in int channel);
        string getExternalDeviceType(in int channel);

        int setHandleType(in int channel, in string handleType);
        int setExternalDeviceType(in int channel, in string type);

        /** Event: A handle was opened without being electronically unlocked */
        valueobject MechanicallyUnlockedEvent extends idl.Event {
            PackageInfo_7_0_0 packageInfo;  ///< package info
            int channel;                    ///< channel number (zero-based)
            string doorStateName;           ///< name of the door state sensor (if assigned)
            string doorHandleName;          ///< name of the door handle sensor (if assigned)
            string doorLockName;            ///< name of the door lock actuator (if assigned)
        };

        /** Event: A door was opened without unlocking the door handle */
        valueobject DoorForcedOpenEvent extends idl.Event {
            PackageInfo_7_0_0 packageInfo;  ///< package info
            int channel;                    ///< channel number (zero-based)
            string doorStateName;           ///< name of the door state sensor (if assigned)
            string doorHandleName;          ///< name of the door handle sensor (if assigned)
            string doorLockName;            ///< name of the door lock actuator (if assigned)
        };
    };

    interface BatteryPoweredDevicePackage_1_0_2 extends Package_3_0_3 {

        /**
        * This method outputs the given voltage value or zero if no value is available.
        * @return 0 or the voltage value in V
        */
        double getBatteryVoltage();

        /** Event: the voltage of this device changed */
        valueobject VoltageChangedEvent extends idl.Event {
            double oldVoltage;                     ///< old voltage
            double newVoltage;                     ///< new voltage
        };
    };
}

#endif /* !__PERIPHERAL_DEVICE_PACKAGE_IDL__ */
