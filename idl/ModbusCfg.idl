/* SPDX-License-Identifier: BSD-3-Clause */
/*
 * Copyright 2022 Raritan Inc. All rights reserved.
 */

#ifndef __PERIPHERAL_MODBUS_CFG_IDL__
#define __PERIPHERAL_MODBUS_CFG_IDL__

/**
 * Peripheral Devices
 */
module peripheral {
    interface ModbusCfg {
        structure SerialSettings {
            enumeration Parity { NONE, EVEN, ODD };
            int             baud;
            Parity          parity;
            int             dataBits;
            int             stopBits;
        };

        enumeration ModbusFunction {
            COIL,
            DISCRETE_INPUT,
            HOLDING_REGISTER,
            INPUT_REGISTER
        };

        constant int EXCEPTION_ILLEGAL_FUNCTION        = 1;
        constant int EXCEPTION_ILLEGAL_DATA_ADDRESS    = 2;
        constant int EXCEPTION_ILLEGAL_DATA_VALUE      = 3;
        constant int EXCEPTION_SLAVE_OR_SERVER_FAILURE = 4;
        constant int EXCEPTION_ACKNOWLEDGE             = 5;
        constant int EXCEPTION_SLAVE_OR_SERVER_BUSY    = 6;
        constant int EXCEPTION_NEGATIVE_ACKNOWLEDGE    = 7;
        constant int EXCEPTION_MEMORY_PARITY           = 8;
        constant int EXCEPTION_GATEWAY_PATH           = 10;
        constant int EXCEPTION_GATEWAY_TARGET         = 11;

        enumeration SpecificModbusErrors {
            ERROR_BADCRC,
            ERROR_BADDATA,
            ERROR_BADEXC,
            ERROR_UNKEXC,
            ERROR_MDATA,
            ERROR_OTHER
        };
    };
}

#endif /* __PERIPHERAL_MODBUS_CFG_IDL__ */
