/* SPDX-License-Identifier: BSD-3-Clause */
/*
 * Copyright 2010 Raritan Inc. All rights reserved.
 */

/**
 * TLS Certificate Management
 */
module cert {

    /** TLS certificate management interface */
    interface ServerSSLCert_4_0_0 {

        /** success code */
        constant int SUCCESS                            = 0;

        /** shared error codes */
        constant int ERR_BUSY                           = 1;
        constant int ERR_CSR_OR_CERT_PENDING            = 101;
        constant int ERR_KEY_MISSING                    = 200;
        constant int ERR_CERT_MISSING                   = 201;
        constant int ERR_CERT_FORMAT_INVALID            = 202;
        constant int ERR_CERT_KEY_MISMATCH              = 203;
        constant int ERR_KEY_FORMAT_INVALID             = 204;

        /** key-pair generation specific error codes */
        constant int ERR_GEN_KEY_LEN_INVALID            = 100;
        constant int ERR_GEN_CSR_OR_CERT_PENDING        = 101; ///< same as ERR_CSR_OR_CERT_PENDING
        constant int ERR_GEN_KEY_GEN_FAILED             = 102;
        constant int ERR_GEN_KEY_TYPE_INVALID           = 103;
        constant int ERR_GEN_ELLIPTIC_CURVE_INVALID     = 104;
        constant int ERR_GEN_VALIDITY_OUT_OF_RANGE      = 105;

        /** key-pair installation specific error codes (backward compatibility) */
        constant int ERR_INSTALL_KEY_MISSING            = 200; ///< same as ERR_KEY_MISSING
        constant int ERR_INSTALL_CERT_MISSING           = 201; ///< same as ERR_CERT_MISSING
        constant int ERR_INSTALL_CERT_FORMAT_INVALID    = 202; ///< same as ERR_CERT_FORMAT_INVALID
        constant int ERR_INSTALL_CERT_KEY_MISMATCH      = 203; ///< same as ERR_CERT_KEY_MISMATCH
        constant int ERR_INSTALL_KEY_FORMAT_INVALID     = 204; ///> same as ERR_KEY_FORMAT_INVALID

        /** Certificate issuer or subject attributes */
        structure CommonAttributes {
            string country;             ///< Country code
            string stateOrProvince;     ///< State or province
            string locality;            ///< Locality or city
            string organization;        ///< Organization
            string organizationalUnit;  ///< Organizational Unit
            string commonName;          ///< Common Name
            string emailAddress;        ///< Email Address
        };

        /** Supported key types */
        enumeration KeyType {
            KEY_TYPE_UNKNOWN,           ///< Key type unknown (only allowed as return value)
            KEY_TYPE_RSA,               ///< RSA key
            KEY_TYPE_ECDSA              ///< ECDSA key
        };

        /** Supported elliptic curves for key type ECDSA */
        enumeration EllipticCurve {
            EC_CURVE_UNKNOWN,           ///< Curve unknown (only allowed as return value)
            EC_CURVE_NIST_P256,         ///< NIST curve P-256 (also known as secp256r1 and prime256v1)
            EC_CURVE_NIST_P384,         ///< NIST curve P-384 (also known as secp384r1)
            EC_CURVE_NIST_P521          ///< NIST curve P-521 (also known as secp521r1)
        };

        /** Public key information */
        structure KeyInfo {
            KeyType type;               ///< Key type
            EllipticCurve ecCurve;      ///< Selected elliptic curve (only relevant if key type is ECDSA)
            int rsaKeyLength;           ///< Length of the RSA key in bits (only relevant if key type is RSA)
            boolean inSecureElement;    ///< \c true if located in a Secure Element
        };

        /**
         * Certificate signing request information
         *
         * If names is empty then commonName from the subject is used as single entry.
         */
        structure ReqInfo {
            CommonAttributes subject;   ///< Certificate subject attributes
            vector<string> names;       ///< DNS names and/or IP addresses
            KeyInfo keyInfo;            ///< Key information
        };

        /** Certificate information */
        structure CertInfo {
            CommonAttributes subject;   ///< Subject attributes
            CommonAttributes issuer;    ///< Issuer attributes
            vector<string> names;       ///< DNS names and/or IP addresses
            string invalidBefore;       ///< Begin of validity period
            string invalidAfter;        ///< End of validity period
            string serialNumber;        ///< Serial number
            KeyInfo keyInfo;            ///< Key information
        };

        /** Certificate manager information */
        structure Info {
            boolean havePendingReq;                     ///< \c true if a CSR is pending
            boolean havePendingCert;                    ///< \c true if an uploaded certificate is pending activation
            ReqInfo pendingReqInfo;                     ///< Information about pending CSR
            CertInfo pendingCertInfo;                   ///< Information about pending certificate file (device certificate)
            vector<CertInfo> pendingCertChainInfos;     ///< Information about pending certificate file (remaining certificate chain if available)
            CertInfo activeCertInfo;                    ///< Information about active certificate file (device certificate)
            vector<CertInfo> activeCertChainInfos;      ///< Information about active certificate file (remaining certificate chain if available)
            int maxSignDays;                            ///< Maximum number of days a self signed certificate will be valid.
        };

        /**
         * Get all supported key variants.
         *
         * @return Vector of KeyInfo structures representing all supported key variants
         */
        vector<KeyInfo> getSupportedKeyInfos();

        /**
         * Generate an unsigned key pair.
         *
         * @param reqInfo    Certificate signing request information
         * @param challenge  Challenge password
         *
         * @return SUCCESS or one of the error code constants
         */
        int generateUnsignedKeyPair(in ReqInfo reqInfo, in string challenge);

        /**
         * Generate a self-signed key pair.
         *
         * @param reqInfo  Certificate signing request information
         * @param days     Number of days the certificate will be valid
         *
         * @return SUCCESS or one of the error code constants
         */
        int generateSelfSignedKeyPair(in ReqInfo reqInfo, in int days);

        /**
         * Remove pending key and certificate signing request or certificate.
         */
        void deletePending();

        /**
         * Retrieve certificate manager information.
         *
         * @param info  Result: Certificate manager information
         */
        void getInfo(out Info info);

        /**
         * Get the active cert chain in PEM format.
         *
         * Currently not available via JSON-RPC.
         *
         * @return Cert chain in PEM format.
         */
        string getActiveCertChainPEM();

        /**
         * Get the active private key in PEM format.
         *
         * Currently not available via JSON-RPC.
         *
         * @param keyPassword  Password to encrypt the returned key (currently not used)
         *
         * @return The private key in PEM format.
         */
        string getActiveKeyPEM(in string keyPassword);

        /**
         * Get the pending cert signing request (CSR) in PEM format.
         *
         * Currently not available via JSON-RPC.
         *
         * @return Cert signing request in PEM format.
         */
        string getPendingRequestPEM();

        /**
         * Get the pending cert chain in PEM format.
         *
         * Currently not available via JSON-RPC.
         *
         * @return Cert chain in PEM format.
         */
        string getPendingCertChainPEM();

        /**
         * Get the pending private key in PEM format.
         *
         * Currently not available via JSON-RPC.
         *
         * @param keyPassword  Password to encrypt the returned key (currently not used)
         *
         * @return The private key in PEM format.
         */
        string getPendingKeyPEM(in string keyPassword);

        /**
         * Set the pending cert chain in PEM format.
         *
         * Currently not available via JSON-RPC.
         *
         * @param certChain  Cert chain in PEM format.
         *
         * @return SUCCESS or one of the error code constants
         */
        int setPendingCertChainPEM(in string certChain);

        /**
         * Set the pending private key and cert chain in PEM format.
         *
         * Currently not available via JSON-RPC.
         *
         * @param key          Private key in PEM format.
         * @param certChain    Cert chain in PEM format.
         * @param keyPassword  Password to decrypt the private key (currently not used)
         *
         * @return SUCCESS or one of the error code constants
         */
        int setPendingKeyAndCertChainPEM(in string key, in string certChain, in string keyPassword);

        /**
         * Activate a pending key pair.
         *
         * @return SUCCESS or one of the error code constants
         */
        int installPendingKeyPair();

    };

}
