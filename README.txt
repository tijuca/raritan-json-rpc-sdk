Xerus™ JSON-RPC Software Development Kit
=========================================

Welcome to the Xerus™ JSON-RPC Software Development Kit! This archive
contains libraries and documentation for developing programs using the
JSON-RPC interface of Legrand, Raritan and Server Technology Xerus™
products.

Open the file html\index.html file in a web browser to view the complete
documentation.

Archive Contents
================

ChangeLog.txt                   List of interface changes between firmware releases
LICENSES                        License files referred to by SPDX license tags
html                            SDK documentation in HTML format
idl                             Formal interface definitions in IDL format
LICENSE.api-documentation       License information for the API documentation
LICENSE.txt                     General license information
link_cascade_units.py           Script for adding port-forwarding nodes as link units
LuaPLC_Examples                 Example Lua scripts for device-side execution
luaservice_client.py            Interactive program for managing device Lua scripts
pdu-dotnet-api                  C# client library for JSON-RPC interface
pdu-java-api                    Java client library for JSON-RPC interface
pdu-perl-api                    Perl client library for JSON-RPC interface
pdu-python-api                  Python client library for JSON-RPC interface
perl-json-rpc-demo.pl           Example script for using the Perl client library
px2_configure.pl                Mass configuration utility base on the Perl client lib
px2_shell.py                    Interactive Python shell for exploring the JSON-RPC API
python-idl-example.py           Example script for using the Python client library, focused on settings
python-json-rpc-demo.py         Example script for using the Python client library, focused on metering and switching
px2_bulk_password_change.py     Script to change the password of multiple PDUs
px2_bulk_update.py              Script to update a large number of PDUs in parallel (Python, recommended)
px2_bulk_update_demo.pl         Script to update a large number of PDUs in parallel (Perl)
sensor_log_receiving_service.py Example script for receiving Sensor Log messages using a PDUs Data Push mechanism
gateway-sensors-PMMC.py         Example script for Gateway Sensors configuration
gateway-sensors-PM710.py        Example script for Gateway Sensors configuration
README.px2_configure            Documentation for px2_configure.pl utility
well-known-rids.md              List of well-known resource IDs of JSON-RPC objects
