# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright 2025 Raritan Inc. All rights reserved.
#
# This is an auto-generated file.

#
# Section generated by IdlC from "CardReader.idl"
#

import raritan.rpc
from raritan.rpc import Interface, Structure, ValueObject, Enumeration, typecheck, DecodeException
import raritan.rpc.idl

import raritan.rpc.smartcard


# interface
class CardReader(Interface):
    idlType = "smartcard.CardReader:1.0.4"

    NO_ERROR = 0

    ERR_SLOT_EMPTY = 1

    # structure
    class MetaData(Structure):
        idlType = "smartcard.CardReader_1_0_4.MetaData:1.0.0"
        elements = ["id", "manufacturer", "product", "serialNumber", "channel", "position"]

        def __init__(self, id="", manufacturer="", product="", serialNumber="", channel=0, position=""):
            if not typecheck._is_int(id):
                typecheck.is_string(id, AssertionError)
            if not typecheck._is_int(manufacturer):
                typecheck.is_string(manufacturer, AssertionError)
            if not typecheck._is_int(product):
                typecheck.is_string(product, AssertionError)
            if not typecheck._is_int(serialNumber):
                typecheck.is_string(serialNumber, AssertionError)
            typecheck.is_int(channel, AssertionError)
            if not typecheck._is_int(position):
                typecheck.is_string(position, AssertionError)

            self.id = id
            self.manufacturer = manufacturer
            self.product = product
            self.serialNumber = serialNumber
            self.channel = channel
            self.position = position

        @classmethod
        def decode(cls, json, agent, useDefaults=False):
            obj = cls(
                id = json['id'] if 'id' in json or not useDefaults else "",
                manufacturer = json['manufacturer'] if 'manufacturer' in json or not useDefaults else "",
                product = json['product'] if 'product' in json or not useDefaults else "",
                serialNumber = json['serialNumber'] if 'serialNumber' in json or not useDefaults else "",
                channel = json['channel'] if 'channel' in json or not useDefaults else 0,
                position = json['position'] if 'position' in json or not useDefaults else "",
            )
            return obj

        def encode(self):
            json = {}
            json['id'] = str(self.id)
            json['manufacturer'] = str(self.manufacturer)
            json['product'] = str(self.product)
            json['serialNumber'] = str(self.serialNumber)
            json['channel'] = self.channel
            json['position'] = str(self.position)
            return json

    # structure
    class CardInformation(Structure):
        idlType = "smartcard.CardReader_1_0_4.CardInformation:1.0.0"
        elements = ["type", "uid"]

        def __init__(self, type="", uid=""):
            if not typecheck._is_int(type):
                typecheck.is_string(type, AssertionError)
            if not typecheck._is_int(uid):
                typecheck.is_string(uid, AssertionError)

            self.type = type
            self.uid = uid

        @classmethod
        def decode(cls, json, agent, useDefaults=False):
            obj = cls(
                type = json['type'] if 'type' in json or not useDefaults else "",
                uid = json['uid'] if 'uid' in json or not useDefaults else "",
            )
            return obj

        def encode(self):
            json = {}
            json['type'] = str(self.type)
            json['uid'] = str(self.uid)
            return json

    # value object
    class CardEvent(raritan.rpc.idl.Event):
        idlType = "smartcard.CardReader_1_0_4.CardEvent:2.0.0"

        def __init__(self, metaData=None, source=None):
            super(raritan.rpc.smartcard.CardReader.CardEvent, self).__init__(source)
            if metaData is None:
                metaData = raritan.rpc.smartcard.CardReader.MetaData()
            typecheck.is_struct(metaData, raritan.rpc.smartcard.CardReader.MetaData, AssertionError)

            self.metaData = metaData

        def encode(self):
            json = super(raritan.rpc.smartcard.CardReader.CardEvent, self).encode()
            json['metaData'] = raritan.rpc.smartcard.CardReader.MetaData.encode(self.metaData)
            return json

        @classmethod
        def decode(cls, json, agent, useDefaults=False):
            obj = cls(
                metaData = raritan.rpc.smartcard.CardReader.MetaData.decode(json['metaData'], agent, useDefaults=useDefaults) if 'metaData' in json or not useDefaults else raritan.rpc.smartcard.CardReader.MetaData(),
                # for idl.Event
                source = Interface.decode(json['source'], agent) if 'source' in json or not useDefaults else None,
            )
            return obj

        def listElements(self):
            elements = ["metaData"]
            elements = elements + super(raritan.rpc.smartcard.CardReader.CardEvent, self).listElements()
            return elements

    # value object
    class CardInsertedEvent(CardEvent):
        idlType = "smartcard.CardReader_1_0_4.CardInsertedEvent:2.0.0"

        def __init__(self, metaData=None, source=None):
            super(raritan.rpc.smartcard.CardReader.CardInsertedEvent, self).__init__(metaData, source)

        def encode(self):
            json = super(raritan.rpc.smartcard.CardReader.CardInsertedEvent, self).encode()
            return json

        @classmethod
        def decode(cls, json, agent, useDefaults=False):
            obj = cls(
                # for smartcard.CardReader_1_0_4.CardEvent_2_0_0
                metaData = raritan.rpc.smartcard.CardReader.MetaData.decode(json['metaData'], agent, useDefaults=useDefaults) if 'metaData' in json or not useDefaults else raritan.rpc.smartcard.CardReader.MetaData(),
                # for idl.Event
                source = Interface.decode(json['source'], agent) if 'source' in json or not useDefaults else None,
            )
            return obj

        def listElements(self):
            elements = []
            elements = elements + super(raritan.rpc.smartcard.CardReader.CardInsertedEvent, self).listElements()
            return elements

    # value object
    class CardRemovedEvent(CardEvent):
        idlType = "smartcard.CardReader_1_0_4.CardRemovedEvent:2.0.0"

        def __init__(self, metaData=None, source=None):
            super(raritan.rpc.smartcard.CardReader.CardRemovedEvent, self).__init__(metaData, source)

        def encode(self):
            json = super(raritan.rpc.smartcard.CardReader.CardRemovedEvent, self).encode()
            return json

        @classmethod
        def decode(cls, json, agent, useDefaults=False):
            obj = cls(
                # for smartcard.CardReader_1_0_4.CardEvent_2_0_0
                metaData = raritan.rpc.smartcard.CardReader.MetaData.decode(json['metaData'], agent, useDefaults=useDefaults) if 'metaData' in json or not useDefaults else raritan.rpc.smartcard.CardReader.MetaData(),
                # for idl.Event
                source = Interface.decode(json['source'], agent) if 'source' in json or not useDefaults else None,
            )
            return obj

        def listElements(self):
            elements = []
            elements = elements + super(raritan.rpc.smartcard.CardReader.CardRemovedEvent, self).listElements()
            return elements

    class _getMetaData(Interface.Method):
        name = 'getMetaData'

        @staticmethod
        def encode():
            args = {}
            return args

        @staticmethod
        def decode(rsp, agent, useDefaults=False):
            _ret_ = raritan.rpc.smartcard.CardReader.MetaData.decode(rsp['_ret_'], agent, useDefaults=useDefaults)
            typecheck.is_struct(_ret_, raritan.rpc.smartcard.CardReader.MetaData, DecodeException)
            return _ret_

    class _getCardInformation(Interface.Method):
        name = 'getCardInformation'

        @staticmethod
        def encode():
            args = {}
            return args

        @staticmethod
        def decode(rsp, agent, useDefaults=False):
            _ret_ = rsp['_ret_']
            cardInfo = raritan.rpc.smartcard.CardReader.CardInformation.decode(rsp['cardInfo'], agent, useDefaults=useDefaults)
            typecheck.is_int(_ret_, DecodeException)
            typecheck.is_struct(cardInfo, raritan.rpc.smartcard.CardReader.CardInformation, DecodeException)
            return (_ret_, cardInfo)
    def __init__(self, target, agent):
        super(CardReader, self).__init__(target, agent)
        self.getMetaData = CardReader._getMetaData(self)
        self.getCardInformation = CardReader._getCardInformation(self)

#
# Section generated by IdlC from "CardReaderManager.idl"
#

import raritan.rpc
from raritan.rpc import Interface, Structure, ValueObject, Enumeration, typecheck, DecodeException
import raritan.rpc.event

import raritan.rpc.idl

import raritan.rpc.smartcard


# interface
class CardReaderManager(Interface):
    idlType = "smartcard.CardReaderManager:1.0.5"

    # structure
    class CardReaderSettings(Structure):
        idlType = "smartcard.CardReaderManager_1_0_5.CardReaderSettings:1.0.0"
        elements = ["name", "description", "cardFormat"]

        def __init__(self, name="", description="", cardFormat=""):
            if not typecheck._is_int(name):
                typecheck.is_string(name, AssertionError)
            if not typecheck._is_int(description):
                typecheck.is_string(description, AssertionError)
            if not typecheck._is_int(cardFormat):
                typecheck.is_string(cardFormat, AssertionError)

            self.name = name
            self.description = description
            self.cardFormat = cardFormat

        @classmethod
        def decode(cls, json, agent, useDefaults=False):
            obj = cls(
                name = json['name'] if 'name' in json or not useDefaults else "",
                description = json['description'] if 'description' in json or not useDefaults else "",
                cardFormat = json['cardFormat'] if 'cardFormat' in json or not useDefaults else "",
            )
            return obj

        def encode(self):
            json = {}
            json['name'] = str(self.name)
            json['description'] = str(self.description)
            json['cardFormat'] = str(self.cardFormat)
            return json

    # value object
    class CardReaderEvent(raritan.rpc.idl.Event):
        idlType = "smartcard.CardReaderManager_1_0_5.CardReaderEvent:1.0.0"

        def __init__(self, cardReader=None, metaData=None, source=None):
            super(raritan.rpc.smartcard.CardReaderManager.CardReaderEvent, self).__init__(source)
            if metaData is None:
                metaData = raritan.rpc.smartcard.CardReader.MetaData()
            typecheck.is_interface(cardReader, raritan.rpc.smartcard.CardReader, AssertionError)
            typecheck.is_struct(metaData, raritan.rpc.smartcard.CardReader.MetaData, AssertionError)

            self.cardReader = cardReader
            self.metaData = metaData

        def encode(self):
            json = super(raritan.rpc.smartcard.CardReaderManager.CardReaderEvent, self).encode()
            json['cardReader'] = Interface.encode(self.cardReader)
            json['metaData'] = raritan.rpc.smartcard.CardReader.MetaData.encode(self.metaData)
            return json

        @classmethod
        def decode(cls, json, agent, useDefaults=False):
            obj = cls(
                cardReader = Interface.decode(json['cardReader'], agent) if 'cardReader' in json or not useDefaults else None,
                metaData = raritan.rpc.smartcard.CardReader.MetaData.decode(json['metaData'], agent, useDefaults=useDefaults) if 'metaData' in json or not useDefaults else raritan.rpc.smartcard.CardReader.MetaData(),
                # for idl.Event
                source = Interface.decode(json['source'], agent) if 'source' in json or not useDefaults else None,
            )
            return obj

        def listElements(self):
            elements = ["cardReader", "metaData"]
            elements = elements + super(raritan.rpc.smartcard.CardReaderManager.CardReaderEvent, self).listElements()
            return elements

    # value object
    class CardReaderAttachedEvent(CardReaderEvent):
        idlType = "smartcard.CardReaderManager_1_0_5.CardReaderAttachedEvent:1.0.0"

        def __init__(self, cardReader=None, metaData=None, source=None):
            super(raritan.rpc.smartcard.CardReaderManager.CardReaderAttachedEvent, self).__init__(cardReader, metaData, source)

        def encode(self):
            json = super(raritan.rpc.smartcard.CardReaderManager.CardReaderAttachedEvent, self).encode()
            return json

        @classmethod
        def decode(cls, json, agent, useDefaults=False):
            obj = cls(
                # for smartcard.CardReaderManager_1_0_5.CardReaderEvent
                cardReader = Interface.decode(json['cardReader'], agent) if 'cardReader' in json or not useDefaults else None,
                metaData = raritan.rpc.smartcard.CardReader.MetaData.decode(json['metaData'], agent, useDefaults=useDefaults) if 'metaData' in json or not useDefaults else raritan.rpc.smartcard.CardReader.MetaData(),
                # for idl.Event
                source = Interface.decode(json['source'], agent) if 'source' in json or not useDefaults else None,
            )
            return obj

        def listElements(self):
            elements = []
            elements = elements + super(raritan.rpc.smartcard.CardReaderManager.CardReaderAttachedEvent, self).listElements()
            return elements

    # value object
    class CardReaderDetachedEvent(CardReaderEvent):
        idlType = "smartcard.CardReaderManager_1_0_5.CardReaderDetachedEvent:1.0.0"

        def __init__(self, cardReader=None, metaData=None, source=None):
            super(raritan.rpc.smartcard.CardReaderManager.CardReaderDetachedEvent, self).__init__(cardReader, metaData, source)

        def encode(self):
            json = super(raritan.rpc.smartcard.CardReaderManager.CardReaderDetachedEvent, self).encode()
            return json

        @classmethod
        def decode(cls, json, agent, useDefaults=False):
            obj = cls(
                # for smartcard.CardReaderManager_1_0_5.CardReaderEvent
                cardReader = Interface.decode(json['cardReader'], agent) if 'cardReader' in json or not useDefaults else None,
                metaData = raritan.rpc.smartcard.CardReader.MetaData.decode(json['metaData'], agent, useDefaults=useDefaults) if 'metaData' in json or not useDefaults else raritan.rpc.smartcard.CardReader.MetaData(),
                # for idl.Event
                source = Interface.decode(json['source'], agent) if 'source' in json or not useDefaults else None,
            )
            return obj

        def listElements(self):
            elements = []
            elements = elements + super(raritan.rpc.smartcard.CardReaderManager.CardReaderDetachedEvent, self).listElements()
            return elements

    # value object
    class CardReaderSettingsChangedEvent(raritan.rpc.event.UserEvent):
        idlType = "smartcard.CardReaderManager_1_0_5.CardReaderSettingsChangedEvent:1.0.0"

        def __init__(self, cardReader=None, oldSettings=None, newSettings=None, position="", actUserName="", actIpAddr="", source=None):
            super(raritan.rpc.smartcard.CardReaderManager.CardReaderSettingsChangedEvent, self).__init__(actUserName, actIpAddr, source)
            if oldSettings is None:
                oldSettings = raritan.rpc.smartcard.CardReaderManager.CardReaderSettings()
            if newSettings is None:
                newSettings = raritan.rpc.smartcard.CardReaderManager.CardReaderSettings()
            typecheck.is_interface(cardReader, raritan.rpc.smartcard.CardReader, AssertionError)
            typecheck.is_struct(oldSettings, raritan.rpc.smartcard.CardReaderManager.CardReaderSettings, AssertionError)
            typecheck.is_struct(newSettings, raritan.rpc.smartcard.CardReaderManager.CardReaderSettings, AssertionError)
            if not typecheck._is_int(position):
                typecheck.is_string(position, AssertionError)

            self.cardReader = cardReader
            self.oldSettings = oldSettings
            self.newSettings = newSettings
            self.position = position

        def encode(self):
            json = super(raritan.rpc.smartcard.CardReaderManager.CardReaderSettingsChangedEvent, self).encode()
            json['cardReader'] = Interface.encode(self.cardReader)
            json['oldSettings'] = raritan.rpc.smartcard.CardReaderManager.CardReaderSettings.encode(self.oldSettings)
            json['newSettings'] = raritan.rpc.smartcard.CardReaderManager.CardReaderSettings.encode(self.newSettings)
            json['position'] = str(self.position)
            return json

        @classmethod
        def decode(cls, json, agent, useDefaults=False):
            obj = cls(
                cardReader = Interface.decode(json['cardReader'], agent) if 'cardReader' in json or not useDefaults else None,
                oldSettings = raritan.rpc.smartcard.CardReaderManager.CardReaderSettings.decode(json['oldSettings'], agent, useDefaults=useDefaults) if 'oldSettings' in json or not useDefaults else raritan.rpc.smartcard.CardReaderManager.CardReaderSettings(),
                newSettings = raritan.rpc.smartcard.CardReaderManager.CardReaderSettings.decode(json['newSettings'], agent, useDefaults=useDefaults) if 'newSettings' in json or not useDefaults else raritan.rpc.smartcard.CardReaderManager.CardReaderSettings(),
                position = json['position'] if 'position' in json or not useDefaults else "",
                # for event.UserEvent
                actUserName = json['actUserName'] if 'actUserName' in json or not useDefaults else "",
                actIpAddr = json['actIpAddr'] if 'actIpAddr' in json or not useDefaults else "",
                # for idl.Event
                source = Interface.decode(json['source'], agent) if 'source' in json or not useDefaults else None,
            )
            return obj

        def listElements(self):
            elements = ["cardReader", "oldSettings", "newSettings", "position"]
            elements = elements + super(raritan.rpc.smartcard.CardReaderManager.CardReaderSettingsChangedEvent, self).listElements()
            return elements

    class _getCardReaders(Interface.Method):
        name = 'getCardReaders'

        @staticmethod
        def encode():
            args = {}
            return args

        @staticmethod
        def decode(rsp, agent, useDefaults=False):
            _ret_ = [Interface.decode(x0, agent) for x0 in rsp['_ret_']]
            for x0 in _ret_:
                typecheck.is_interface(x0, raritan.rpc.smartcard.CardReader, DecodeException)
            return _ret_

    class _getCardReaderById(Interface.Method):
        name = 'getCardReaderById'

        @staticmethod
        def encode(readerId):
            if not typecheck._is_int(readerId):
                typecheck.is_string(readerId, AssertionError)
            args = {}
            args['readerId'] = str(readerId)
            return args

        @staticmethod
        def decode(rsp, agent, useDefaults=False):
            _ret_ = Interface.decode(rsp['_ret_'], agent)
            typecheck.is_interface(_ret_, raritan.rpc.smartcard.CardReader, DecodeException)
            return _ret_

    class _setCardReaderSettings(Interface.Method):
        name = 'setCardReaderSettings'

        @staticmethod
        def encode(position, setting):
            if not typecheck._is_int(position):
                typecheck.is_string(position, AssertionError)
            typecheck.is_struct(setting, raritan.rpc.smartcard.CardReaderManager.CardReaderSettings, AssertionError)
            args = {}
            args['position'] = str(position)
            args['setting'] = raritan.rpc.smartcard.CardReaderManager.CardReaderSettings.encode(setting)
            return args

        @staticmethod
        def decode(rsp, agent, useDefaults=False):
            _ret_ = rsp['_ret_']
            typecheck.is_int(_ret_, DecodeException)
            return _ret_

    class _getAllCardReaderSettings(Interface.Method):
        name = 'getAllCardReaderSettings'

        @staticmethod
        def encode():
            args = {}
            return args

        @staticmethod
        def decode(rsp, agent, useDefaults=False):
            _ret_ = dict([(
                elem['key'],
                raritan.rpc.smartcard.CardReaderManager.CardReaderSettings.decode(elem['value'], agent, useDefaults=useDefaults))
                for elem in rsp['_ret_']])
            return _ret_

    class _getSupportedCardFormats(Interface.Method):
        name = 'getSupportedCardFormats'

        @staticmethod
        def encode():
            args = {}
            return args

        @staticmethod
        def decode(rsp, agent, useDefaults=False):
            _ret_ = [x0 for x0 in rsp['_ret_']]
            for x0 in _ret_:
                if not typecheck._is_int(x0):
                    typecheck.is_string(x0, DecodeException)
            return _ret_
    def __init__(self, target, agent):
        super(CardReaderManager, self).__init__(target, agent)
        self.getCardReaders = CardReaderManager._getCardReaders(self)
        self.getCardReaderById = CardReaderManager._getCardReaderById(self)
        self.setCardReaderSettings = CardReaderManager._setCardReaderSettings(self)
        self.getAllCardReaderSettings = CardReaderManager._getAllCardReaderSettings(self)
        self.getSupportedCardFormats = CardReaderManager._getSupportedCardFormats(self)
