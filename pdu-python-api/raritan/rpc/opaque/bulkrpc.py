# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright 2014 Raritan Inc. All rights reserved.

class JsonObject:

    @staticmethod
    def decode(json, agent, useDefaults=False):
        return json

    @staticmethod
    def encode(obj):
        return obj
