# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright 2025 Raritan Inc. All rights reserved.
#
# This is an auto-generated file.

#
# Section generated by IdlC from "DsamPort.idl"
#

import raritan.rpc
from raritan.rpc import Interface, Structure, ValueObject, Enumeration, typecheck, DecodeException
import raritan.rpc.dsam

import raritan.rpc.event


# interface
class DsamPort(Interface):
    idlType = "dsam.DsamPort:1.0.0"

    SUCCESS = 0

    SETTINGS_INVALID = 1

    SSH_DPA_PORT_INVALID = 2

    SSH_DPA_PORT_IN_USE = 3

    # enumeration
    class DeviceInterfaceType(Enumeration):
        idlType = "dsam.DsamPort.DeviceInterfaceType:1.0.0"
        values = ["DEV_IFTYPE_AUTO", "DEV_IFTYPE_DTE", "DEV_IFTYPE_DCE"]

    DeviceInterfaceType.DEV_IFTYPE_AUTO = DeviceInterfaceType(0)
    DeviceInterfaceType.DEV_IFTYPE_DTE = DeviceInterfaceType(1)
    DeviceInterfaceType.DEV_IFTYPE_DCE = DeviceInterfaceType(2)

    # enumeration
    class Parity(Enumeration):
        idlType = "dsam.DsamPort.Parity:1.0.0"
        values = ["PARITY_NONE", "PARITY_ODD", "PARITY_EVEN"]

    Parity.PARITY_NONE = Parity(0)
    Parity.PARITY_ODD = Parity(1)
    Parity.PARITY_EVEN = Parity(2)

    # enumeration
    class FlowControl(Enumeration):
        idlType = "dsam.DsamPort.FlowControl:1.0.0"
        values = ["FLOW_CTRL_NONE", "FLOW_CTRL_HARDWARE", "FLOW_CTRL_SOFTWARE"]

    FlowControl.FLOW_CTRL_NONE = FlowControl(0)
    FlowControl.FLOW_CTRL_HARDWARE = FlowControl(1)
    FlowControl.FLOW_CTRL_SOFTWARE = FlowControl(2)

    # enumeration
    class State(Enumeration):
        idlType = "dsam.DsamPort.State:1.0.0"
        values = ["STATE_AVAILABLE", "STATE_OCCUPIED", "STATE_BUSY"]

    State.STATE_AVAILABLE = State(0)
    State.STATE_OCCUPIED = State(1)
    State.STATE_BUSY = State(2)

    # structure
    class Info(Structure):
        idlType = "dsam.DsamPort.Info:1.0.0"
        elements = ["dsamNumber", "portNumber", "connected", "devIfType", "state"]

        def __init__(self, dsamNumber=0, portNumber=0, connected=False, devIfType=None, state=None):
            if devIfType is None:
                devIfType = raritan.rpc.dsam.DsamPort.DeviceInterfaceType.DEV_IFTYPE_AUTO
            if state is None:
                state = raritan.rpc.dsam.DsamPort.State.STATE_AVAILABLE
            typecheck.is_int(dsamNumber, AssertionError)
            typecheck.is_int(portNumber, AssertionError)
            typecheck.is_bool(connected, AssertionError)
            typecheck.is_enum(devIfType, raritan.rpc.dsam.DsamPort.DeviceInterfaceType, AssertionError)
            typecheck.is_enum(state, raritan.rpc.dsam.DsamPort.State, AssertionError)

            self.dsamNumber = dsamNumber
            self.portNumber = portNumber
            self.connected = connected
            self.devIfType = devIfType
            self.state = state

        @classmethod
        def decode(cls, json, agent, useDefaults=False):
            obj = cls(
                dsamNumber = json['dsamNumber'] if 'dsamNumber' in json or not useDefaults else 0,
                portNumber = json['portNumber'] if 'portNumber' in json or not useDefaults else 0,
                connected = json['connected'] if 'connected' in json or not useDefaults else False,
                devIfType = raritan.rpc.dsam.DsamPort.DeviceInterfaceType.decode(json['devIfType']) if 'devIfType' in json or not useDefaults else raritan.rpc.dsam.DsamPort.DeviceInterfaceType.DEV_IFTYPE_AUTO,
                state = raritan.rpc.dsam.DsamPort.State.decode(json['state']) if 'state' in json or not useDefaults else raritan.rpc.dsam.DsamPort.State.STATE_AVAILABLE,
            )
            return obj

        def encode(self):
            json = {}
            json['dsamNumber'] = self.dsamNumber
            json['portNumber'] = self.portNumber
            json['connected'] = self.connected
            json['devIfType'] = raritan.rpc.dsam.DsamPort.DeviceInterfaceType.encode(self.devIfType)
            json['state'] = raritan.rpc.dsam.DsamPort.State.encode(self.state)
            return json

    # structure
    class Settings(Structure):
        idlType = "dsam.DsamPort.Settings:1.0.0"
        elements = ["name", "devIfType", "baudRate", "parity", "stopBits", "flowCtrl", "breakDurationMs", "sshDpaPortEnabled", "sshDpaPort", "allowSharedAccess"]

        def __init__(self, name="", devIfType=None, baudRate=0, parity=None, stopBits=0, flowCtrl=None, breakDurationMs=0, sshDpaPortEnabled=False, sshDpaPort=0, allowSharedAccess=False):
            if devIfType is None:
                devIfType = raritan.rpc.dsam.DsamPort.DeviceInterfaceType.DEV_IFTYPE_AUTO
            if parity is None:
                parity = raritan.rpc.dsam.DsamPort.Parity.PARITY_NONE
            if flowCtrl is None:
                flowCtrl = raritan.rpc.dsam.DsamPort.FlowControl.FLOW_CTRL_NONE
            if not typecheck._is_int(name):
                typecheck.is_string(name, AssertionError)
            typecheck.is_enum(devIfType, raritan.rpc.dsam.DsamPort.DeviceInterfaceType, AssertionError)
            typecheck.is_int(baudRate, AssertionError)
            typecheck.is_enum(parity, raritan.rpc.dsam.DsamPort.Parity, AssertionError)
            typecheck.is_int(stopBits, AssertionError)
            typecheck.is_enum(flowCtrl, raritan.rpc.dsam.DsamPort.FlowControl, AssertionError)
            typecheck.is_int(breakDurationMs, AssertionError)
            typecheck.is_bool(sshDpaPortEnabled, AssertionError)
            typecheck.is_int(sshDpaPort, AssertionError)
            typecheck.is_bool(allowSharedAccess, AssertionError)

            self.name = name
            self.devIfType = devIfType
            self.baudRate = baudRate
            self.parity = parity
            self.stopBits = stopBits
            self.flowCtrl = flowCtrl
            self.breakDurationMs = breakDurationMs
            self.sshDpaPortEnabled = sshDpaPortEnabled
            self.sshDpaPort = sshDpaPort
            self.allowSharedAccess = allowSharedAccess

        @classmethod
        def decode(cls, json, agent, useDefaults=False):
            obj = cls(
                name = json['name'] if 'name' in json or not useDefaults else "",
                devIfType = raritan.rpc.dsam.DsamPort.DeviceInterfaceType.decode(json['devIfType']) if 'devIfType' in json or not useDefaults else raritan.rpc.dsam.DsamPort.DeviceInterfaceType.DEV_IFTYPE_AUTO,
                baudRate = json['baudRate'] if 'baudRate' in json or not useDefaults else 0,
                parity = raritan.rpc.dsam.DsamPort.Parity.decode(json['parity']) if 'parity' in json or not useDefaults else raritan.rpc.dsam.DsamPort.Parity.PARITY_NONE,
                stopBits = json['stopBits'] if 'stopBits' in json or not useDefaults else 0,
                flowCtrl = raritan.rpc.dsam.DsamPort.FlowControl.decode(json['flowCtrl']) if 'flowCtrl' in json or not useDefaults else raritan.rpc.dsam.DsamPort.FlowControl.FLOW_CTRL_NONE,
                breakDurationMs = json['breakDurationMs'] if 'breakDurationMs' in json or not useDefaults else 0,
                sshDpaPortEnabled = json['sshDpaPortEnabled'] if 'sshDpaPortEnabled' in json or not useDefaults else False,
                sshDpaPort = json['sshDpaPort'] if 'sshDpaPort' in json or not useDefaults else 0,
                allowSharedAccess = json['allowSharedAccess'] if 'allowSharedAccess' in json or not useDefaults else False,
            )
            return obj

        def encode(self):
            json = {}
            json['name'] = str(self.name)
            json['devIfType'] = raritan.rpc.dsam.DsamPort.DeviceInterfaceType.encode(self.devIfType)
            json['baudRate'] = self.baudRate
            json['parity'] = raritan.rpc.dsam.DsamPort.Parity.encode(self.parity)
            json['stopBits'] = self.stopBits
            json['flowCtrl'] = raritan.rpc.dsam.DsamPort.FlowControl.encode(self.flowCtrl)
            json['breakDurationMs'] = self.breakDurationMs
            json['sshDpaPortEnabled'] = self.sshDpaPortEnabled
            json['sshDpaPort'] = self.sshDpaPort
            json['allowSharedAccess'] = self.allowSharedAccess
            return json

    # value object
    class InfoChangedEvent(raritan.rpc.event.UserEvent):
        idlType = "dsam.DsamPort.InfoChangedEvent:1.0.0"

        def __init__(self, oldInfo=None, newInfo=None, portName="", actUserName="", actIpAddr="", source=None):
            super(raritan.rpc.dsam.DsamPort.InfoChangedEvent, self).__init__(actUserName, actIpAddr, source)
            if oldInfo is None:
                oldInfo = raritan.rpc.dsam.DsamPort.Info()
            if newInfo is None:
                newInfo = raritan.rpc.dsam.DsamPort.Info()
            typecheck.is_struct(oldInfo, raritan.rpc.dsam.DsamPort.Info, AssertionError)
            typecheck.is_struct(newInfo, raritan.rpc.dsam.DsamPort.Info, AssertionError)
            if not typecheck._is_int(portName):
                typecheck.is_string(portName, AssertionError)

            self.oldInfo = oldInfo
            self.newInfo = newInfo
            self.portName = portName

        def encode(self):
            json = super(raritan.rpc.dsam.DsamPort.InfoChangedEvent, self).encode()
            json['oldInfo'] = raritan.rpc.dsam.DsamPort.Info.encode(self.oldInfo)
            json['newInfo'] = raritan.rpc.dsam.DsamPort.Info.encode(self.newInfo)
            json['portName'] = str(self.portName)
            return json

        @classmethod
        def decode(cls, json, agent, useDefaults=False):
            obj = cls(
                oldInfo = raritan.rpc.dsam.DsamPort.Info.decode(json['oldInfo'], agent, useDefaults=useDefaults) if 'oldInfo' in json or not useDefaults else raritan.rpc.dsam.DsamPort.Info(),
                newInfo = raritan.rpc.dsam.DsamPort.Info.decode(json['newInfo'], agent, useDefaults=useDefaults) if 'newInfo' in json or not useDefaults else raritan.rpc.dsam.DsamPort.Info(),
                portName = json['portName'] if 'portName' in json or not useDefaults else "",
                # for event.UserEvent
                actUserName = json['actUserName'] if 'actUserName' in json or not useDefaults else "",
                actIpAddr = json['actIpAddr'] if 'actIpAddr' in json or not useDefaults else "",
                # for idl.Event
                source = Interface.decode(json['source'], agent) if 'source' in json or not useDefaults else None,
            )
            return obj

        def listElements(self):
            elements = ["oldInfo", "newInfo", "portName"]
            elements = elements + super(raritan.rpc.dsam.DsamPort.InfoChangedEvent, self).listElements()
            return elements

    # value object
    class SettingsChangedEvent(raritan.rpc.event.UserEvent):
        idlType = "dsam.DsamPort.SettingsChangedEvent:1.0.0"

        def __init__(self, dsamNumber=0, portNumber=0, oldSettings=None, newSettings=None, actUserName="", actIpAddr="", source=None):
            super(raritan.rpc.dsam.DsamPort.SettingsChangedEvent, self).__init__(actUserName, actIpAddr, source)
            if oldSettings is None:
                oldSettings = raritan.rpc.dsam.DsamPort.Settings()
            if newSettings is None:
                newSettings = raritan.rpc.dsam.DsamPort.Settings()
            typecheck.is_int(dsamNumber, AssertionError)
            typecheck.is_int(portNumber, AssertionError)
            typecheck.is_struct(oldSettings, raritan.rpc.dsam.DsamPort.Settings, AssertionError)
            typecheck.is_struct(newSettings, raritan.rpc.dsam.DsamPort.Settings, AssertionError)

            self.dsamNumber = dsamNumber
            self.portNumber = portNumber
            self.oldSettings = oldSettings
            self.newSettings = newSettings

        def encode(self):
            json = super(raritan.rpc.dsam.DsamPort.SettingsChangedEvent, self).encode()
            json['dsamNumber'] = self.dsamNumber
            json['portNumber'] = self.portNumber
            json['oldSettings'] = raritan.rpc.dsam.DsamPort.Settings.encode(self.oldSettings)
            json['newSettings'] = raritan.rpc.dsam.DsamPort.Settings.encode(self.newSettings)
            return json

        @classmethod
        def decode(cls, json, agent, useDefaults=False):
            obj = cls(
                dsamNumber = json['dsamNumber'] if 'dsamNumber' in json or not useDefaults else 0,
                portNumber = json['portNumber'] if 'portNumber' in json or not useDefaults else 0,
                oldSettings = raritan.rpc.dsam.DsamPort.Settings.decode(json['oldSettings'], agent, useDefaults=useDefaults) if 'oldSettings' in json or not useDefaults else raritan.rpc.dsam.DsamPort.Settings(),
                newSettings = raritan.rpc.dsam.DsamPort.Settings.decode(json['newSettings'], agent, useDefaults=useDefaults) if 'newSettings' in json or not useDefaults else raritan.rpc.dsam.DsamPort.Settings(),
                # for event.UserEvent
                actUserName = json['actUserName'] if 'actUserName' in json or not useDefaults else "",
                actIpAddr = json['actIpAddr'] if 'actIpAddr' in json or not useDefaults else "",
                # for idl.Event
                source = Interface.decode(json['source'], agent) if 'source' in json or not useDefaults else None,
            )
            return obj

        def listElements(self):
            elements = ["dsamNumber", "portNumber", "oldSettings", "newSettings"]
            elements = elements + super(raritan.rpc.dsam.DsamPort.SettingsChangedEvent, self).listElements()
            return elements

    class _getInfo(Interface.Method):
        name = 'getInfo'

        @staticmethod
        def encode():
            args = {}
            return args

        @staticmethod
        def decode(rsp, agent, useDefaults=False):
            _ret_ = raritan.rpc.dsam.DsamPort.Info.decode(rsp['_ret_'], agent, useDefaults=useDefaults)
            typecheck.is_struct(_ret_, raritan.rpc.dsam.DsamPort.Info, DecodeException)
            return _ret_

    class _getSettings(Interface.Method):
        name = 'getSettings'

        @staticmethod
        def encode():
            args = {}
            return args

        @staticmethod
        def decode(rsp, agent, useDefaults=False):
            _ret_ = raritan.rpc.dsam.DsamPort.Settings.decode(rsp['_ret_'], agent, useDefaults=useDefaults)
            typecheck.is_struct(_ret_, raritan.rpc.dsam.DsamPort.Settings, DecodeException)
            return _ret_

    class _setSettings(Interface.Method):
        name = 'setSettings'

        @staticmethod
        def encode(settings):
            typecheck.is_struct(settings, raritan.rpc.dsam.DsamPort.Settings, AssertionError)
            args = {}
            args['settings'] = raritan.rpc.dsam.DsamPort.Settings.encode(settings)
            return args

        @staticmethod
        def decode(rsp, agent, useDefaults=False):
            _ret_ = rsp['_ret_']
            typecheck.is_int(_ret_, DecodeException)
            return _ret_

    class _getTtyUsbNumber(Interface.Method):
        name = 'getTtyUsbNumber'

        @staticmethod
        def encode():
            args = {}
            return args

        @staticmethod
        def decode(rsp, agent, useDefaults=False):
            _ret_ = rsp['_ret_']
            typecheck.is_int(_ret_, DecodeException)
            return _ret_

    class _setState(Interface.Method):
        name = 'setState'

        @staticmethod
        def encode(state):
            typecheck.is_enum(state, raritan.rpc.dsam.DsamPort.State, AssertionError)
            args = {}
            args['state'] = raritan.rpc.dsam.DsamPort.State.encode(state)
            return args

        @staticmethod
        def decode(rsp, agent, useDefaults=False):
            return None
    def __init__(self, target, agent):
        super(DsamPort, self).__init__(target, agent)
        self.getInfo = DsamPort._getInfo(self)
        self.getSettings = DsamPort._getSettings(self)
        self.setSettings = DsamPort._setSettings(self)
        self.getTtyUsbNumber = DsamPort._getTtyUsbNumber(self)
        self.setState = DsamPort._setState(self)

#
# Section generated by IdlC from "DsamDevice.idl"
#

import raritan.rpc
from raritan.rpc import Interface, Structure, ValueObject, Enumeration, typecheck, DecodeException
import raritan.rpc.dsam


# interface
class DsamDevice(Interface):
    idlType = "dsam.DsamDevice:1.0.0"

    SUCCESS = 0

    # structure
    class FirmwareVersion(Structure):
        idlType = "dsam.DsamDevice.FirmwareVersion:1.0.0"
        elements = ["major", "minor"]

        def __init__(self, major=0, minor=0):
            typecheck.is_int(major, AssertionError)
            typecheck.is_int(minor, AssertionError)

            self.major = major
            self.minor = minor

        @classmethod
        def decode(cls, json, agent, useDefaults=False):
            obj = cls(
                major = json['major'] if 'major' in json or not useDefaults else 0,
                minor = json['minor'] if 'minor' in json or not useDefaults else 0,
            )
            return obj

        def encode(self):
            json = {}
            json['major'] = self.major
            json['minor'] = self.minor
            return json

    # structure
    class Info(Structure):
        idlType = "dsam.DsamDevice.Info:1.0.0"
        elements = ["dsamNumber", "serialNumber", "portCount", "hardwareVersion", "firmwareVersion"]

        def __init__(self, dsamNumber=0, serialNumber="", portCount=0, hardwareVersion=0, firmwareVersion=None):
            if firmwareVersion is None:
                firmwareVersion = raritan.rpc.dsam.DsamDevice.FirmwareVersion()
            typecheck.is_int(dsamNumber, AssertionError)
            if not typecheck._is_int(serialNumber):
                typecheck.is_string(serialNumber, AssertionError)
            typecheck.is_int(portCount, AssertionError)
            typecheck.is_int(hardwareVersion, AssertionError)
            typecheck.is_struct(firmwareVersion, raritan.rpc.dsam.DsamDevice.FirmwareVersion, AssertionError)

            self.dsamNumber = dsamNumber
            self.serialNumber = serialNumber
            self.portCount = portCount
            self.hardwareVersion = hardwareVersion
            self.firmwareVersion = firmwareVersion

        @classmethod
        def decode(cls, json, agent, useDefaults=False):
            obj = cls(
                dsamNumber = json['dsamNumber'] if 'dsamNumber' in json or not useDefaults else 0,
                serialNumber = json['serialNumber'] if 'serialNumber' in json or not useDefaults else "",
                portCount = json['portCount'] if 'portCount' in json or not useDefaults else 0,
                hardwareVersion = json['hardwareVersion'] if 'hardwareVersion' in json or not useDefaults else 0,
                firmwareVersion = raritan.rpc.dsam.DsamDevice.FirmwareVersion.decode(json['firmwareVersion'], agent, useDefaults=useDefaults) if 'firmwareVersion' in json or not useDefaults else raritan.rpc.dsam.DsamDevice.FirmwareVersion(),
            )
            return obj

        def encode(self):
            json = {}
            json['dsamNumber'] = self.dsamNumber
            json['serialNumber'] = str(self.serialNumber)
            json['portCount'] = self.portCount
            json['hardwareVersion'] = self.hardwareVersion
            json['firmwareVersion'] = raritan.rpc.dsam.DsamDevice.FirmwareVersion.encode(self.firmwareVersion)
            return json

    class _getInfo(Interface.Method):
        name = 'getInfo'

        @staticmethod
        def encode():
            args = {}
            return args

        @staticmethod
        def decode(rsp, agent, useDefaults=False):
            _ret_ = raritan.rpc.dsam.DsamDevice.Info.decode(rsp['_ret_'], agent, useDefaults=useDefaults)
            typecheck.is_struct(_ret_, raritan.rpc.dsam.DsamDevice.Info, DecodeException)
            return _ret_

    class _getPorts(Interface.Method):
        name = 'getPorts'

        @staticmethod
        def encode():
            args = {}
            return args

        @staticmethod
        def decode(rsp, agent, useDefaults=False):
            _ret_ = dict([(
                elem['key'],
                Interface.decode(elem['value'], agent))
                for elem in rsp['_ret_']])
            return _ret_

    class _startFirmwareUpdate(Interface.Method):
        name = 'startFirmwareUpdate'

        @staticmethod
        def encode():
            args = {}
            return args

        @staticmethod
        def decode(rsp, agent, useDefaults=False):
            _ret_ = rsp['_ret_']
            typecheck.is_int(_ret_, DecodeException)
            return _ret_
    def __init__(self, target, agent):
        super(DsamDevice, self).__init__(target, agent)
        self.getInfo = DsamDevice._getInfo(self)
        self.getPorts = DsamDevice._getPorts(self)
        self.startFirmwareUpdate = DsamDevice._startFirmwareUpdate(self)

#
# Section generated by IdlC from "DsamManager.idl"
#

import raritan.rpc
from raritan.rpc import Interface, Structure, ValueObject, Enumeration, typecheck, DecodeException
import raritan.rpc.dsam

import raritan.rpc.idl


# interface
class DsamManager(Interface):
    idlType = "dsam.DsamManager:1.0.0"

    # value object
    class DsamAttachedEvent(raritan.rpc.idl.Event):
        idlType = "dsam.DsamManager.DsamAttachedEvent:1.0.0"

        def __init__(self, info=None, device=None, source=None):
            super(raritan.rpc.dsam.DsamManager.DsamAttachedEvent, self).__init__(source)
            if info is None:
                info = raritan.rpc.dsam.DsamDevice.Info()
            typecheck.is_struct(info, raritan.rpc.dsam.DsamDevice.Info, AssertionError)
            typecheck.is_interface(device, raritan.rpc.dsam.DsamDevice, AssertionError)

            self.info = info
            self.device = device

        def encode(self):
            json = super(raritan.rpc.dsam.DsamManager.DsamAttachedEvent, self).encode()
            json['info'] = raritan.rpc.dsam.DsamDevice.Info.encode(self.info)
            json['device'] = Interface.encode(self.device)
            return json

        @classmethod
        def decode(cls, json, agent, useDefaults=False):
            obj = cls(
                info = raritan.rpc.dsam.DsamDevice.Info.decode(json['info'], agent, useDefaults=useDefaults) if 'info' in json or not useDefaults else raritan.rpc.dsam.DsamDevice.Info(),
                device = Interface.decode(json['device'], agent) if 'device' in json or not useDefaults else None,
                # for idl.Event
                source = Interface.decode(json['source'], agent) if 'source' in json or not useDefaults else None,
            )
            return obj

        def listElements(self):
            elements = ["info", "device"]
            elements = elements + super(raritan.rpc.dsam.DsamManager.DsamAttachedEvent, self).listElements()
            return elements

    # value object
    class DsamDetachedEvent(raritan.rpc.idl.Event):
        idlType = "dsam.DsamManager.DsamDetachedEvent:1.0.0"

        def __init__(self, info=None, source=None):
            super(raritan.rpc.dsam.DsamManager.DsamDetachedEvent, self).__init__(source)
            if info is None:
                info = raritan.rpc.dsam.DsamDevice.Info()
            typecheck.is_struct(info, raritan.rpc.dsam.DsamDevice.Info, AssertionError)

            self.info = info

        def encode(self):
            json = super(raritan.rpc.dsam.DsamManager.DsamDetachedEvent, self).encode()
            json['info'] = raritan.rpc.dsam.DsamDevice.Info.encode(self.info)
            return json

        @classmethod
        def decode(cls, json, agent, useDefaults=False):
            obj = cls(
                info = raritan.rpc.dsam.DsamDevice.Info.decode(json['info'], agent, useDefaults=useDefaults) if 'info' in json or not useDefaults else raritan.rpc.dsam.DsamDevice.Info(),
                # for idl.Event
                source = Interface.decode(json['source'], agent) if 'source' in json or not useDefaults else None,
            )
            return obj

        def listElements(self):
            elements = ["info"]
            elements = elements + super(raritan.rpc.dsam.DsamManager.DsamDetachedEvent, self).listElements()
            return elements

    # value object
    class DsamControllerChangedEvent(raritan.rpc.idl.Event):
        idlType = "dsam.DsamManager.DsamControllerChangedEvent:1.0.0"

        def __init__(self, dsamNumber=0, reset=False, resetReason="", source=None):
            super(raritan.rpc.dsam.DsamManager.DsamControllerChangedEvent, self).__init__(source)
            typecheck.is_int(dsamNumber, AssertionError)
            typecheck.is_bool(reset, AssertionError)
            if not typecheck._is_int(resetReason):
                typecheck.is_string(resetReason, AssertionError)

            self.dsamNumber = dsamNumber
            self.reset = reset
            self.resetReason = resetReason

        def encode(self):
            json = super(raritan.rpc.dsam.DsamManager.DsamControllerChangedEvent, self).encode()
            json['dsamNumber'] = self.dsamNumber
            json['reset'] = self.reset
            json['resetReason'] = str(self.resetReason)
            return json

        @classmethod
        def decode(cls, json, agent, useDefaults=False):
            obj = cls(
                dsamNumber = json['dsamNumber'] if 'dsamNumber' in json or not useDefaults else 0,
                reset = json['reset'] if 'reset' in json or not useDefaults else False,
                resetReason = json['resetReason'] if 'resetReason' in json or not useDefaults else "",
                # for idl.Event
                source = Interface.decode(json['source'], agent) if 'source' in json or not useDefaults else None,
            )
            return obj

        def listElements(self):
            elements = ["dsamNumber", "reset", "resetReason"]
            elements = elements + super(raritan.rpc.dsam.DsamManager.DsamControllerChangedEvent, self).listElements()
            return elements

    class _getDsamDevices(Interface.Method):
        name = 'getDsamDevices'

        @staticmethod
        def encode():
            args = {}
            return args

        @staticmethod
        def decode(rsp, agent, useDefaults=False):
            _ret_ = dict([(
                elem['key'],
                Interface.decode(elem['value'], agent))
                for elem in rsp['_ret_']])
            return _ret_

    class _getFirmwareUpdateFileVersion(Interface.Method):
        name = 'getFirmwareUpdateFileVersion'

        @staticmethod
        def encode():
            args = {}
            return args

        @staticmethod
        def decode(rsp, agent, useDefaults=False):
            _ret_ = raritan.rpc.dsam.DsamDevice.FirmwareVersion.decode(rsp['_ret_'], agent, useDefaults=useDefaults)
            typecheck.is_struct(_ret_, raritan.rpc.dsam.DsamDevice.FirmwareVersion, DecodeException)
            return _ret_
    def __init__(self, target, agent):
        super(DsamManager, self).__init__(target, agent)
        self.getDsamDevices = DsamManager._getDsamDevices(self)
        self.getFirmwareUpdateFileVersion = DsamManager._getFirmwareUpdateFileVersion(self)
