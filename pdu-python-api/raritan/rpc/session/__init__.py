# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright 2025 Raritan Inc. All rights reserved.
#
# This is an auto-generated file.

#
# Section generated by IdlC from "SessionManager.idl"
#

import raritan.rpc
from raritan.rpc import Interface, Structure, ValueObject, Enumeration, typecheck, DecodeException
import raritan.rpc.session


# structure
class Session(Structure):
    idlType = "session.Session:2.0.0"
    elements = ["sessionId", "username", "remoteIp", "clientType", "creationTime", "timeout", "idle", "userIdle"]

    def __init__(self, sessionId=0, username="", remoteIp="", clientType="", creationTime=None, timeout=0, idle=0, userIdle=0):
        typecheck.is_int(sessionId, AssertionError)
        if not typecheck._is_int(username):
            typecheck.is_string(username, AssertionError)
        if not typecheck._is_int(remoteIp):
            typecheck.is_string(remoteIp, AssertionError)
        if not typecheck._is_int(clientType):
            typecheck.is_string(clientType, AssertionError)
        typecheck.is_time(creationTime, AssertionError)
        typecheck.is_int(timeout, AssertionError)
        typecheck.is_int(idle, AssertionError)
        typecheck.is_int(userIdle, AssertionError)

        self.sessionId = sessionId
        self.username = username
        self.remoteIp = remoteIp
        self.clientType = clientType
        self.creationTime = creationTime
        self.timeout = timeout
        self.idle = idle
        self.userIdle = userIdle

    @classmethod
    def decode(cls, json, agent, useDefaults=False):
        obj = cls(
            sessionId = json['sessionId'] if 'sessionId' in json or not useDefaults else 0,
            username = json['username'] if 'username' in json or not useDefaults else "",
            remoteIp = json['remoteIp'] if 'remoteIp' in json or not useDefaults else "",
            clientType = json['clientType'] if 'clientType' in json or not useDefaults else "",
            creationTime = raritan.rpc.Time.decode(json['creationTime']) if 'creationTime' in json or not useDefaults else None,
            timeout = json['timeout'] if 'timeout' in json or not useDefaults else 0,
            idle = json['idle'] if 'idle' in json or not useDefaults else 0,
            userIdle = json['userIdle'] if 'userIdle' in json or not useDefaults else 0,
        )
        return obj

    def encode(self):
        json = {}
        json['sessionId'] = self.sessionId
        json['username'] = str(self.username)
        json['remoteIp'] = str(self.remoteIp)
        json['clientType'] = str(self.clientType)
        json['creationTime'] = raritan.rpc.Time.encode(self.creationTime)
        json['timeout'] = self.timeout
        json['idle'] = self.idle
        json['userIdle'] = self.userIdle
        return json

# structure
class HistoryEntry(Structure):
    idlType = "session.HistoryEntry:1.0.0"
    elements = ["creationTime", "remoteIp", "clientType"]

    def __init__(self, creationTime=None, remoteIp="", clientType=""):
        typecheck.is_time(creationTime, AssertionError)
        if not typecheck._is_int(remoteIp):
            typecheck.is_string(remoteIp, AssertionError)
        if not typecheck._is_int(clientType):
            typecheck.is_string(clientType, AssertionError)

        self.creationTime = creationTime
        self.remoteIp = remoteIp
        self.clientType = clientType

    @classmethod
    def decode(cls, json, agent, useDefaults=False):
        obj = cls(
            creationTime = raritan.rpc.Time.decode(json['creationTime']) if 'creationTime' in json or not useDefaults else None,
            remoteIp = json['remoteIp'] if 'remoteIp' in json or not useDefaults else "",
            clientType = json['clientType'] if 'clientType' in json or not useDefaults else "",
        )
        return obj

    def encode(self):
        json = {}
        json['creationTime'] = raritan.rpc.Time.encode(self.creationTime)
        json['remoteIp'] = str(self.remoteIp)
        json['clientType'] = str(self.clientType)
        return json

# interface
class SessionManager(Interface):
    idlType = "session.SessionManager:2.0.0"

    ERR_ACTIVE_SESSION_EXCLUSIVE_FOR_USER = 1

    # enumeration
    class CloseReason(Enumeration):
        idlType = "session.SessionManager_2_0_0.CloseReason:1.0.0"
        values = ["CLOSE_REASON_LOGOUT", "CLOSE_REASON_TIMEOUT", "CLOSE_REASON_BROWSER_CLOSED", "CLOSE_REASON_FORCED_DISCONNECT"]

    CloseReason.CLOSE_REASON_LOGOUT = CloseReason(0)
    CloseReason.CLOSE_REASON_TIMEOUT = CloseReason(1)
    CloseReason.CLOSE_REASON_BROWSER_CLOSED = CloseReason(2)
    CloseReason.CLOSE_REASON_FORCED_DISCONNECT = CloseReason(3)

    class _newSession(Interface.Method):
        name = 'newSession'

        @staticmethod
        def encode():
            args = {}
            return args

        @staticmethod
        def decode(rsp, agent, useDefaults=False):
            _ret_ = rsp['_ret_']
            session = raritan.rpc.session.Session.decode(rsp['session'], agent, useDefaults=useDefaults)
            token = rsp['token']
            typecheck.is_int(_ret_, DecodeException)
            typecheck.is_struct(session, raritan.rpc.session.Session, DecodeException)
            if not typecheck._is_int(token):
                typecheck.is_string(token, DecodeException)
            return (_ret_, session, token)

    class _getCurrentSession(Interface.Method):
        name = 'getCurrentSession'

        @staticmethod
        def encode():
            args = {}
            return args

        @staticmethod
        def decode(rsp, agent, useDefaults=False):
            _ret_ = raritan.rpc.session.Session.decode(rsp['_ret_'], agent, useDefaults=useDefaults)
            typecheck.is_struct(_ret_, raritan.rpc.session.Session, DecodeException)
            return _ret_

    class _getSessions(Interface.Method):
        name = 'getSessions'

        @staticmethod
        def encode():
            args = {}
            return args

        @staticmethod
        def decode(rsp, agent, useDefaults=False):
            _ret_ = [raritan.rpc.session.Session.decode(x0, agent, useDefaults=useDefaults) for x0 in rsp['_ret_']]
            for x0 in _ret_:
                typecheck.is_struct(x0, raritan.rpc.session.Session, DecodeException)
            return _ret_

    class _closeSession(Interface.Method):
        name = 'closeSession'

        @staticmethod
        def encode(sessionId, reason):
            typecheck.is_int(sessionId, AssertionError)
            typecheck.is_enum(reason, raritan.rpc.session.SessionManager.CloseReason, AssertionError)
            args = {}
            args['sessionId'] = sessionId
            args['reason'] = raritan.rpc.session.SessionManager.CloseReason.encode(reason)
            return args

        @staticmethod
        def decode(rsp, agent, useDefaults=False):
            return None

    class _closeCurrentSession(Interface.Method):
        name = 'closeCurrentSession'

        @staticmethod
        def encode(reason):
            typecheck.is_enum(reason, raritan.rpc.session.SessionManager.CloseReason, AssertionError)
            args = {}
            args['reason'] = raritan.rpc.session.SessionManager.CloseReason.encode(reason)
            return args

        @staticmethod
        def decode(rsp, agent, useDefaults=False):
            return None

    class _touchCurrentSession(Interface.Method):
        name = 'touchCurrentSession'

        @staticmethod
        def encode(userActivity):
            typecheck.is_bool(userActivity, AssertionError)
            args = {}
            args['userActivity'] = userActivity
            return args

        @staticmethod
        def decode(rsp, agent, useDefaults=False):
            return None

    class _getSessionHistory(Interface.Method):
        name = 'getSessionHistory'

        @staticmethod
        def encode():
            args = {}
            return args

        @staticmethod
        def decode(rsp, agent, useDefaults=False):
            _ret_ = [raritan.rpc.session.HistoryEntry.decode(x0, agent, useDefaults=useDefaults) for x0 in rsp['_ret_']]
            for x0 in _ret_:
                typecheck.is_struct(x0, raritan.rpc.session.HistoryEntry, DecodeException)
            return _ret_
    def __init__(self, target, agent):
        super(SessionManager, self).__init__(target, agent)
        self.newSession = SessionManager._newSession(self)
        self.getCurrentSession = SessionManager._getCurrentSession(self)
        self.getSessions = SessionManager._getSessions(self)
        self.closeSession = SessionManager._closeSession(self)
        self.closeCurrentSession = SessionManager._closeCurrentSession(self)
        self.touchCurrentSession = SessionManager._touchCurrentSession(self)
        self.getSessionHistory = SessionManager._getSessionHistory(self)
