Welcome to the Xerus JSON-RPC Python bindings. This project contains a library for developing programs using the JSON-RPC interface of Raritan and Server Technology Xerus products:

- Server Technology: PRO4X, PRO3X
- Raritan: PX4, PX4TS, PX3, PX3TS, PX2, SRC, PXO, PXC, BCM2

Xerus devices expose their full functionality via a JSON-RPC API. This API can be used for remote control, mass configuration or integration with other systems.

The full documentation can be found here: https://help.raritan.com/json-rpc/4.3.0/
