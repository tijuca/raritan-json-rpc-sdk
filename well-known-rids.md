Well-Known Resource IDs
=======================

All JSON-RPC requests must be directed to a URL on the device's HTTPS service.
The path component of this URL is called resource ID (RID). The list below
defines the resource IDs for interfaces that serve as entry points into the
API:

```py
# Python example: create a proxy for a well-known RID
from raritan.rpc import devsettings
snmp_proxy = devsettings.Snmp("/snmp", agent)
```

In contrast, object references returned from the device typically have cryptic
resource IDs like `/tfwopaque/pdumodel.Outlet:2.1.9/outlet.0`. These "opaque"
IDs are subject to change and should not be hard-coded in programs.

See the legend below for the meaning of placeholders like `<pdu_id>`.

| Resource ID                                                                         | Interface                                   | Comment                                      |
|-------------------------------------------------------------------------------------|---------------------------------------------|----------------------------------------------|
| `/model/inlet`                                                                      | ---                                         | alias for `/model/pdu/0/inlet`               |
| `/model/ocp`                                                                        | ---                                         | alias for `/model/pdu/0/ocp`                 |
| `/model/outlet`                                                                     | ---                                         | alias for `/model/pdu/0/outlet`              |
| `/model/outletgroup`                                                                | pdumodel.OutletGroupManager                 |                                              |
| `/model/outletgroup/<outlet_grp_id>`                                                | pdumodel.OutletGroup                        |                                              |
| `/model/outletgroup/<outlet_grp_id>/activePower`                                    | sensors.NumericSensor                       |                                              |
| `/model/outletgroup/<outlet_grp_id>/activeEnergy`                                   | sensors.NumericSensor                       |                                              |
| `/model/pdu/<pdu_id>`                                                               | pdumodel.Pdu                                |                                              |
| `/model/pdu/<pdu_id>/inlet/<inlet_id>`                                              | pdumodel.Inlet                              |                                              |
| `/model/pdu/<pdu_id>/inlet/<inlet_id>/activeEnergy`                                 | sensors.AccumulatingNumericSensor           |                                              |
| `/model/pdu/<pdu_id>/inlet/<inlet_id>/activePower`                                  | sensors.NumericSensor                       |                                              |
| `/model/pdu/<pdu_id>/inlet/<inlet_id>/apparentEnergy`                               | sensors.AccumulatingNumericSensor           |                                              |
| `/model/pdu/<pdu_id>/inlet/<inlet_id>/apparentPower`                                | sensors.NumericSensor                       |                                              |
| `/model/pdu/<pdu_id>/inlet/<inlet_id>/crestFactor`                                  | sensors.NumericSensor                       |                                              |
| `/model/pdu/<pdu_id>/inlet/<inlet_id>/current`                                      | sensors.NumericSensor                       |                                              |
| `/model/pdu/<pdu_id>/inlet/<inlet_id>/currentThd`                                   | sensors.NumericSensor                       |                                              |
| `/model/pdu/<pdu_id>/inlet/<inlet_id>/displacementPowerFactor`                      | sensors.NumericSensor                       |                                              |
| `/model/pdu/<pdu_id>/inlet/<inlet_id>/lineFrequency`                                | sensors.NumericSensor                       |                                              |
| `/model/pdu/<pdu_id>/inlet/<inlet_id>/peakCurrent`                                  | sensors.NumericSensor                       |                                              |
| `/model/pdu/<pdu_id>/inlet/<inlet_id>/phaseAngle`                                   | sensors.NumericSensor                       |                                              |
| `/model/pdu/<pdu_id>/inlet/<inlet_id>/linepair/<pair_id>/activeEnergy`              | sensors.AccumulatingNumericSensor           |                                              |
| `/model/pdu/<pdu_id>/inlet/<inlet_id>/linepair/<pair_id>/activePower`               | sensors.NumericSensor                       |                                              |
| `/model/pdu/<pdu_id>/inlet/<inlet_id>/linepair/<pair_id>/apparentEnergy`            | sensors.AccumulatingNumericSensor           |                                              |
| `/model/pdu/<pdu_id>/inlet/<inlet_id>/linepair/<pair_id>/apparentPower`             | sensors.NumericSensor                       |                                              |
| `/model/pdu/<pdu_id>/inlet/<inlet_id>/linepair/<pair_id>/crestFactor`               | sensors.NumericSensor                       |                                              |
| `/model/pdu/<pdu_id>/inlet/<inlet_id>/linepair/<pair_id>/current`                   | sensors.NumericSensor                       |                                              |
| `/model/pdu/<pdu_id>/inlet/<inlet_id>/linepair/<pair_id>/currentThd`                | sensors.NumericSensor                       |                                              |
| `/model/pdu/<pdu_id>/inlet/<inlet_id>/linepair/<pair_id>/displacementPowerFactor`   | sensors.NumericSensor                       |                                              |
| `/model/pdu/<pdu_id>/inlet/<inlet_id>/linepair/<pair_id>/peakCurrent`               | sensors.NumericSensor                       |                                              |
| `/model/pdu/<pdu_id>/inlet/<inlet_id>/linepair/<pair_id>/phaseAngle`                | sensors.NumericSensor                       |                                              |
| `/model/pdu/<pdu_id>/inlet/<inlet_id>/linepair/<pair_id>/powerFactor`               | sensors.NumericSensor                       |                                              |
| `/model/pdu/<pdu_id>/inlet/<inlet_id>/linepair/<pair_id>/reactivePower`             | sensors.NumericSensor                       |                                              |
| `/model/pdu/<pdu_id>/inlet/<inlet_id>/linepair/<pair_id>/voltage`                   | sensors.NumericSensor                       |                                              |
| `/model/pdu/<pdu_id>/inlet/<inlet_id>/linepair/<pair_id>/voltageThd`                | sensors.NumericSensor                       |                                              |
| `/model/pdu/<pdu_id>/inlet/<inlet_id>/pole/<pair_id>/activeEnergy`                  | sensors.AccumulatingNumericSensor           |                                              |
| `/model/pdu/<pdu_id>/inlet/<inlet_id>/pole/<pole_id>/activePower`                   | sensors.NumericSensor                       |                                              |
| `/model/pdu/<pdu_id>/inlet/<inlet_id>/pole/<pole_id>/apparentEnergy`                | sensors.AccumulatingNumericSensor           |                                              |
| `/model/pdu/<pdu_id>/inlet/<inlet_id>/pole/<pole_id>/apparentPower`                 | sensors.NumericSensor                       |                                              |
| `/model/pdu/<pdu_id>/inlet/<inlet_id>/pole/<pole_id>/crestFactor`                   | sensors.NumericSensor                       |                                              |
| `/model/pdu/<pdu_id>/inlet/<inlet_id>/pole/<pole_id>/current`                       | sensors.NumericSensor                       |                                              |
| `/model/pdu/<pdu_id>/inlet/<inlet_id>/pole/<pole_id>/currentThd`                    | sensors.NumericSensor                       |                                              |
| `/model/pdu/<pdu_id>/inlet/<inlet_id>/pole/<pole_id>/displacementPowerFactor`       | sensors.NumericSensor                       |                                              |
| `/model/pdu/<pdu_id>/inlet/<inlet_id>/pole/<pole_id>/peakCurrent`                   | sensors.NumericSensor                       |                                              |
| `/model/pdu/<pdu_id>/inlet/<inlet_id>/pole/<pole_id>/phaseAngle`                    | sensors.NumericSensor                       |                                              |
| `/model/pdu/<pdu_id>/inlet/<inlet_id>/pole/<pole_id>/powerFactor`                   | sensors.NumericSensor                       |                                              |
| `/model/pdu/<pdu_id>/inlet/<inlet_id>/pole/<pole_id>/reactivePower`                 | sensors.NumericSensor                       |                                              |
| `/model/pdu/<pdu_id>/inlet/<inlet_id>/pole/<pole_id>/voltage`                       | sensors.NumericSensor                       |                                              |
| `/model/pdu/<pdu_id>/inlet/<inlet_id>/pole/<pole_id>/voltageLN`                     | sensors.NumericSensor                       |                                              |
| `/model/pdu/<pdu_id>/inlet/<inlet_id>/pole/<pole_id>/voltageThd`                    | sensors.NumericSensor                       |                                              |
| `/model/pdu/<pdu_id>/inlet/<inlet_id>/powerFactor`                                  | sensors.NumericSensor                       |                                              |
| `/model/pdu/<pdu_id>/inlet/<inlet_id>/powerQuality`                                 | sensors.StateSensor                         |                                              |
| `/model/pdu/<pdu_id>/inlet/<inlet_id>/reactivePower`                                | sensors.NumericSensor                       |                                              |
| `/model/pdu/<pdu_id>/inlet/<inlet_id>/residualCurrent`                              | sensors.NumericSensor                       | models with RCM type A                       |
| `/model/pdu/<pdu_id>/inlet/<inlet_id>/residualCurrent`                              | pdumodel.TypeBResidualCurrentNumericSensor  | models with RCM type B                       |
| `/model/pdu/<pdu_id>/inlet/<inlet_id>/residualCurrentStatus`                        | pdumodel.ResidualCurrentStateSensor         |                                              |
| `/model/pdu/<pdu_id>/inlet/<inlet_id>/surgeProtectorStatus`                         | sensors.StateSensor                         |                                              |
| `/model/pdu/<pdu_id>/inlet/<inlet_id>/unbalancedCurrent`                            | sensors.NumericSensor                       |                                              |
| `/model/pdu/<pdu_id>/inlet/<inlet_id>/unbalancedLineLineCurrent`                    | sensors.NumericSensor                       |                                              |
| `/model/pdu/<pdu_id>/inlet/<inlet_id>/unbalancedVoltage`                            | sensors.NumericSensor                       |                                              |
| `/model/pdu/<pdu_id>/inlet/<inlet_id>/unbalancedLineLineVoltage`                    | sensors.NumericSensor                       |                                              |
| `/model/pdu/<pdu_id>/inlet/<inlet_id>/voltage`                                      | sensors.NumericSensor                       |                                              |
| `/model/pdu/<pdu_id>/inlet/<inlet_id>/voltageThd`                                   | sensors.NumericSensor                       |                                              |
| `/model/pdu/<pdu_id>/ocp/<ocp_id>`                                                  | pdumodel.OverCurrentProtector               |                                              |
| `/model/pdu/<pdu_id>/ocp/<ocp_id>/activeEnergy`                                     | sensors.AccumulatingNumericSensor           |                                              |
| `/model/pdu/<pdu_id>/ocp/<ocp_id>/activePower`                                      | sensors.NumericSensor                       |                                              |
| `/model/pdu/<pdu_id>/ocp/<ocp_id>/apparentEnergy`                                   | sensors.AccumulatingNumericSensor           |                                              |
| `/model/pdu/<pdu_id>/ocp/<ocp_id>/apparentPower`                                    | sensors.NumericSensor                       |                                              |
| `/model/pdu/<pdu_id>/ocp/<ocp_id>/current`                                          | sensors.NumericSensor                       |                                              |
| `/model/pdu/<pdu_id>/ocp/<ocp_id>/displacementPowerFactor`                          | sensors.NumericSensor                       |                                              |
| `/model/pdu/<pdu_id>/ocp/<ocp_id>/lineFrequency`                                    | sensors.NumericSensor                       |                                              |
| `/model/pdu/<pdu_id>/ocp/<ocp_id>/maximumCurrent`                                   | sensors.NumericSensor                       |                                              |
| `/model/pdu/<pdu_id>/ocp/<ocp_id>/peakCurrent`                                      | sensors.NumericSensor                       |                                              |
| `/model/pdu/<pdu_id>/ocp/<ocp_id>/phaseAngle`                                       | sensors.NumericSensor                       |                                              |
| `/model/pdu/<pdu_id>/ocp/<ocp_id>/powerFactor`                                      | sensors.NumericSensor                       |                                              |
| `/model/pdu/<pdu_id>/ocp/<ocp_id>/reactivePower`                                    | sensors.NumericSensor                       |                                              |
| `/model/pdu/<pdu_id>/ocp/<ocp_id>/voltage`                                          | sensors.NumericSensor                       |                                              |
| `/model/pdu/<pdu_id>/ocp/<ocp_id>/trip`                                             | pdumodel.OverCurrentProtectorTripSensor     |                                              |
| `/model/pdu/<pdu_id>/outlet/<outlet_id>`                                            | pdumodel.Outlet                             |                                              |
| `/model/pdu/<pdu_id>/outlet/<outlet_id>/activeEnergy`                               | sensors.AccumulatingNumericSensor           |                                              |
| `/model/pdu/<pdu_id>/outlet/<outlet_id>/activePower`                                | sensors.NumericSensor                       |                                              |
| `/model/pdu/<pdu_id>/outlet/<outlet_id>/apparentEnergy`                             | sensors.AccumulatingNumericSensor           |                                              |
| `/model/pdu/<pdu_id>/outlet/<outlet_id>/apparentPower`                              | sensors.NumericSensor                       |                                              |
| `/model/pdu/<pdu_id>/outlet/<outlet_id>/crestFactor`                                | sensors.NumericSensor                       |                                              |
| `/model/pdu/<pdu_id>/outlet/<outlet_id>/current`                                    | sensors.NumericSensor                       |                                              |
| `/model/pdu/<pdu_id>/outlet/<outlet_id>/currentThd`                                 | sensors.NumericSensor                       |                                              |
| `/model/pdu/<pdu_id>/outlet/<outlet_id>/displacementPowerFactor`                    | sensors.NumericSensor                       |                                              |
| `/model/pdu/<pdu_id>/outlet/<outlet_id>/inrushCurrent`                              | sensors.NumericSensor                       |                                              |
| `/model/pdu/<pdu_id>/outlet/<outlet_id>/lineFrequency`                              | sensors.NumericSensor                       |                                              |
| `/model/pdu/<pdu_id>/outlet/<outlet_id>/maximumCurrent`                             | sensors.NumericSensor                       |                                              |
| `/model/pdu/<pdu_id>/outlet/<outlet_id>/outletState`                                | sensors.StateSensor                         |                                              |
| `/model/pdu/<pdu_id>/outlet/<outlet_id>/peakCurrent`                                | sensors.NumericSensor                       |                                              |
| `/model/pdu/<pdu_id>/outlet/<outlet_id>/phaseAngle`                                 | sensors.NumericSensor                       |                                              |
| `/model/pdu/<pdu_id>/outlet/<outlet_id>/powerFactor`                                | sensors.NumericSensor                       |                                              |
| `/model/pdu/<pdu_id>/outlet/<outlet_id>/reactivePower`                              | sensors.NumericSensor                       |                                              |
| `/model/pdu/<pdu_id>/outlet/<outlet_id>/unbalancedCurrent`                          | sensors.NumericSensor                       |                                              |
| `/model/pdu/<pdu_id>/outlet/<outlet_id>/voltage`                                    | sensors.NumericSensor                       |                                              |
| `/model/pdu/<pdu_id>/outlet/<outlet_id>/voltageThd`                                 | sensors.NumericSensor                       |                                              |
| `/model/peripheraldevicemanager`                                                    | peripheral.DeviceManager                    |                                              |
| `/model/peripheraldeviceslot/<dev_slot>`                                            | peripheral.DeviceSlot                       |                                              |
| `/model/peripheraldeviceslot/<dev_slot>/device`                                     | sensors.Sensor                              |                                              |
| `/model/pmc`                                                                        | pdumodel.PowerMeterController               |                                              |
| `/model/unit`                                                                       | pdumodel.Unit                               |                                              |
| `/model/sensorlogger`                                                               | sensors.Logger                              |                                              |
| `/model/assetstriplogger`                                                           | assetmgrmodel.AssetStripLogger              |                                              |
| `/model/alertedsensormanager`                                                       | sensors.AlertedSensorManager                |                                              |
| `/auth`                                                                             | auth.AuthManager                            |                                              |
| `/auth/currentUser`                                                                 | usermgmt.User                               | currently authenticated user                 |
| `/auth/ldap`                                                                        | auth.LdapManager                            |                                              |
| `/auth/radius`                                                                      | auth.RadiusManager                          |                                              |
| `/auth/role`                                                                        | usermgmt.RoleManager                        |                                              |
| `/auth/role/<role_name>`                                                            | usermgmt.Role                               |                                              |
| `/auth/tacplus`                                                                     | auth.TacPlusManager                         |                                              |
| `/auth/user`                                                                        | usermgmt.UserManager                        |                                              |
| `/auth/user/<user_name>`                                                            | usermgmt.User                               |                                              |
| `/bulk`                                                                             | bulkrpc.BulkRequest                         |                                              |
| `/bulkcfg`                                                                          | bulkcfg.BulkConfiguration                   |                                              |
| `/cardreader`                                                                       | smartcard.CardReaderManager                 |                                              |
| `/cascade`                                                                          | cascading.CascadeManager                    |                                              |
| `/crestron`                                                                         | devsettings.Crestron                        |                                              |
| `/datapush`                                                                         | event.DataPushService                       |                                              |
| `/datetime`                                                                         | datetime.DateTime                           |                                              |
| `/debuglog`                                                                         | logging.DebugLog                            |                                              |
| `/diag`                                                                             | diag.DiagLogSettings                        |                                              |
| `/display`                                                                          | display.DisplayControl                      |                                              |
| `/dooraccesscontrol`                                                                | smartlock.DoorAccessControl                 |                                              |
| `/event_engine`                                                                     | event.Engine                                |                                              |
| `/event_engine/alarms`                                                              | event.AlarmManager                          |                                              |
| `/event_engine/timer_events`                                                        | event.TimerEventManager                     |                                              |
| `/eventlog`                                                                         | logging.EventLog                            |                                              |
| `/eventservice`                                                                     | event.Service                               |                                              |
| `/firmware`                                                                         | firmware.Firmware                           |                                              |
| `/cgi-bin/fwupdate_progress.cgi`                                                    | firmware.UpdateStatus                       |                                              |
| `/fitness`                                                                          | fitness.Fitness                             |                                              |
| `/hwhealth`                                                                         | fitness.HardwareHealth                      |                                              |
| `/keypad`                                                                           | smartlock.KeypadManager                     |                                              |
| `/luaservice`                                                                       | luaservice.Manager                          |                                              |
| `/modbus`                                                                           | devsettings.Modbus                          |                                              |
| `/modbus/gateway`                                                                   | modbus.GatewayMgr                           |                                              |
| `/net`                                                                              | net.Net                                     |                                              |
| `/net/diag`                                                                         | net.Diagnostics                             |                                              |
| `/net/services`                                                                     | net.Services                                |                                              |
| `/ns`                                                                               | jsonrpc.NameService                         |                                              |
| `/portForwardingAutoSetup`                                                          | net.PortForwardingAutoSetup                 |                                              |
| `/production`                                                                       | production.Production                       |                                              |
| `/production/g2_dev`                                                                | peripheral.G2Production                     |                                              |
| `/rawcfg`                                                                           | rawcfg.RawConfiguration                     |                                              |
| `/redfishcfg`                                                                       | devsettings.Redfish                         | `/redfish` is the published API              |
| `/res_mon`                                                                          | res_mon.ResMon                              |                                              |
| `/security`                                                                         | security.Security                           |                                              |
| `/serial`                                                                           | serial.PortDispatcher                       |                                              |
| `/serial/<serial_port>`                                                             | serial.SerialPort                           |                                              |
| `/server_ssl_cert`                                                                  | cert.ServerSSLCert                          |                                              |
| `/servermon`                                                                        | servermon.ServerMonitor                     |                                              |
| `/session`                                                                          | session.SessionManager                      |                                              |
| `/link/<link_id>/<rid>`                                                             | ---                                         | fowarded to `<rid>` on link unit `<link_id>` |
| `/smtp`                                                                             | devsettings.Smtp                            |                                              |
| `/snmp`                                                                             | devsettings.Snmp                            |                                              |
| `/storage_mgr`                                                                      | webcam.StorageManager                       |                                              |
| `/system`                                                                           | sys.System                                  |                                              |
| `/test/Unit`                                                                        | test.Unit                                   |                                              |
| `/test/Usb`                                                                         | usb.Usb                                     | alias for `/usb`                             |
| `/usb`                                                                              | usb.Usb                                     |                                              |
| `/webcam_mgr`                                                                       | webcam.WebcamManager                        |                                              |
| `/wlanlog`                                                                          | logging.WlanLog                             |                                              |
| `/zeroconf`                                                                         | devsettings.Zeroconf                        |                                              |

## Legend

| Placeholder       | Meaning                                                  |
|-------------------|----------------------------------------------------------|
| `<pdu_id>`        | ID of PDU (0 or 1 for primary unit, 2..8 for link units) |
| `<inlet_id>`      | ID of inlet (0-based)                                    |
| `<pair_id>`       | ID of line pair                                          |
| `<ocp_id>`        | ID of overcurrent protector (0-based)                    |
| `<outlet_id>`     | ID of outlet (0-based)                                   |
| `<outlet_grp_id>` | ID of outlet group                                       |
| `<pole_id>`       | ID of pole                                               |
| `<user_name>`     | Login name of user                                       |
| `<role_name>`     | Name of user role                                        |
| `<dev_slot>`      | Index of peripheral device slot (0-based)                |
| `<serial_port>`   | Name of serial port, usually `main`                      |
| `<link_id>`       | ID of link unit (2..8)                                   |
| `<rid>`           | Any valid JSON-RPC RID on link unit                      |
