// SPDX-License-Identifier: BSD-3-Clause
//
// Copyright 2025 Raritan Inc. All rights reserved.
//
// This file was generated by IdlC from GatewaySensorManager.idl.

using System;
using System.Linq;
using LightJson;
using Com.Raritan.Idl;
using Com.Raritan.JsonRpc;
using Com.Raritan.Util;

#pragma warning disable 0108, 0219, 0414, 1591

namespace Com.Raritan.JsonRpc.peripheral.GatewaySensorManager_2_0_0 {
  public class InterpretationRule_ValObjCodec : ValueObjectCodec
 {

    public static new void Register() {
      ValueObjectCodec.RegisterCodec(Com.Raritan.Idl.peripheral.GatewaySensorManager_2_0_0.InterpretationRule.typeInfo, new InterpretationRule_ValObjCodec());
    }

    public override void EncodeValObj(LightJson.JsonObject json, ValueObject vo) {
      var inst = (Com.Raritan.Idl.peripheral.GatewaySensorManager_2_0_0.InterpretationRule)vo;
      json["interpretation"] = (int)inst.interpretation;
      json["ignoreTimeout"] = inst.ignoreTimeout;
    }

    public override ValueObject DecodeValObj(ValueObject vo, LightJson.JsonObject json, Agent agent) {
      Com.Raritan.Idl.peripheral.GatewaySensorManager_2_0_0.InterpretationRule inst;
      if (vo == null) {
        inst = new Com.Raritan.Idl.peripheral.GatewaySensorManager_2_0_0.InterpretationRule();
      } else {
        inst = (Com.Raritan.Idl.peripheral.GatewaySensorManager_2_0_0.InterpretationRule)vo;
      }
      inst.interpretation = (Com.Raritan.Idl.peripheral.GatewaySensorManager_2_0_0.Interpretation)(int)json["interpretation"];
      inst.ignoreTimeout = (int)json["ignoreTimeout"];
      return inst;
    }

  }
}
