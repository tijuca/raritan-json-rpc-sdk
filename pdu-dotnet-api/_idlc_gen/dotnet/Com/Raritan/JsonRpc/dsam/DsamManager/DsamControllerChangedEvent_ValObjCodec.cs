// SPDX-License-Identifier: BSD-3-Clause
//
// Copyright 2025 Raritan Inc. All rights reserved.
//
// This file was generated by IdlC from DsamManager.idl.

using System;
using System.Linq;
using LightJson;
using Com.Raritan.Idl;
using Com.Raritan.JsonRpc;
using Com.Raritan.Util;

#pragma warning disable 0108, 0219, 0414, 1591

namespace Com.Raritan.JsonRpc.dsam.DsamManager {
  public class DsamControllerChangedEvent_ValObjCodec : Com.Raritan.JsonRpc.idl.Event_ValObjCodec {

    public static new void Register() {
      ValueObjectCodec.RegisterCodec(Com.Raritan.Idl.dsam.DsamManager.DsamControllerChangedEvent.typeInfo, new DsamControllerChangedEvent_ValObjCodec());
    }

    public override void EncodeValObj(LightJson.JsonObject json, ValueObject vo) {
      base.EncodeValObj(json, vo);
      var inst = (Com.Raritan.Idl.dsam.DsamManager.DsamControllerChangedEvent)vo;
      json["dsamNumber"] = inst.dsamNumber;
      json["reset"] = inst.reset;
      json["resetReason"] = inst.resetReason;
    }

    public override ValueObject DecodeValObj(ValueObject vo, LightJson.JsonObject json, Agent agent) {
      Com.Raritan.Idl.dsam.DsamManager.DsamControllerChangedEvent inst;
      if (vo == null) {
        inst = new Com.Raritan.Idl.dsam.DsamManager.DsamControllerChangedEvent();
      } else {
        inst = (Com.Raritan.Idl.dsam.DsamManager.DsamControllerChangedEvent)vo;
      }
      base.DecodeValObj(vo, json, agent);
      inst.dsamNumber = (int)json["dsamNumber"];
      inst.reset = (bool)json["reset"];
      inst.resetReason = (string)json["resetReason"];
      return inst;
    }

  }
}
