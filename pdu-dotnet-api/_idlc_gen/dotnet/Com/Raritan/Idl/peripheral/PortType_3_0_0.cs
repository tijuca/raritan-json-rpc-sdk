// SPDX-License-Identifier: BSD-3-Clause
//
// Copyright 2025 Raritan Inc. All rights reserved.
//
// This file was generated by IdlC from PeripheralDeviceSlot.idl.

using System;
using System.Linq;
using LightJson;
using Com.Raritan.Idl;
using Com.Raritan.JsonRpc;
using Com.Raritan.Util;

#pragma warning disable 0108, 0219, 0414, 1591

namespace Com.Raritan.Idl.peripheral {

  public enum PortType_3_0_0 {
    ONBOARD,
    DEV_PORT,
    ONEWIRE_HUB_PORT,
    ONEWIRE_CHAIN_POS,
    REMOTE_HUB_PORT,
    WIRELESS_BRIDGE,
    WIRELESS_DEVICE,
    UNSPECIFIED,
  }
}
