// SPDX-License-Identifier: BSD-3-Clause
//
// Copyright 2025 Raritan Inc. All rights reserved.
//
// This file was generated by IdlC from PortForwardingAutoSetup.idl.

using System;
using System.Linq;
using LightJson;
using Com.Raritan.Idl;
using Com.Raritan.JsonRpc;
using Com.Raritan.Util;

#pragma warning disable 0108, 0219, 0414, 1591

namespace Com.Raritan.Idl.net {
  public class PortForwardingAutoSetup : ObjectProxy {

    static public readonly new TypeInfo typeInfo = new TypeInfo("net.PortForwardingAutoSetup:1.0.0", null);

    public PortForwardingAutoSetup(Agent agent, string rid, TypeInfo ti) : base(agent, rid, ti) {}
    public PortForwardingAutoSetup(Agent agent, string rid) : this(agent, rid, typeInfo) {}

    public static new PortForwardingAutoSetup StaticCast(ObjectProxy proxy) {
      return proxy == null ? null : new PortForwardingAutoSetup(proxy.Agent, proxy.Rid, proxy.StaticTypeInfo);
    }

    public const int SUCCESS = 0;

    public const int ERR_PF_AUTO_SETUP_NOT_A_PRIMARY_UNIT = 1;

    public const int ERR_PF_AUTO_SETUP_ALREADY_RUNNING = 2;

    public const int ERR_PF_AUTO_SETUP_INVALID_PARAM = 3;

    public class StartResult {
      public int _ret_;
    }

    public StartResult start(int numberOfExpansionUnits, string username, string password, string newPassword, bool disableStrongPasswordReq) {
      var _parameters = new LightJson.JsonObject();
      _parameters["numberOfExpansionUnits"] = numberOfExpansionUnits;
      _parameters["username"] = username;
      _parameters["password"] = password;
      _parameters["newPassword"] = newPassword;
      _parameters["disableStrongPasswordReq"] = disableStrongPasswordReq;

      var _result = RpcCall("start", _parameters);
      var _ret = new StartResult();
      _ret._ret_ = (int)_result["_ret_"];
      return _ret;
    }

    public AsyncRequest start(int numberOfExpansionUnits, string username, string password, string newPassword, bool disableStrongPasswordReq, AsyncRpcResponse<StartResult>.SuccessHandler rsp, AsyncRpcResponse.FailureHandler fail) {
      return start(numberOfExpansionUnits, username, password, newPassword, disableStrongPasswordReq, rsp, fail, RpcCtrl.Default);
    }

    public AsyncRequest start(int numberOfExpansionUnits, string username, string password, string newPassword, bool disableStrongPasswordReq, AsyncRpcResponse<StartResult>.SuccessHandler rsp, AsyncRpcResponse.FailureHandler fail, RpcCtrl rpcCtrl) {
      var _parameters = new LightJson.JsonObject();
      try {
        _parameters["numberOfExpansionUnits"] = numberOfExpansionUnits;
        _parameters["username"] = username;
        _parameters["password"] = password;
        _parameters["newPassword"] = newPassword;
        _parameters["disableStrongPasswordReq"] = disableStrongPasswordReq;
      } catch (Exception e) {
        if (fail != null) fail(e);
      }

      return RpcCall("start", _parameters,
        _result => {
          try {
            var _ret = new StartResult();
            _ret._ret_ = (int)_result["_ret_"];
            rsp(_ret);
          } catch (Exception e) {
            if (fail != null) fail(e);
          }
        }, fail, rpcCtrl);
    }

    public class GetStatusResult {
      public Com.Raritan.Idl.net.PortForwardingAutoSetupStatus _ret_;
    }

    public GetStatusResult getStatus() {
      JsonObject _parameters = null;
      var _result = RpcCall("getStatus", _parameters);
      var _ret = new GetStatusResult();
      _ret._ret_ = Com.Raritan.Idl.net.PortForwardingAutoSetupStatus.Decode(_result["_ret_"], agent);
      return _ret;
    }

    public AsyncRequest getStatus(AsyncRpcResponse<GetStatusResult>.SuccessHandler rsp, AsyncRpcResponse.FailureHandler fail) {
      return getStatus(rsp, fail, RpcCtrl.Default);
    }

    public AsyncRequest getStatus(AsyncRpcResponse<GetStatusResult>.SuccessHandler rsp, AsyncRpcResponse.FailureHandler fail, RpcCtrl rpcCtrl) {
      JsonObject _parameters = null;
      return RpcCall("getStatus", _parameters,
        _result => {
          try {
            var _ret = new GetStatusResult();
            _ret._ret_ = Com.Raritan.Idl.net.PortForwardingAutoSetupStatus.Decode(_result["_ret_"], agent);
            rsp(_ret);
          } catch (Exception e) {
            if (fail != null) fail(e);
          }
        }, fail, rpcCtrl);
    }

    public class CancelResult {
    }

    public CancelResult cancel() {
      JsonObject _parameters = null;
      var _result = RpcCall("cancel", _parameters);
      var _ret = new CancelResult();
      return _ret;
    }

    public AsyncRequest cancel(AsyncRpcResponse<CancelResult>.SuccessHandler rsp, AsyncRpcResponse.FailureHandler fail) {
      return cancel(rsp, fail, RpcCtrl.Default);
    }

    public AsyncRequest cancel(AsyncRpcResponse<CancelResult>.SuccessHandler rsp, AsyncRpcResponse.FailureHandler fail, RpcCtrl rpcCtrl) {
      JsonObject _parameters = null;
      return RpcCall("cancel", _parameters,
        _result => {
          try {
            var _ret = new CancelResult();
            rsp(_ret);
          } catch (Exception e) {
            if (fail != null) fail(e);
          }
        }, fail, rpcCtrl);
    }

  }
}
