// SPDX-License-Identifier: BSD-3-Clause
//
// Copyright 2025 Raritan Inc. All rights reserved.
//
// This file was generated by IdlC from Net.idl.

using System;
using System.Linq;
using LightJson;
using Com.Raritan.Idl;
using Com.Raritan.JsonRpc;
using Com.Raritan.Util;

#pragma warning disable 0108, 0219, 0414, 1591

namespace Com.Raritan.Idl.net {

  public class PortForwardingInfo_3_0_0 : ICloneable {
    public object Clone() {
      PortForwardingInfo_3_0_0 copy = new PortForwardingInfo_3_0_0();
      copy.enabled = this.enabled;
      copy.nodeIndexValid = this.nodeIndexValid;
      copy.nodeIndex = this.nodeIndex;
      copy.expansionUnitConnected = this.expansionUnitConnected;
      copy.primaryUnitDownstreamIfName = this.primaryUnitDownstreamIfName;
      copy.primaryUnitIPv4AddrInfos = this.primaryUnitIPv4AddrInfos;
      copy.primaryUnitIPv6AddrInfos = this.primaryUnitIPv6AddrInfos;
      copy.linkLocalIPv6Address = this.linkLocalIPv6Address;
      return copy;
    }

    public LightJson.JsonObject Encode() {
      LightJson.JsonObject json = new LightJson.JsonObject();
      json["enabled"] = this.enabled;
      json["nodeIndexValid"] = this.nodeIndexValid;
      json["nodeIndex"] = this.nodeIndex;
      json["expansionUnitConnected"] = this.expansionUnitConnected;
      json["primaryUnitDownstreamIfName"] = this.primaryUnitDownstreamIfName;
      json["primaryUnitIPv4AddrInfos"] = new JsonArray(this.primaryUnitIPv4AddrInfos.Select(
        _value => (JsonValue)(_value.Encode())));
      json["primaryUnitIPv6AddrInfos"] = new JsonArray(this.primaryUnitIPv6AddrInfos.Select(
        _value => (JsonValue)(_value.Encode())));
      json["linkLocalIPv6Address"] = this.linkLocalIPv6Address;
      return json;
    }

    public static PortForwardingInfo_3_0_0 Decode(LightJson.JsonObject json, Agent agent) {
      PortForwardingInfo_3_0_0 inst = new PortForwardingInfo_3_0_0();
      inst.enabled = (bool)json["enabled"];
      inst.nodeIndexValid = (bool)json["nodeIndexValid"];
      inst.nodeIndex = (int)json["nodeIndex"];
      inst.expansionUnitConnected = (bool)json["expansionUnitConnected"];
      inst.primaryUnitDownstreamIfName = (string)json["primaryUnitDownstreamIfName"];
      inst.primaryUnitIPv4AddrInfos = new System.Collections.Generic.List<Com.Raritan.Idl.net.PortForwardingPrimaryUnitAddrInfo>(json["primaryUnitIPv4AddrInfos"].AsJsonArray.Select(
        _value => Com.Raritan.Idl.net.PortForwardingPrimaryUnitAddrInfo.Decode(_value, agent)));
      inst.primaryUnitIPv6AddrInfos = new System.Collections.Generic.List<Com.Raritan.Idl.net.PortForwardingPrimaryUnitAddrInfo>(json["primaryUnitIPv6AddrInfos"].AsJsonArray.Select(
        _value => Com.Raritan.Idl.net.PortForwardingPrimaryUnitAddrInfo.Decode(_value, agent)));
      inst.linkLocalIPv6Address = (string)json["linkLocalIPv6Address"];
      return inst;
    }

    public bool enabled = false;
    public bool nodeIndexValid = false;
    public int nodeIndex = 0;
    public bool expansionUnitConnected = false;
    public string primaryUnitDownstreamIfName = "";
    public System.Collections.Generic.IEnumerable<Com.Raritan.Idl.net.PortForwardingPrimaryUnitAddrInfo> primaryUnitIPv4AddrInfos = new System.Collections.Generic.List<Com.Raritan.Idl.net.PortForwardingPrimaryUnitAddrInfo>();
    public System.Collections.Generic.IEnumerable<Com.Raritan.Idl.net.PortForwardingPrimaryUnitAddrInfo> primaryUnitIPv6AddrInfos = new System.Collections.Generic.List<Com.Raritan.Idl.net.PortForwardingPrimaryUnitAddrInfo>();
    public string linkLocalIPv6Address = "";
  }
}
