// SPDX-License-Identifier: BSD-3-Clause
//
// Copyright 2025 Raritan Inc. All rights reserved.
//
// This file was generated by IdlC from DsamDevice.idl.

using System;
using System.Linq;
using LightJson;
using Com.Raritan.Idl;
using Com.Raritan.JsonRpc;
using Com.Raritan.Util;

#pragma warning disable 0108, 0219, 0414, 1591

namespace Com.Raritan.Idl.dsam {
  public class DsamDevice : ObjectProxy {

    static public readonly new TypeInfo typeInfo = new TypeInfo("dsam.DsamDevice:1.0.0", null);

    public DsamDevice(Agent agent, string rid, TypeInfo ti) : base(agent, rid, ti) {}
    public DsamDevice(Agent agent, string rid) : this(agent, rid, typeInfo) {}

    public static new DsamDevice StaticCast(ObjectProxy proxy) {
      return proxy == null ? null : new DsamDevice(proxy.Agent, proxy.Rid, proxy.StaticTypeInfo);
    }

    public const int SUCCESS = 0;

    public class FirmwareVersion : ICloneable {
      public object Clone() {
        FirmwareVersion copy = new FirmwareVersion();
        copy.major = this.major;
        copy.minor = this.minor;
        return copy;
      }

      public LightJson.JsonObject Encode() {
        LightJson.JsonObject json = new LightJson.JsonObject();
        json["major"] = this.major;
        json["minor"] = this.minor;
        return json;
      }

      public static FirmwareVersion Decode(LightJson.JsonObject json, Agent agent) {
        FirmwareVersion inst = new FirmwareVersion();
        inst.major = (int)json["major"];
        inst.minor = (int)json["minor"];
        return inst;
      }

      public int major = 0;
      public int minor = 0;
    }

    public class Info : ICloneable {
      public object Clone() {
        Info copy = new Info();
        copy.dsamNumber = this.dsamNumber;
        copy.serialNumber = this.serialNumber;
        copy.portCount = this.portCount;
        copy.hardwareVersion = this.hardwareVersion;
        copy.firmwareVersion = this.firmwareVersion;
        return copy;
      }

      public LightJson.JsonObject Encode() {
        LightJson.JsonObject json = new LightJson.JsonObject();
        json["dsamNumber"] = this.dsamNumber;
        json["serialNumber"] = this.serialNumber;
        json["portCount"] = this.portCount;
        json["hardwareVersion"] = this.hardwareVersion;
        json["firmwareVersion"] = this.firmwareVersion.Encode();
        return json;
      }

      public static Info Decode(LightJson.JsonObject json, Agent agent) {
        Info inst = new Info();
        inst.dsamNumber = (int)json["dsamNumber"];
        inst.serialNumber = (string)json["serialNumber"];
        inst.portCount = (int)json["portCount"];
        inst.hardwareVersion = (int)json["hardwareVersion"];
        inst.firmwareVersion = Com.Raritan.Idl.dsam.DsamDevice.FirmwareVersion.Decode(json["firmwareVersion"], agent);
        return inst;
      }

      public int dsamNumber = 0;
      public string serialNumber = "";
      public int portCount = 0;
      public int hardwareVersion = 0;
      public Com.Raritan.Idl.dsam.DsamDevice.FirmwareVersion firmwareVersion = new Com.Raritan.Idl.dsam.DsamDevice.FirmwareVersion();
    }

    public class GetInfoResult {
      public Com.Raritan.Idl.dsam.DsamDevice.Info _ret_;
    }

    public GetInfoResult getInfo() {
      JsonObject _parameters = null;
      var _result = RpcCall("getInfo", _parameters);
      var _ret = new GetInfoResult();
      _ret._ret_ = Com.Raritan.Idl.dsam.DsamDevice.Info.Decode(_result["_ret_"], agent);
      return _ret;
    }

    public AsyncRequest getInfo(AsyncRpcResponse<GetInfoResult>.SuccessHandler rsp, AsyncRpcResponse.FailureHandler fail) {
      return getInfo(rsp, fail, RpcCtrl.Default);
    }

    public AsyncRequest getInfo(AsyncRpcResponse<GetInfoResult>.SuccessHandler rsp, AsyncRpcResponse.FailureHandler fail, RpcCtrl rpcCtrl) {
      JsonObject _parameters = null;
      return RpcCall("getInfo", _parameters,
        _result => {
          try {
            var _ret = new GetInfoResult();
            _ret._ret_ = Com.Raritan.Idl.dsam.DsamDevice.Info.Decode(_result["_ret_"], agent);
            rsp(_ret);
          } catch (Exception e) {
            if (fail != null) fail(e);
          }
        }, fail, rpcCtrl);
    }

    public class GetPortsResult {
      public System.Collections.Generic.IDictionary<int, Com.Raritan.Idl.dsam.DsamPort> _ret_;
    }

    public GetPortsResult getPorts() {
      JsonObject _parameters = null;
      var _result = RpcCall("getPorts", _parameters);
      var _ret = new GetPortsResult();
      _ret._ret_ = DictionaryHelper.Create(_result["_ret_"].AsJsonArray.Select(
        _value => new System.Collections.Generic.KeyValuePair<int, Com.Raritan.Idl.dsam.DsamPort>(_value["key"], Com.Raritan.Idl.dsam.DsamPort.StaticCast(ObjectProxy.Decode(_value["value"], agent)))));
      return _ret;
    }

    public AsyncRequest getPorts(AsyncRpcResponse<GetPortsResult>.SuccessHandler rsp, AsyncRpcResponse.FailureHandler fail) {
      return getPorts(rsp, fail, RpcCtrl.Default);
    }

    public AsyncRequest getPorts(AsyncRpcResponse<GetPortsResult>.SuccessHandler rsp, AsyncRpcResponse.FailureHandler fail, RpcCtrl rpcCtrl) {
      JsonObject _parameters = null;
      return RpcCall("getPorts", _parameters,
        _result => {
          try {
            var _ret = new GetPortsResult();
            _ret._ret_ = DictionaryHelper.Create(_result["_ret_"].AsJsonArray.Select(
              _value => new System.Collections.Generic.KeyValuePair<int, Com.Raritan.Idl.dsam.DsamPort>(_value["key"], Com.Raritan.Idl.dsam.DsamPort.StaticCast(ObjectProxy.Decode(_value["value"], agent)))));
            rsp(_ret);
          } catch (Exception e) {
            if (fail != null) fail(e);
          }
        }, fail, rpcCtrl);
    }

    public class StartFirmwareUpdateResult {
      public int _ret_;
    }

    public StartFirmwareUpdateResult startFirmwareUpdate() {
      JsonObject _parameters = null;
      var _result = RpcCall("startFirmwareUpdate", _parameters);
      var _ret = new StartFirmwareUpdateResult();
      _ret._ret_ = (int)_result["_ret_"];
      return _ret;
    }

    public AsyncRequest startFirmwareUpdate(AsyncRpcResponse<StartFirmwareUpdateResult>.SuccessHandler rsp, AsyncRpcResponse.FailureHandler fail) {
      return startFirmwareUpdate(rsp, fail, RpcCtrl.Default);
    }

    public AsyncRequest startFirmwareUpdate(AsyncRpcResponse<StartFirmwareUpdateResult>.SuccessHandler rsp, AsyncRpcResponse.FailureHandler fail, RpcCtrl rpcCtrl) {
      JsonObject _parameters = null;
      return RpcCall("startFirmwareUpdate", _parameters,
        _result => {
          try {
            var _ret = new StartFirmwareUpdateResult();
            _ret._ret_ = (int)_result["_ret_"];
            rsp(_ret);
          } catch (Exception e) {
            if (fail != null) fail(e);
          }
        }, fail, rpcCtrl);
    }

  }
}
