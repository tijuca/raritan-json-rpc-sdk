// SPDX-License-Identifier: BSD-3-Clause
//
// Copyright 2025 Raritan Inc. All rights reserved.
//
// This file was generated by IdlC from ZigbeeDevice.idl.

using System;
using System.Linq;
using LightJson;
using Com.Raritan.Idl;
using Com.Raritan.JsonRpc;
using Com.Raritan.Util;

#pragma warning disable 0108, 0219, 0414, 1591

namespace Com.Raritan.Idl.zigbee {
  public class ZigbeeDevice : ObjectProxy {

    static public readonly new TypeInfo typeInfo = new TypeInfo("zigbee.ZigbeeDevice:1.0.0", null);

    public ZigbeeDevice(Agent agent, string rid, TypeInfo ti) : base(agent, rid, ti) {}
    public ZigbeeDevice(Agent agent, string rid) : this(agent, rid, typeInfo) {}

    public static new ZigbeeDevice StaticCast(ObjectProxy proxy) {
      return proxy == null ? null : new ZigbeeDevice(proxy.Agent, proxy.Rid, proxy.StaticTypeInfo);
    }

    public const int CLUSTER_BASIC = 0x0;

    public const int CLUSTER_POWER_CONFIGURATION = 0x1;

    public const int CLUSTER_RSSI = 0xb;

    public const int CLUSTER_CONTACT_CLOSURE = 0xf;

    public const int CLUSTER_TEMPERATURE = 0x402;

    public const int CLUSTER_HUMIDITY = 0x405;

    public class MetaData : ICloneable {
      public object Clone() {
        MetaData copy = new MetaData();
        copy.sourceId = this.sourceId;
        copy.clusters = this.clusters;
        copy.preferredSlot = this.preferredSlot;
        return copy;
      }

      public LightJson.JsonObject Encode() {
        LightJson.JsonObject json = new LightJson.JsonObject();
        json["sourceId"] = this.sourceId;
        json["clusters"] = new JsonArray(this.clusters.Select(
          _value => (JsonValue)(_value)));
        json["preferredSlot"] = this.preferredSlot;
        return json;
      }

      public static MetaData Decode(LightJson.JsonObject json, Agent agent) {
        MetaData inst = new MetaData();
        inst.sourceId = (int)json["sourceId"];
        inst.clusters = new System.Collections.Generic.List<int>(json["clusters"].AsJsonArray.Select(
          _value => (int)_value));
        inst.preferredSlot = (int)json["preferredSlot"];
        return inst;
      }

      public int sourceId = 0;
      public System.Collections.Generic.IEnumerable<int> clusters = new System.Collections.Generic.List<int>();
      public int preferredSlot = 0;
    }

    public class ClusterValue : ICloneable {
      public object Clone() {
        ClusterValue copy = new ClusterValue();
        copy.id = this.id;
        copy.endpoint = this.endpoint;
        copy.attribute = this.attribute;
        copy.timestamp = this.timestamp;
        copy.value = this.value;
        return copy;
      }

      public LightJson.JsonObject Encode() {
        LightJson.JsonObject json = new LightJson.JsonObject();
        json["id"] = this.id;
        json["endpoint"] = this.endpoint;
        json["attribute"] = this.attribute;
        json["timestamp"] = (this.timestamp.ToUniversalTime().Ticks - new System.DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc).Ticks) / System.TimeSpan.TicksPerSecond;
        json["value"] = this.value;
        return json;
      }

      public static ClusterValue Decode(LightJson.JsonObject json, Agent agent) {
        ClusterValue inst = new ClusterValue();
        inst.id = (int)json["id"];
        inst.endpoint = (int)json["endpoint"];
        inst.attribute = (int)json["attribute"];
        inst.timestamp = new System.DateTime(json["timestamp"] * System.TimeSpan.TicksPerSecond + new System.DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc).Ticks, System.DateTimeKind.Utc).ToLocalTime();
        inst.value = (string)json["value"];
        return inst;
      }

      public int id = 0;
      public int endpoint = 0;
      public int attribute = 0;
      public System.DateTime timestamp = new System.DateTime(0);
      public string value = "";
    }

    public class ClusterDataEvent : Com.Raritan.Idl.idl.Event {
      static public readonly new TypeInfo typeInfo = new TypeInfo("zigbee.ZigbeeDevice.ClusterDataEvent:1.0.0", Com.Raritan.Idl.idl.Event.typeInfo);

      public Com.Raritan.Idl.zigbee.ZigbeeDevice.ClusterValue value = new Com.Raritan.Idl.zigbee.ZigbeeDevice.ClusterValue();
    }

    public class GetMetaDataResult {
      public Com.Raritan.Idl.zigbee.ZigbeeDevice.MetaData _ret_;
    }

    public GetMetaDataResult getMetaData() {
      JsonObject _parameters = null;
      var _result = RpcCall("getMetaData", _parameters);
      var _ret = new GetMetaDataResult();
      _ret._ret_ = Com.Raritan.Idl.zigbee.ZigbeeDevice.MetaData.Decode(_result["_ret_"], agent);
      return _ret;
    }

    public AsyncRequest getMetaData(AsyncRpcResponse<GetMetaDataResult>.SuccessHandler rsp, AsyncRpcResponse.FailureHandler fail) {
      return getMetaData(rsp, fail, RpcCtrl.Default);
    }

    public AsyncRequest getMetaData(AsyncRpcResponse<GetMetaDataResult>.SuccessHandler rsp, AsyncRpcResponse.FailureHandler fail, RpcCtrl rpcCtrl) {
      JsonObject _parameters = null;
      return RpcCall("getMetaData", _parameters,
        _result => {
          try {
            var _ret = new GetMetaDataResult();
            _ret._ret_ = Com.Raritan.Idl.zigbee.ZigbeeDevice.MetaData.Decode(_result["_ret_"], agent);
            rsp(_ret);
          } catch (Exception e) {
            if (fail != null) fail(e);
          }
        }, fail, rpcCtrl);
    }

    public class GetClusterValuesResult {
      public System.Collections.Generic.IEnumerable<Com.Raritan.Idl.zigbee.ZigbeeDevice.ClusterValue> _ret_;
    }

    public GetClusterValuesResult getClusterValues() {
      JsonObject _parameters = null;
      var _result = RpcCall("getClusterValues", _parameters);
      var _ret = new GetClusterValuesResult();
      _ret._ret_ = new System.Collections.Generic.List<Com.Raritan.Idl.zigbee.ZigbeeDevice.ClusterValue>(_result["_ret_"].AsJsonArray.Select(
        _value => Com.Raritan.Idl.zigbee.ZigbeeDevice.ClusterValue.Decode(_value, agent)));
      return _ret;
    }

    public AsyncRequest getClusterValues(AsyncRpcResponse<GetClusterValuesResult>.SuccessHandler rsp, AsyncRpcResponse.FailureHandler fail) {
      return getClusterValues(rsp, fail, RpcCtrl.Default);
    }

    public AsyncRequest getClusterValues(AsyncRpcResponse<GetClusterValuesResult>.SuccessHandler rsp, AsyncRpcResponse.FailureHandler fail, RpcCtrl rpcCtrl) {
      JsonObject _parameters = null;
      return RpcCall("getClusterValues", _parameters,
        _result => {
          try {
            var _ret = new GetClusterValuesResult();
            _ret._ret_ = new System.Collections.Generic.List<Com.Raritan.Idl.zigbee.ZigbeeDevice.ClusterValue>(_result["_ret_"].AsJsonArray.Select(
              _value => Com.Raritan.Idl.zigbee.ZigbeeDevice.ClusterValue.Decode(_value, agent)));
            rsp(_ret);
          } catch (Exception e) {
            if (fail != null) fail(e);
          }
        }, fail, rpcCtrl);
    }

  }
}
