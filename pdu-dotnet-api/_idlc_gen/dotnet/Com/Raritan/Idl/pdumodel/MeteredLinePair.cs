// SPDX-License-Identifier: BSD-3-Clause
//
// Copyright 2025 Raritan Inc. All rights reserved.
//
// This file was generated by IdlC from Pole.idl.

using System;
using System.Linq;
using LightJson;
using Com.Raritan.Idl;
using Com.Raritan.JsonRpc;
using Com.Raritan.Util;

#pragma warning disable 0108, 0219, 0414, 1591

namespace Com.Raritan.Idl.pdumodel {

  public class MeteredLinePair : ICloneable {
    public object Clone() {
      MeteredLinePair copy = new MeteredLinePair();
      copy.leftLine = this.leftLine;
      copy.rightLine = this.rightLine;
      copy.leftNodeId = this.leftNodeId;
      copy.rightNodeId = this.rightNodeId;
      copy.voltage = this.voltage;
      copy.current = this.current;
      copy.peakCurrent = this.peakCurrent;
      copy.activePower = this.activePower;
      copy.reactivePower = this.reactivePower;
      copy.apparentPower = this.apparentPower;
      copy.powerFactor = this.powerFactor;
      copy.phaseAngle = this.phaseAngle;
      copy.displacementPowerFactor = this.displacementPowerFactor;
      copy.activeEnergy = this.activeEnergy;
      copy.apparentEnergy = this.apparentEnergy;
      copy.crestFactor = this.crestFactor;
      copy.voltageThd = this.voltageThd;
      copy.currentThd = this.currentThd;
      return copy;
    }

    public LightJson.JsonObject Encode() {
      LightJson.JsonObject json = new LightJson.JsonObject();
      json["leftLine"] = (int)this.leftLine;
      json["rightLine"] = (int)this.rightLine;
      json["leftNodeId"] = this.leftNodeId;
      json["rightNodeId"] = this.rightNodeId;
      json["voltage"] = this.voltage != null ? this.voltage.Encode() : JsonValue.Null;
      json["current"] = this.current != null ? this.current.Encode() : JsonValue.Null;
      json["peakCurrent"] = this.peakCurrent != null ? this.peakCurrent.Encode() : JsonValue.Null;
      json["activePower"] = this.activePower != null ? this.activePower.Encode() : JsonValue.Null;
      json["reactivePower"] = this.reactivePower != null ? this.reactivePower.Encode() : JsonValue.Null;
      json["apparentPower"] = this.apparentPower != null ? this.apparentPower.Encode() : JsonValue.Null;
      json["powerFactor"] = this.powerFactor != null ? this.powerFactor.Encode() : JsonValue.Null;
      json["phaseAngle"] = this.phaseAngle != null ? this.phaseAngle.Encode() : JsonValue.Null;
      json["displacementPowerFactor"] = this.displacementPowerFactor != null ? this.displacementPowerFactor.Encode() : JsonValue.Null;
      json["activeEnergy"] = this.activeEnergy != null ? this.activeEnergy.Encode() : JsonValue.Null;
      json["apparentEnergy"] = this.apparentEnergy != null ? this.apparentEnergy.Encode() : JsonValue.Null;
      json["crestFactor"] = this.crestFactor != null ? this.crestFactor.Encode() : JsonValue.Null;
      json["voltageThd"] = this.voltageThd != null ? this.voltageThd.Encode() : JsonValue.Null;
      json["currentThd"] = this.currentThd != null ? this.currentThd.Encode() : JsonValue.Null;
      return json;
    }

    public static MeteredLinePair Decode(LightJson.JsonObject json, Agent agent) {
      MeteredLinePair inst = new MeteredLinePair();
      inst.leftLine = (Com.Raritan.Idl.pdumodel.PowerLine_3_0_0)(int)json["leftLine"];
      inst.rightLine = (Com.Raritan.Idl.pdumodel.PowerLine_3_0_0)(int)json["rightLine"];
      inst.leftNodeId = (int)json["leftNodeId"];
      inst.rightNodeId = (int)json["rightNodeId"];
      inst.voltage = Com.Raritan.Idl.sensors.NumericSensor_4_0_5.StaticCast(ObjectProxy.Decode(json["voltage"], agent));
      inst.current = Com.Raritan.Idl.sensors.NumericSensor_4_0_5.StaticCast(ObjectProxy.Decode(json["current"], agent));
      inst.peakCurrent = Com.Raritan.Idl.sensors.NumericSensor_4_0_5.StaticCast(ObjectProxy.Decode(json["peakCurrent"], agent));
      inst.activePower = Com.Raritan.Idl.sensors.NumericSensor_4_0_5.StaticCast(ObjectProxy.Decode(json["activePower"], agent));
      inst.reactivePower = Com.Raritan.Idl.sensors.NumericSensor_4_0_5.StaticCast(ObjectProxy.Decode(json["reactivePower"], agent));
      inst.apparentPower = Com.Raritan.Idl.sensors.NumericSensor_4_0_5.StaticCast(ObjectProxy.Decode(json["apparentPower"], agent));
      inst.powerFactor = Com.Raritan.Idl.sensors.NumericSensor_4_0_5.StaticCast(ObjectProxy.Decode(json["powerFactor"], agent));
      inst.phaseAngle = Com.Raritan.Idl.sensors.NumericSensor_4_0_5.StaticCast(ObjectProxy.Decode(json["phaseAngle"], agent));
      inst.displacementPowerFactor = Com.Raritan.Idl.sensors.NumericSensor_4_0_5.StaticCast(ObjectProxy.Decode(json["displacementPowerFactor"], agent));
      inst.activeEnergy = Com.Raritan.Idl.sensors.NumericSensor_4_0_5.StaticCast(ObjectProxy.Decode(json["activeEnergy"], agent));
      inst.apparentEnergy = Com.Raritan.Idl.sensors.NumericSensor_4_0_5.StaticCast(ObjectProxy.Decode(json["apparentEnergy"], agent));
      inst.crestFactor = Com.Raritan.Idl.sensors.NumericSensor_4_0_5.StaticCast(ObjectProxy.Decode(json["crestFactor"], agent));
      inst.voltageThd = Com.Raritan.Idl.sensors.NumericSensor_4_0_5.StaticCast(ObjectProxy.Decode(json["voltageThd"], agent));
      inst.currentThd = Com.Raritan.Idl.sensors.NumericSensor_4_0_5.StaticCast(ObjectProxy.Decode(json["currentThd"], agent));
      return inst;
    }

    public Com.Raritan.Idl.pdumodel.PowerLine_3_0_0 leftLine = Com.Raritan.Idl.pdumodel.PowerLine_3_0_0.L1;
    public Com.Raritan.Idl.pdumodel.PowerLine_3_0_0 rightLine = Com.Raritan.Idl.pdumodel.PowerLine_3_0_0.L1;
    public int leftNodeId = 0;
    public int rightNodeId = 0;
    public Com.Raritan.Idl.sensors.NumericSensor_4_0_5 voltage = null;
    public Com.Raritan.Idl.sensors.NumericSensor_4_0_5 current = null;
    public Com.Raritan.Idl.sensors.NumericSensor_4_0_5 peakCurrent = null;
    public Com.Raritan.Idl.sensors.NumericSensor_4_0_5 activePower = null;
    public Com.Raritan.Idl.sensors.NumericSensor_4_0_5 reactivePower = null;
    public Com.Raritan.Idl.sensors.NumericSensor_4_0_5 apparentPower = null;
    public Com.Raritan.Idl.sensors.NumericSensor_4_0_5 powerFactor = null;
    public Com.Raritan.Idl.sensors.NumericSensor_4_0_5 phaseAngle = null;
    public Com.Raritan.Idl.sensors.NumericSensor_4_0_5 displacementPowerFactor = null;
    public Com.Raritan.Idl.sensors.NumericSensor_4_0_5 activeEnergy = null;
    public Com.Raritan.Idl.sensors.NumericSensor_4_0_5 apparentEnergy = null;
    public Com.Raritan.Idl.sensors.NumericSensor_4_0_5 crestFactor = null;
    public Com.Raritan.Idl.sensors.NumericSensor_4_0_5 voltageThd = null;
    public Com.Raritan.Idl.sensors.NumericSensor_4_0_5 currentThd = null;
  }
}
