// SPDX-License-Identifier: BSD-3-Clause
//
// Copyright 2025 Raritan Inc. All rights reserved.
//
// This file was generated by IdlC from EDevice.idl.

using System;
using System.Linq;
using LightJson;
using Com.Raritan.Idl;
using Com.Raritan.JsonRpc;
using Com.Raritan.Util;

#pragma warning disable 0108, 0219, 0414, 1591

namespace Com.Raritan.Idl.pdumodel {
  public class EDevice_1_0_1 : ObjectProxy {

    static public readonly new TypeInfo typeInfo = new TypeInfo("pdumodel.EDevice:1.0.1", null);

    public EDevice_1_0_1(Agent agent, string rid, TypeInfo ti) : base(agent, rid, ti) {}
    public EDevice_1_0_1(Agent agent, string rid) : this(agent, rid, typeInfo) {}

    public static new EDevice_1_0_1 StaticCast(ObjectProxy proxy) {
      return proxy == null ? null : new EDevice_1_0_1(proxy.Agent, proxy.Rid, proxy.StaticTypeInfo);
    }

    public class GetParentsResult {
      public System.Collections.Generic.IEnumerable<Com.Raritan.Idl.pdumodel.EDevice_1_0_1> _ret_;
    }

    public GetParentsResult getParents() {
      JsonObject _parameters = null;
      var _result = RpcCall("getParents", _parameters);
      var _ret = new GetParentsResult();
      _ret._ret_ = new System.Collections.Generic.List<Com.Raritan.Idl.pdumodel.EDevice_1_0_1>(_result["_ret_"].AsJsonArray.Select(
        _value => Com.Raritan.Idl.pdumodel.EDevice_1_0_1.StaticCast(ObjectProxy.Decode(_value, agent))));
      return _ret;
    }

    public AsyncRequest getParents(AsyncRpcResponse<GetParentsResult>.SuccessHandler rsp, AsyncRpcResponse.FailureHandler fail) {
      return getParents(rsp, fail, RpcCtrl.Default);
    }

    public AsyncRequest getParents(AsyncRpcResponse<GetParentsResult>.SuccessHandler rsp, AsyncRpcResponse.FailureHandler fail, RpcCtrl rpcCtrl) {
      JsonObject _parameters = null;
      return RpcCall("getParents", _parameters,
        _result => {
          try {
            var _ret = new GetParentsResult();
            _ret._ret_ = new System.Collections.Generic.List<Com.Raritan.Idl.pdumodel.EDevice_1_0_1>(_result["_ret_"].AsJsonArray.Select(
              _value => Com.Raritan.Idl.pdumodel.EDevice_1_0_1.StaticCast(ObjectProxy.Decode(_value, agent))));
            rsp(_ret);
          } catch (Exception e) {
            if (fail != null) fail(e);
          }
        }, fail, rpcCtrl);
    }

    public class GetChildrenResult {
      public System.Collections.Generic.IEnumerable<Com.Raritan.Idl.pdumodel.EDevice_1_0_1> _ret_;
    }

    public GetChildrenResult getChildren() {
      JsonObject _parameters = null;
      var _result = RpcCall("getChildren", _parameters);
      var _ret = new GetChildrenResult();
      _ret._ret_ = new System.Collections.Generic.List<Com.Raritan.Idl.pdumodel.EDevice_1_0_1>(_result["_ret_"].AsJsonArray.Select(
        _value => Com.Raritan.Idl.pdumodel.EDevice_1_0_1.StaticCast(ObjectProxy.Decode(_value, agent))));
      return _ret;
    }

    public AsyncRequest getChildren(AsyncRpcResponse<GetChildrenResult>.SuccessHandler rsp, AsyncRpcResponse.FailureHandler fail) {
      return getChildren(rsp, fail, RpcCtrl.Default);
    }

    public AsyncRequest getChildren(AsyncRpcResponse<GetChildrenResult>.SuccessHandler rsp, AsyncRpcResponse.FailureHandler fail, RpcCtrl rpcCtrl) {
      JsonObject _parameters = null;
      return RpcCall("getChildren", _parameters,
        _result => {
          try {
            var _ret = new GetChildrenResult();
            _ret._ret_ = new System.Collections.Generic.List<Com.Raritan.Idl.pdumodel.EDevice_1_0_1>(_result["_ret_"].AsJsonArray.Select(
              _value => Com.Raritan.Idl.pdumodel.EDevice_1_0_1.StaticCast(ObjectProxy.Decode(_value, agent))));
            rsp(_ret);
          } catch (Exception e) {
            if (fail != null) fail(e);
          }
        }, fail, rpcCtrl);
    }

    public class GetWaveformResult {
      public Com.Raritan.Idl.pdumodel.Waveform _ret_;
    }

    public GetWaveformResult getWaveform() {
      JsonObject _parameters = null;
      var _result = RpcCall("getWaveform", _parameters);
      var _ret = new GetWaveformResult();
      _ret._ret_ = Com.Raritan.Idl.pdumodel.Waveform.Decode(_result["_ret_"], agent);
      return _ret;
    }

    public AsyncRequest getWaveform(AsyncRpcResponse<GetWaveformResult>.SuccessHandler rsp, AsyncRpcResponse.FailureHandler fail) {
      return getWaveform(rsp, fail, RpcCtrl.Default);
    }

    public AsyncRequest getWaveform(AsyncRpcResponse<GetWaveformResult>.SuccessHandler rsp, AsyncRpcResponse.FailureHandler fail, RpcCtrl rpcCtrl) {
      JsonObject _parameters = null;
      return RpcCall("getWaveform", _parameters,
        _result => {
          try {
            var _ret = new GetWaveformResult();
            _ret._ret_ = Com.Raritan.Idl.pdumodel.Waveform.Decode(_result["_ret_"], agent);
            rsp(_ret);
          } catch (Exception e) {
            if (fail != null) fail(e);
          }
        }, fail, rpcCtrl);
    }

    public class GetPoleWaveformResult {
      public Com.Raritan.Idl.pdumodel.Waveform _ret_;
    }

    public GetPoleWaveformResult getPoleWaveform(Com.Raritan.Idl.pdumodel.PowerLine_3_0_0 line) {
      var _parameters = new LightJson.JsonObject();
      _parameters["line"] = (int)line;

      var _result = RpcCall("getPoleWaveform", _parameters);
      var _ret = new GetPoleWaveformResult();
      _ret._ret_ = Com.Raritan.Idl.pdumodel.Waveform.Decode(_result["_ret_"], agent);
      return _ret;
    }

    public AsyncRequest getPoleWaveform(Com.Raritan.Idl.pdumodel.PowerLine_3_0_0 line, AsyncRpcResponse<GetPoleWaveformResult>.SuccessHandler rsp, AsyncRpcResponse.FailureHandler fail) {
      return getPoleWaveform(line, rsp, fail, RpcCtrl.Default);
    }

    public AsyncRequest getPoleWaveform(Com.Raritan.Idl.pdumodel.PowerLine_3_0_0 line, AsyncRpcResponse<GetPoleWaveformResult>.SuccessHandler rsp, AsyncRpcResponse.FailureHandler fail, RpcCtrl rpcCtrl) {
      var _parameters = new LightJson.JsonObject();
      try {
        _parameters["line"] = (int)line;
      } catch (Exception e) {
        if (fail != null) fail(e);
      }

      return RpcCall("getPoleWaveform", _parameters,
        _result => {
          try {
            var _ret = new GetPoleWaveformResult();
            _ret._ret_ = Com.Raritan.Idl.pdumodel.Waveform.Decode(_result["_ret_"], agent);
            rsp(_ret);
          } catch (Exception e) {
            if (fail != null) fail(e);
          }
        }, fail, rpcCtrl);
    }

  }
}
