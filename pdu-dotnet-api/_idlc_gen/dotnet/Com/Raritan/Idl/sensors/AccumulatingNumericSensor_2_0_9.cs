// SPDX-License-Identifier: BSD-3-Clause
//
// Copyright 2025 Raritan Inc. All rights reserved.
//
// This file was generated by IdlC from AccumulatingNumericSensor.idl.

using System;
using System.Linq;
using LightJson;
using Com.Raritan.Idl;
using Com.Raritan.JsonRpc;
using Com.Raritan.Util;

#pragma warning disable 0108, 0219, 0414, 1591

namespace Com.Raritan.Idl.sensors {
  public class AccumulatingNumericSensor_2_0_9 : Com.Raritan.Idl.sensors.NumericSensor_4_0_8 {

    static public readonly new TypeInfo typeInfo = new TypeInfo("sensors.AccumulatingNumericSensor:2.0.9", Com.Raritan.Idl.sensors.NumericSensor_4_0_8.typeInfo);

    public AccumulatingNumericSensor_2_0_9(Agent agent, string rid, TypeInfo ti) : base(agent, rid, ti) {}
    public AccumulatingNumericSensor_2_0_9(Agent agent, string rid) : this(agent, rid, typeInfo) {}

    public static new AccumulatingNumericSensor_2_0_9 StaticCast(ObjectProxy proxy) {
      return proxy == null ? null : new AccumulatingNumericSensor_2_0_9(proxy.Agent, proxy.Rid, proxy.StaticTypeInfo);
    }

    public class ResetEvent : Com.Raritan.Idl._event.UserEvent {
      static public readonly new TypeInfo typeInfo = new TypeInfo("sensors.AccumulatingNumericSensor_2_0_9.ResetEvent:1.0.0", Com.Raritan.Idl._event.UserEvent.typeInfo);

      public Com.Raritan.Idl.sensors.NumericSensor_4_0_8.Reading oldReading = new Com.Raritan.Idl.sensors.NumericSensor_4_0_8.Reading();
      public Com.Raritan.Idl.sensors.NumericSensor_4_0_8.Reading newReading = new Com.Raritan.Idl.sensors.NumericSensor_4_0_8.Reading();
    }

    public class ResetValueResult {
    }

    public ResetValueResult resetValue() {
      JsonObject _parameters = null;
      var _result = RpcCall("resetValue", _parameters);
      var _ret = new ResetValueResult();
      return _ret;
    }

    public AsyncRequest resetValue(AsyncRpcResponse<ResetValueResult>.SuccessHandler rsp, AsyncRpcResponse.FailureHandler fail) {
      return resetValue(rsp, fail, RpcCtrl.Default);
    }

    public AsyncRequest resetValue(AsyncRpcResponse<ResetValueResult>.SuccessHandler rsp, AsyncRpcResponse.FailureHandler fail, RpcCtrl rpcCtrl) {
      JsonObject _parameters = null;
      return RpcCall("resetValue", _parameters,
        _result => {
          try {
            var _ret = new ResetValueResult();
            rsp(_ret);
          } catch (Exception e) {
            if (fail != null) fail(e);
          }
        }, fail, rpcCtrl);
    }

    public class GetLastResetTimeResult {
      public System.DateTime _ret_;
    }

    public GetLastResetTimeResult getLastResetTime() {
      JsonObject _parameters = null;
      var _result = RpcCall("getLastResetTime", _parameters);
      var _ret = new GetLastResetTimeResult();
      _ret._ret_ = new System.DateTime(_result["_ret_"] * System.TimeSpan.TicksPerSecond + new System.DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc).Ticks, System.DateTimeKind.Utc).ToLocalTime();
      return _ret;
    }

    public AsyncRequest getLastResetTime(AsyncRpcResponse<GetLastResetTimeResult>.SuccessHandler rsp, AsyncRpcResponse.FailureHandler fail) {
      return getLastResetTime(rsp, fail, RpcCtrl.Default);
    }

    public AsyncRequest getLastResetTime(AsyncRpcResponse<GetLastResetTimeResult>.SuccessHandler rsp, AsyncRpcResponse.FailureHandler fail, RpcCtrl rpcCtrl) {
      JsonObject _parameters = null;
      return RpcCall("getLastResetTime", _parameters,
        _result => {
          try {
            var _ret = new GetLastResetTimeResult();
            _ret._ret_ = new System.DateTime(_result["_ret_"] * System.TimeSpan.TicksPerSecond + new System.DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc).Ticks, System.DateTimeKind.Utc).ToLocalTime();
            rsp(_ret);
          } catch (Exception e) {
            if (fail != null) fail(e);
          }
        }, fail, rpcCtrl);
    }

  }
}
