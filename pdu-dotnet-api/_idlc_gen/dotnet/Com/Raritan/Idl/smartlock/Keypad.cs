// SPDX-License-Identifier: BSD-3-Clause
//
// Copyright 2025 Raritan Inc. All rights reserved.
//
// This file was generated by IdlC from Keypad.idl.

using System;
using System.Linq;
using LightJson;
using Com.Raritan.Idl;
using Com.Raritan.JsonRpc;
using Com.Raritan.Util;

#pragma warning disable 0108, 0219, 0414, 1591

namespace Com.Raritan.Idl.smartlock {
  public class Keypad : ObjectProxy {

    static public readonly new TypeInfo typeInfo = new TypeInfo("smartlock.Keypad:1.0.0", null);

    public Keypad(Agent agent, string rid, TypeInfo ti) : base(agent, rid, ti) {}
    public Keypad(Agent agent, string rid) : this(agent, rid, typeInfo) {}

    public static new Keypad StaticCast(ObjectProxy proxy) {
      return proxy == null ? null : new Keypad(proxy.Agent, proxy.Rid, proxy.StaticTypeInfo);
    }

    public const int NO_ERROR = 0;

    public const int ERR_SLOT_EMPTY = 1;

    public class MetaData : ICloneable {
      public object Clone() {
        MetaData copy = new MetaData();
        copy.id = this.id;
        copy.manufacturer = this.manufacturer;
        copy.product = this.product;
        copy.serialNumber = this.serialNumber;
        copy.channel = this.channel;
        copy.position = this.position;
        return copy;
      }

      public LightJson.JsonObject Encode() {
        LightJson.JsonObject json = new LightJson.JsonObject();
        json["id"] = this.id;
        json["manufacturer"] = this.manufacturer;
        json["product"] = this.product;
        json["serialNumber"] = this.serialNumber;
        json["channel"] = this.channel;
        json["position"] = this.position;
        return json;
      }

      public static MetaData Decode(LightJson.JsonObject json, Agent agent) {
        MetaData inst = new MetaData();
        inst.id = (string)json["id"];
        inst.manufacturer = (string)json["manufacturer"];
        inst.product = (string)json["product"];
        inst.serialNumber = (string)json["serialNumber"];
        inst.channel = (int)json["channel"];
        inst.position = (string)json["position"];
        return inst;
      }

      public string id = "";
      public string manufacturer = "";
      public string product = "";
      public string serialNumber = "";
      public int channel = 0;
      public string position = "";
    }

    public class PINEnteredEvent : Com.Raritan.Idl.idl.Event {
      static public readonly new TypeInfo typeInfo = new TypeInfo("smartlock.Keypad.PINEnteredEvent:1.0.0", Com.Raritan.Idl.idl.Event.typeInfo);

      public Com.Raritan.Idl.smartlock.Keypad.MetaData metaData = new Com.Raritan.Idl.smartlock.Keypad.MetaData();
    }

    public class GetMetaDataResult {
      public Com.Raritan.Idl.smartlock.Keypad.MetaData _ret_;
    }

    public GetMetaDataResult getMetaData() {
      JsonObject _parameters = null;
      var _result = RpcCall("getMetaData", _parameters);
      var _ret = new GetMetaDataResult();
      _ret._ret_ = Com.Raritan.Idl.smartlock.Keypad.MetaData.Decode(_result["_ret_"], agent);
      return _ret;
    }

    public AsyncRequest getMetaData(AsyncRpcResponse<GetMetaDataResult>.SuccessHandler rsp, AsyncRpcResponse.FailureHandler fail) {
      return getMetaData(rsp, fail, RpcCtrl.Default);
    }

    public AsyncRequest getMetaData(AsyncRpcResponse<GetMetaDataResult>.SuccessHandler rsp, AsyncRpcResponse.FailureHandler fail, RpcCtrl rpcCtrl) {
      JsonObject _parameters = null;
      return RpcCall("getMetaData", _parameters,
        _result => {
          try {
            var _ret = new GetMetaDataResult();
            _ret._ret_ = Com.Raritan.Idl.smartlock.Keypad.MetaData.Decode(_result["_ret_"], agent);
            rsp(_ret);
          } catch (Exception e) {
            if (fail != null) fail(e);
          }
        }, fail, rpcCtrl);
    }

    public class GetPINResult {
      public int _ret_;
      public string pin;
    }

    public GetPINResult getPIN() {
      JsonObject _parameters = null;
      var _result = RpcCall("getPIN", _parameters);
      var _ret = new GetPINResult();
      _ret._ret_ = (int)_result["_ret_"];
      _ret.pin = (string)_result["pin"];
      return _ret;
    }

    public AsyncRequest getPIN(AsyncRpcResponse<GetPINResult>.SuccessHandler rsp, AsyncRpcResponse.FailureHandler fail) {
      return getPIN(rsp, fail, RpcCtrl.Default);
    }

    public AsyncRequest getPIN(AsyncRpcResponse<GetPINResult>.SuccessHandler rsp, AsyncRpcResponse.FailureHandler fail, RpcCtrl rpcCtrl) {
      JsonObject _parameters = null;
      return RpcCall("getPIN", _parameters,
        _result => {
          try {
            var _ret = new GetPINResult();
            _ret._ret_ = (int)_result["_ret_"];
            _ret.pin = (string)_result["pin"];
            rsp(_ret);
          } catch (Exception e) {
            if (fail != null) fail(e);
          }
        }, fail, rpcCtrl);
    }

  }
}
