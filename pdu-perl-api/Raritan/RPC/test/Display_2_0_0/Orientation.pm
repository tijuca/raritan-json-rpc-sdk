# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright 2025 Raritan Inc. All rights reserved.
#
# This file was generated by IdlC from TestDisplay.idl.

use strict;

package Raritan::RPC::test::Display_2_0_0::Orientation;

use constant NORMAL => 0;
use constant FLIPPED => 1;
use constant LEFT => 2;
use constant RIGHT => 3;

1;
