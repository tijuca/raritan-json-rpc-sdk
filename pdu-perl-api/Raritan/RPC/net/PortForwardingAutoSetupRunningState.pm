# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright 2025 Raritan Inc. All rights reserved.
#
# This file was generated by IdlC from PortForwardingAutoSetup.idl.

use strict;

package Raritan::RPC::net::PortForwardingAutoSetupRunningState;

use constant PF_AUTO_SETUP_NONE => 0;
use constant PF_AUTO_SETUP_RUNNING => 1;
use constant PF_AUTO_SETUP_CANCELED => 2;
use constant PF_AUTO_SETUP_FINISHED_WITH_ERROR => 3;
use constant PF_AUTO_SETUP_FINISHED_SUCCESSFULLY => 4;

1;
