# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright 2025 Raritan Inc. All rights reserved.
#
# This file was generated by IdlC from RadiusManager.idl.

use strict;

package Raritan::RPC::auth::RadiusManager_4_0_0::AuthType;

use constant PAP => 0;
use constant CHAP => 1;
use constant MSCHAPv2 => 2;

1;
