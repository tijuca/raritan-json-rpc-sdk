# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright 2025 Raritan Inc. All rights reserved.
#
# This file was generated by IdlC from RadiusManager.idl.

use strict;

package Raritan::RPC::auth::RadiusManager_3_0_1::ServerSettings;


sub encode {
    my ($in) = @_;
    my $encoded = {};
    $encoded->{'id'} = "$in->{'id'}";
    $encoded->{'server'} = "$in->{'server'}";
    $encoded->{'sharedSecret'} = "$in->{'sharedSecret'}";
    $encoded->{'udpAuthPort'} = 1 * $in->{'udpAuthPort'};
    $encoded->{'udpAccountPort'} = 1 * $in->{'udpAccountPort'};
    $encoded->{'timeout'} = 1 * $in->{'timeout'};
    $encoded->{'retries'} = 1 * $in->{'retries'};
    $encoded->{'authType'} = 1 * $in->{'authType'};
    $encoded->{'disableAccounting'} = ($in->{'disableAccounting'}) ? JSON::true : JSON::false;
    return $encoded;
}

sub decode {
    my ($agent, $in) = @_;
    my $decoded = {};
    $decoded->{'id'} = $in->{'id'};
    $decoded->{'server'} = $in->{'server'};
    $decoded->{'sharedSecret'} = $in->{'sharedSecret'};
    $decoded->{'udpAuthPort'} = $in->{'udpAuthPort'};
    $decoded->{'udpAccountPort'} = $in->{'udpAccountPort'};
    $decoded->{'timeout'} = $in->{'timeout'};
    $decoded->{'retries'} = $in->{'retries'};
    $decoded->{'authType'} = $in->{'authType'};
    $decoded->{'disableAccounting'} = ($in->{'disableAccounting'}) ? 1 : 0;
    return $decoded;
}

1;
