# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright 2025 Raritan Inc. All rights reserved.
#
# This file was generated by IdlC from LdapManager.idl.

use strict;

package Raritan::RPC::auth::LdapManager_3_0_1::ServerSettings;


sub encode {
    my ($in) = @_;
    my $encoded = {};
    $encoded->{'id'} = "$in->{'id'}";
    $encoded->{'server'} = "$in->{'server'}";
    $encoded->{'adoptSettingsId'} = "$in->{'adoptSettingsId'}";
    $encoded->{'type'} = 1 * $in->{'type'};
    $encoded->{'secProto'} = 1 * $in->{'secProto'};
    $encoded->{'port'} = 1 * $in->{'port'};
    $encoded->{'sslPort'} = 1 * $in->{'sslPort'};
    $encoded->{'forceTrustedCert'} = ($in->{'forceTrustedCert'}) ? JSON::true : JSON::false;
    $encoded->{'allowOffTimeRangeCerts'} = ($in->{'allowOffTimeRangeCerts'}) ? JSON::true : JSON::false;
    $encoded->{'certificate'} = "$in->{'certificate'}";
    $encoded->{'adsDomain'} = "$in->{'adsDomain'}";
    $encoded->{'useAnonymousBind'} = ($in->{'useAnonymousBind'}) ? JSON::true : JSON::false;
    $encoded->{'bindDN'} = "$in->{'bindDN'}";
    $encoded->{'bindPwd'} = "$in->{'bindPwd'}";
    $encoded->{'searchBaseDN'} = "$in->{'searchBaseDN'}";
    $encoded->{'loginNameAttr'} = "$in->{'loginNameAttr'}";
    $encoded->{'userEntryObjClass'} = "$in->{'userEntryObjClass'}";
    $encoded->{'userSearchFilter'} = "$in->{'userSearchFilter'}";
    $encoded->{'groupInfoInUserEntry'} = ($in->{'groupInfoInUserEntry'}) ? JSON::true : JSON::false;
    $encoded->{'groupMemberAttr'} = "$in->{'groupMemberAttr'}";
    $encoded->{'groupEntryObjClass'} = "$in->{'groupEntryObjClass'}";
    $encoded->{'groupSearchFilter'} = "$in->{'groupSearchFilter'}";
    return $encoded;
}

sub decode {
    my ($agent, $in) = @_;
    my $decoded = {};
    $decoded->{'id'} = $in->{'id'};
    $decoded->{'server'} = $in->{'server'};
    $decoded->{'adoptSettingsId'} = $in->{'adoptSettingsId'};
    $decoded->{'type'} = $in->{'type'};
    $decoded->{'secProto'} = $in->{'secProto'};
    $decoded->{'port'} = $in->{'port'};
    $decoded->{'sslPort'} = $in->{'sslPort'};
    $decoded->{'forceTrustedCert'} = ($in->{'forceTrustedCert'}) ? 1 : 0;
    $decoded->{'allowOffTimeRangeCerts'} = ($in->{'allowOffTimeRangeCerts'}) ? 1 : 0;
    $decoded->{'certificate'} = $in->{'certificate'};
    $decoded->{'adsDomain'} = $in->{'adsDomain'};
    $decoded->{'useAnonymousBind'} = ($in->{'useAnonymousBind'}) ? 1 : 0;
    $decoded->{'bindDN'} = $in->{'bindDN'};
    $decoded->{'bindPwd'} = $in->{'bindPwd'};
    $decoded->{'searchBaseDN'} = $in->{'searchBaseDN'};
    $decoded->{'loginNameAttr'} = $in->{'loginNameAttr'};
    $decoded->{'userEntryObjClass'} = $in->{'userEntryObjClass'};
    $decoded->{'userSearchFilter'} = $in->{'userSearchFilter'};
    $decoded->{'groupInfoInUserEntry'} = ($in->{'groupInfoInUserEntry'}) ? 1 : 0;
    $decoded->{'groupMemberAttr'} = $in->{'groupMemberAttr'};
    $decoded->{'groupEntryObjClass'} = $in->{'groupEntryObjClass'};
    $decoded->{'groupSearchFilter'} = $in->{'groupSearchFilter'};
    return $decoded;
}

1;
