# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright 2025 Raritan Inc. All rights reserved.
#
# This file was generated by IdlC from KeypadManager.idl.

use strict;

package Raritan::RPC::smartlock::KeypadManager_1_0_1::KeypadSettingsChangedEvent;

use constant typeId => "smartlock.KeypadManager_1_0_1.KeypadSettingsChangedEvent:1.0.0";
use Raritan::RPC::smartlock::KeypadManager_1_0_1::KeypadSettings;
use Raritan::RPC::event::UserEvent;
use Raritan::RPC::smartlock::KeypadManager_1_0_1::KeypadSettings;

sub encode {
    my ($in) = @_;
    my $encoded = Raritan::RPC::event::UserEvent::encode($in);
    $encoded->{'keypad'} = Raritan::RPC::ObjectCodec::encode($in->{'keypad'});
    $encoded->{'oldSettings'} = Raritan::RPC::smartlock::KeypadManager_1_0_1::KeypadSettings::encode($in->{'oldSettings'});
    $encoded->{'newSettings'} = Raritan::RPC::smartlock::KeypadManager_1_0_1::KeypadSettings::encode($in->{'newSettings'});
    $encoded->{'position'} = "$in->{'position'}";
    return $encoded;
}

sub decode {
    my ($agent, $in) = @_;
    my $decoded = Raritan::RPC::event::UserEvent::decode($agent, $in);
    $decoded->{'keypad'} = Raritan::RPC::ObjectCodec::decode($agent, $in->{'keypad'}, 'smartlock.Keypad');
    $decoded->{'oldSettings'} = Raritan::RPC::smartlock::KeypadManager_1_0_1::KeypadSettings::decode($agent, $in->{'oldSettings'});
    $decoded->{'newSettings'} = Raritan::RPC::smartlock::KeypadManager_1_0_1::KeypadSettings::decode($agent, $in->{'newSettings'});
    $decoded->{'position'} = $in->{'position'};
    return $decoded;
}

Raritan::RPC::Registry::registerCodecClass('smartlock.KeypadManager_1_0_1.KeypadSettingsChangedEvent', 1, 0, 0, 'Raritan::RPC::smartlock::KeypadManager_1_0_1::KeypadSettingsChangedEvent');
1;
