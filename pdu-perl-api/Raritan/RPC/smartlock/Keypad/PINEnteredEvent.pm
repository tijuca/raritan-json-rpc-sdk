# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright 2025 Raritan Inc. All rights reserved.
#
# This file was generated by IdlC from Keypad.idl.

use strict;

package Raritan::RPC::smartlock::Keypad::PINEnteredEvent;

use constant typeId => "smartlock.Keypad.PINEnteredEvent:1.0.0";
use Raritan::RPC::idl::Event;
use Raritan::RPC::smartlock::Keypad::MetaData;

sub encode {
    my ($in) = @_;
    my $encoded = Raritan::RPC::idl::Event::encode($in);
    $encoded->{'metaData'} = Raritan::RPC::smartlock::Keypad::MetaData::encode($in->{'metaData'});
    return $encoded;
}

sub decode {
    my ($agent, $in) = @_;
    my $decoded = Raritan::RPC::idl::Event::decode($agent, $in);
    $decoded->{'metaData'} = Raritan::RPC::smartlock::Keypad::MetaData::decode($agent, $in->{'metaData'});
    return $decoded;
}

Raritan::RPC::Registry::registerCodecClass('smartlock.Keypad.PINEnteredEvent', 1, 0, 0, 'Raritan::RPC::smartlock::Keypad::PINEnteredEvent');
1;
