# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright 2025 Raritan Inc. All rights reserved.
#
# This file was generated by IdlC from KeypadManager.idl.

use strict;

package Raritan::RPC::smartlock::KeypadManager::KeypadSettingsChangedEvent;

use constant typeId => "smartlock.KeypadManager.KeypadSettingsChangedEvent:1.0.0";
use Raritan::RPC::smartlock::KeypadManager::KeypadSettings;
use Raritan::RPC::event::UserEvent;
use Raritan::RPC::smartlock::KeypadManager::KeypadSettings;

sub encode {
    my ($in) = @_;
    my $encoded = Raritan::RPC::event::UserEvent::encode($in);
    $encoded->{'oldSettings'} = Raritan::RPC::smartlock::KeypadManager::KeypadSettings::encode($in->{'oldSettings'});
    $encoded->{'newSettings'} = Raritan::RPC::smartlock::KeypadManager::KeypadSettings::encode($in->{'newSettings'});
    $encoded->{'position'} = "$in->{'position'}";
    return $encoded;
}

sub decode {
    my ($agent, $in) = @_;
    my $decoded = Raritan::RPC::event::UserEvent::decode($agent, $in);
    $decoded->{'oldSettings'} = Raritan::RPC::smartlock::KeypadManager::KeypadSettings::decode($agent, $in->{'oldSettings'});
    $decoded->{'newSettings'} = Raritan::RPC::smartlock::KeypadManager::KeypadSettings::decode($agent, $in->{'newSettings'});
    $decoded->{'position'} = $in->{'position'};
    return $decoded;
}

Raritan::RPC::Registry::registerCodecClass('smartlock.KeypadManager.KeypadSettingsChangedEvent', 1, 0, 0, 'Raritan::RPC::smartlock::KeypadManager::KeypadSettingsChangedEvent');
1;
