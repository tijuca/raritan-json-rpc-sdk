# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright 2025 Raritan Inc. All rights reserved.
#
# This file was generated by IdlC from DoorAccessControl.idl.

use strict;

package Raritan::RPC::smartlock::DoorAccessControl_1_1_2::DoorAccessGrantedEvent;

use constant typeId => "smartlock.DoorAccessControl_1_1_2.DoorAccessGrantedEvent:1.0.0";
use Raritan::RPC::idl::Event;
use Raritan::RPC::smartlock::DoorAccessControl_1_1_2::DoorAccessRule;

sub encode {
    my ($in) = @_;
    my $encoded = Raritan::RPC::idl::Event::encode($in);
    $encoded->{'ruleId'} = 1 * $in->{'ruleId'};
    $encoded->{'rule'} = Raritan::RPC::smartlock::DoorAccessControl_1_1_2::DoorAccessRule::encode($in->{'rule'});
    return $encoded;
}

sub decode {
    my ($agent, $in) = @_;
    my $decoded = Raritan::RPC::idl::Event::decode($agent, $in);
    $decoded->{'ruleId'} = $in->{'ruleId'};
    $decoded->{'rule'} = Raritan::RPC::smartlock::DoorAccessControl_1_1_2::DoorAccessRule::decode($agent, $in->{'rule'});
    return $decoded;
}

Raritan::RPC::Registry::registerCodecClass('smartlock.DoorAccessControl_1_1_2.DoorAccessGrantedEvent', 1, 0, 0, 'Raritan::RPC::smartlock::DoorAccessControl_1_1_2::DoorAccessGrantedEvent');
1;
