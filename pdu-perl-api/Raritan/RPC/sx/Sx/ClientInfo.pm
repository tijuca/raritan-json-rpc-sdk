# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright 2025 Raritan Inc. All rights reserved.
#
# This file was generated by IdlC from Sx.idl.

use strict;

package Raritan::RPC::sx::Sx::ClientInfo;

sub encode {
    my ($in) = @_;
    my $encoded = {};
    $encoded->{'portId'} = "$in->{'portId'}";
    $encoded->{'portName'} = "$in->{'portName'}";
    $encoded->{'userName'} = "$in->{'userName'}";
    $encoded->{'userIp'} = "$in->{'userIp'}";
    return $encoded;
}

sub decode {
    my ($agent, $in) = @_;
    my $decoded = {};
    $decoded->{'portId'} = $in->{'portId'};
    $decoded->{'portName'} = $in->{'portName'};
    $decoded->{'userName'} = $in->{'userName'};
    $decoded->{'userIp'} = $in->{'userIp'};
    return $decoded;
}

1;
