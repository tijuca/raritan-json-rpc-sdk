# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright 2025 Raritan Inc. All rights reserved.
#
# This file was generated by IdlC from GatewaySensorManager.idl.

use strict;

package Raritan::RPC::peripheral::GatewaySensorManager::FeedbackChangedEvent;

use constant typeId => "peripheral.GatewaySensorManager.FeedbackChangedEvent:1.0.0";
use Raritan::RPC::idl::Event;
use Raritan::RPC::peripheral::GatewaySensorManager::Feedback;

sub encode {
    my ($in) = @_;
    my $encoded = Raritan::RPC::idl::Event::encode($in);
    $encoded->{'feedback'} = [];
    for (my $i0 = 0; $i0 <= $#{$in->{'feedback'}}; $i0++) {
        $encoded->{'feedback'}->[$i0] = Raritan::RPC::ValObjCodec::encode($in->{'feedback'}->[$i0]);
    }
    return $encoded;
}

sub decode {
    my ($agent, $in) = @_;
    my $decoded = Raritan::RPC::idl::Event::decode($agent, $in);
    $decoded->{'feedback'} = [];
    for (my $i0 = 0; $i0 <= $#{$in->{'feedback'}}; $i0++) {
        $decoded->{'feedback'}->[$i0] = Raritan::RPC::ValObjCodec::decode($agent, $in->{'feedback'}->[$i0], 'peripheral.GatewaySensorManager.Feedback');
    }
    return $decoded;
}

Raritan::RPC::Registry::registerCodecClass('peripheral.GatewaySensorManager.FeedbackChangedEvent', 1, 0, 0, 'Raritan::RPC::peripheral::GatewaySensorManager::FeedbackChangedEvent');
1;
