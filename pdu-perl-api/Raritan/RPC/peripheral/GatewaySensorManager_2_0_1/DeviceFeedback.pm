# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright 2025 Raritan Inc. All rights reserved.
#
# This file was generated by IdlC from GatewaySensorManager.idl.

use strict;

package Raritan::RPC::peripheral::GatewaySensorManager_2_0_1::DeviceFeedback;

use constant typeId => "peripheral.GatewaySensorManager_2_0_1.DeviceFeedback:1.0.0";
use Raritan::RPC::peripheral::GatewaySensorManager_2_0_1::Feedback;

sub encode {
    my ($in) = @_;
    my $encoded = Raritan::RPC::peripheral::GatewaySensorManager_2_0_1::Feedback::encode($in);
    $encoded->{'packageId'} = "$in->{'packageId'}";
    $encoded->{'deviceId'} = "$in->{'deviceId'}";
    return $encoded;
}

sub decode {
    my ($agent, $in) = @_;
    my $decoded = Raritan::RPC::peripheral::GatewaySensorManager_2_0_1::Feedback::decode($agent, $in);
    $decoded->{'packageId'} = $in->{'packageId'};
    $decoded->{'deviceId'} = $in->{'deviceId'};
    return $decoded;
}

Raritan::RPC::Registry::registerCodecClass('peripheral.GatewaySensorManager_2_0_1.DeviceFeedback', 1, 0, 0, 'Raritan::RPC::peripheral::GatewaySensorManager_2_0_1::DeviceFeedback');
1;
