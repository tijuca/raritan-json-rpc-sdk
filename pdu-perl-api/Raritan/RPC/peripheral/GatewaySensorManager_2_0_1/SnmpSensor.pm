# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright 2025 Raritan Inc. All rights reserved.
#
# This file was generated by IdlC from GatewaySensorManager.idl.

use strict;

package Raritan::RPC::peripheral::GatewaySensorManager_2_0_1::SnmpSensor;

use constant typeId => "peripheral.GatewaySensorManager_2_0_1.SnmpSensor:1.0.0";
use Raritan::RPC::peripheral::GatewaySensorManager_2_0_1::Sensor;

sub encode {
    my ($in) = @_;
    my $encoded = Raritan::RPC::peripheral::GatewaySensorManager_2_0_1::Sensor::encode($in);
    $encoded->{'oid'} = "$in->{'oid'}";
    return $encoded;
}

sub decode {
    my ($agent, $in) = @_;
    my $decoded = Raritan::RPC::peripheral::GatewaySensorManager_2_0_1::Sensor::decode($agent, $in);
    $decoded->{'oid'} = $in->{'oid'};
    return $decoded;
}

Raritan::RPC::Registry::registerCodecClass('peripheral.GatewaySensorManager_2_0_1.SnmpSensor', 1, 0, 0, 'Raritan::RPC::peripheral::GatewaySensorManager_2_0_1::SnmpSensor');
1;
