# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright 2025 Raritan Inc. All rights reserved.
#
# This file was generated by IdlC from GatewaySensorManager.idl.

use strict;

package Raritan::RPC::peripheral::GatewaySensorManager_2_0_1::InterpretationRuleRangeRAW;

use constant typeId => "peripheral.GatewaySensorManager_2_0_1.InterpretationRuleRangeRAW:1.0.0";
use Raritan::RPC::peripheral::GatewaySensorManager_2_0_1::InterpretationRuleInvertable;

sub encode {
    my ($in) = @_;
    my $encoded = Raritan::RPC::peripheral::GatewaySensorManager_2_0_1::InterpretationRuleInvertable::encode($in);
    $encoded->{'min'} = 1 * $in->{'min'};
    $encoded->{'max'} = 1 * $in->{'max'};
    $encoded->{'mask'} = 1 * $in->{'mask'};
    return $encoded;
}

sub decode {
    my ($agent, $in) = @_;
    my $decoded = Raritan::RPC::peripheral::GatewaySensorManager_2_0_1::InterpretationRuleInvertable::decode($agent, $in);
    $decoded->{'min'} = $in->{'min'};
    $decoded->{'max'} = $in->{'max'};
    $decoded->{'mask'} = $in->{'mask'};
    return $decoded;
}

Raritan::RPC::Registry::registerCodecClass('peripheral.GatewaySensorManager_2_0_1.InterpretationRuleRangeRAW', 1, 0, 0, 'Raritan::RPC::peripheral::GatewaySensorManager_2_0_1::InterpretationRuleRangeRAW');
1;
