# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright 2025 Raritan Inc. All rights reserved.
#
# This file was generated by IdlC from GatewaySensorManager.idl.

use strict;

package Raritan::RPC::peripheral::GatewaySensorManager_2_0_1::NumericSensorClass;

use constant typeId => "peripheral.GatewaySensorManager_2_0_1.NumericSensorClass:1.0.0";
use Raritan::RPC::sensors::NumericSensor_4_0_8::MetaData;
use Raritan::RPC::peripheral::GatewaySensorManager_2_0_1::SensorClass;
use Raritan::RPC::sensors::NumericSensor_4_0_8::Thresholds;

sub encode {
    my ($in) = @_;
    my $encoded = Raritan::RPC::peripheral::GatewaySensorManager_2_0_1::SensorClass::encode($in);
    $encoded->{'metadata'} = Raritan::RPC::sensors::NumericSensor_4_0_8::MetaData::encode($in->{'metadata'});
    $encoded->{'defaultThresholds'} = Raritan::RPC::sensors::NumericSensor_4_0_8::Thresholds::encode($in->{'defaultThresholds'});
    $encoded->{'preferCommonThresholds'} = ($in->{'preferCommonThresholds'}) ? JSON::true : JSON::false;
    return $encoded;
}

sub decode {
    my ($agent, $in) = @_;
    my $decoded = Raritan::RPC::peripheral::GatewaySensorManager_2_0_1::SensorClass::decode($agent, $in);
    $decoded->{'metadata'} = Raritan::RPC::sensors::NumericSensor_4_0_8::MetaData::decode($agent, $in->{'metadata'});
    $decoded->{'defaultThresholds'} = Raritan::RPC::sensors::NumericSensor_4_0_8::Thresholds::decode($agent, $in->{'defaultThresholds'});
    $decoded->{'preferCommonThresholds'} = ($in->{'preferCommonThresholds'}) ? 1 : 0;
    return $decoded;
}

Raritan::RPC::Registry::registerCodecClass('peripheral.GatewaySensorManager_2_0_1.NumericSensorClass', 1, 0, 0, 'Raritan::RPC::peripheral::GatewaySensorManager_2_0_1::NumericSensorClass');
1;
