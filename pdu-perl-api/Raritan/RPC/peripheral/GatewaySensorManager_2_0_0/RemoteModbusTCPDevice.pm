# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright 2025 Raritan Inc. All rights reserved.
#
# This file was generated by IdlC from GatewaySensorManager.idl.

use strict;

package Raritan::RPC::peripheral::GatewaySensorManager_2_0_0::RemoteModbusTCPDevice;

use constant typeId => "peripheral.GatewaySensorManager_2_0_0.RemoteModbusTCPDevice:1.0.0";
use Raritan::RPC::peripheral::GatewaySensorManager_2_0_0::RemoteModbusDevice;

sub encode {
    my ($in) = @_;
    my $encoded = Raritan::RPC::peripheral::GatewaySensorManager_2_0_0::RemoteModbusDevice::encode($in);
    $encoded->{'ipAddress'} = "$in->{'ipAddress'}";
    $encoded->{'tcpPort'} = 1 * $in->{'tcpPort'};
    return $encoded;
}

sub decode {
    my ($agent, $in) = @_;
    my $decoded = Raritan::RPC::peripheral::GatewaySensorManager_2_0_0::RemoteModbusDevice::decode($agent, $in);
    $decoded->{'ipAddress'} = $in->{'ipAddress'};
    $decoded->{'tcpPort'} = $in->{'tcpPort'};
    return $decoded;
}

Raritan::RPC::Registry::registerCodecClass('peripheral.GatewaySensorManager_2_0_0.RemoteModbusTCPDevice', 1, 0, 0, 'Raritan::RPC::peripheral::GatewaySensorManager_2_0_0::RemoteModbusTCPDevice');
1;
