# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright 2025 Raritan Inc. All rights reserved.
#
# This file was generated by IdlC from PeripheralDevicePackage.idl.

use strict;

package Raritan::RPC::peripheral::BatteryPoweredDevicePackage_1_0_1::VoltageChangedEvent;

use constant typeId => "peripheral.BatteryPoweredDevicePackage_1_0_1.VoltageChangedEvent:1.0.0";
use Raritan::RPC::idl::Event;

sub encode {
    my ($in) = @_;
    my $encoded = Raritan::RPC::idl::Event::encode($in);
    $encoded->{'oldVoltage'} = 1 * $in->{'oldVoltage'};
    $encoded->{'newVoltage'} = 1 * $in->{'newVoltage'};
    return $encoded;
}

sub decode {
    my ($agent, $in) = @_;
    my $decoded = Raritan::RPC::idl::Event::decode($agent, $in);
    $decoded->{'oldVoltage'} = $in->{'oldVoltage'};
    $decoded->{'newVoltage'} = $in->{'newVoltage'};
    return $decoded;
}

Raritan::RPC::Registry::registerCodecClass('peripheral.BatteryPoweredDevicePackage_1_0_1.VoltageChangedEvent', 1, 0, 0, 'Raritan::RPC::peripheral::BatteryPoweredDevicePackage_1_0_1::VoltageChangedEvent');
1;
