# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright 2025 Raritan Inc. All rights reserved.
#
# This file was generated by IdlC from GatewaySensorManager.idl.

use strict;

package Raritan::RPC::peripheral::GatewaySensorManager_3_0_0::InterpretationRuleModbusSystemError;

use constant typeId => "peripheral.GatewaySensorManager_3_0_0.InterpretationRuleModbusSystemError:1.0.0";
use Raritan::RPC::peripheral::GatewaySensorManager_3_0_0::InterpretationRuleInvertable;

sub encode {
    my ($in) = @_;
    my $encoded = Raritan::RPC::peripheral::GatewaySensorManager_3_0_0::InterpretationRuleInvertable::encode($in);
    $encoded->{'errnos'} = [];
    for (my $i0 = 0; $i0 <= $#{$in->{'errnos'}}; $i0++) {
        $encoded->{'errnos'}->[$i0] = 1 * $in->{'errnos'}->[$i0];
    }
    return $encoded;
}

sub decode {
    my ($agent, $in) = @_;
    my $decoded = Raritan::RPC::peripheral::GatewaySensorManager_3_0_0::InterpretationRuleInvertable::decode($agent, $in);
    $decoded->{'errnos'} = [];
    for (my $i0 = 0; $i0 <= $#{$in->{'errnos'}}; $i0++) {
        $decoded->{'errnos'}->[$i0] = $in->{'errnos'}->[$i0];
    }
    return $decoded;
}

Raritan::RPC::Registry::registerCodecClass('peripheral.GatewaySensorManager_3_0_0.InterpretationRuleModbusSystemError', 1, 0, 0, 'Raritan::RPC::peripheral::GatewaySensorManager_3_0_0::InterpretationRuleModbusSystemError');
1;
