# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright 2025 Raritan Inc. All rights reserved.
#
# This file was generated by IdlC from GatewaySensorManager.idl.

use strict;

package Raritan::RPC::peripheral::GatewaySensorManager_3_0_0::Sensor;

use constant typeId => "peripheral.GatewaySensorManager_3_0_0.Sensor:1.0.0";
sub encode {
    my ($in) = @_;
    my $encoded = {};
    $encoded->{'sensorId'} = "$in->{'sensorId'}";
    $encoded->{'disabled'} = ($in->{'disabled'}) ? JSON::true : JSON::false;
    $encoded->{'deviceId'} = "$in->{'deviceId'}";
    $encoded->{'classId'} = "$in->{'classId'}";
    $encoded->{'encodingId'} = "$in->{'encodingId'}";
    $encoded->{'defaultName'} = "$in->{'defaultName'}";
    $encoded->{'defaultDescription'} = "$in->{'defaultDescription'}";
    $encoded->{'defaultLocationX'} = "$in->{'defaultLocationX'}";
    $encoded->{'defaultLocationY'} = "$in->{'defaultLocationY'}";
    $encoded->{'defaultLocationZ'} = "$in->{'defaultLocationZ'}";
    return $encoded;
}

sub decode {
    my ($agent, $in) = @_;
    my $decoded = {};
    $decoded->{'sensorId'} = $in->{'sensorId'};
    $decoded->{'disabled'} = ($in->{'disabled'}) ? 1 : 0;
    $decoded->{'deviceId'} = $in->{'deviceId'};
    $decoded->{'classId'} = $in->{'classId'};
    $decoded->{'encodingId'} = $in->{'encodingId'};
    $decoded->{'defaultName'} = $in->{'defaultName'};
    $decoded->{'defaultDescription'} = $in->{'defaultDescription'};
    $decoded->{'defaultLocationX'} = $in->{'defaultLocationX'};
    $decoded->{'defaultLocationY'} = $in->{'defaultLocationY'};
    $decoded->{'defaultLocationZ'} = $in->{'defaultLocationZ'};
    return $decoded;
}

Raritan::RPC::Registry::registerCodecClass('peripheral.GatewaySensorManager_3_0_0.Sensor', 1, 0, 0, 'Raritan::RPC::peripheral::GatewaySensorManager_3_0_0::Sensor');
1;
