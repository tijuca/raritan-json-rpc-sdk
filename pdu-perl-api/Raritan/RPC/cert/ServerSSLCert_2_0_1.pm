# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright 2025 Raritan Inc. All rights reserved.
#
# This file was generated by IdlC from ServerSSLCert.idl.

use strict;

package Raritan::RPC::cert::ServerSSLCert_2_0_1;

use parent qw(Raritan::RPC::RemoteObject);

use constant typeId => "cert.ServerSSLCert:2.0.1";

sub new {
    my ($class, $agent, $rid, $typeId) = @_;
    $typeId = $typeId || Raritan::RPC::cert::ServerSSLCert_2_0_1::typeId;
    return $class->SUPER::new($agent, $rid, $typeId);
}

use constant SUCCESS => 0;

use constant ERR_GEN_KEY_LEN_INVALID => 100;

use constant ERR_GEN_CSR_OR_CERT_PENDING => 101;

use constant ERR_GEN_KEY_GEN_FAILED => 102;

use constant ERR_INSTALL_KEY_MISSING => 200;

use constant ERR_INSTALL_CERT_MISSING => 201;

use constant ERR_INSTALL_CERT_FORMAT_INVALID => 202;

use constant ERR_INSTALL_CERT_KEY_MISMATCH => 203;

use Raritan::RPC::cert::ServerSSLCert_2_0_1::ReqInfo;

sub generateUnsignedKeyPair($$$) {
    my ($self, $reqInfo, $challenge) = @_;
    my $agent = $self->{'agent'};
    my $args = {};
    $args->{'reqInfo'} = Raritan::RPC::cert::ServerSSLCert_2_0_1::ReqInfo::encode($reqInfo);
    $args->{'challenge'} = "$challenge";
    my $rsp = $agent->json_rpc($self->{'rid'}, 'generateUnsignedKeyPair', $args);
    my $_ret_;
    $_ret_ = $rsp->{'_ret_'};
    return $_ret_;
}

use Raritan::RPC::cert::ServerSSLCert_2_0_1::ReqInfo;

sub generateSelfSignedKeyPair($$$) {
    my ($self, $reqInfo, $days) = @_;
    my $agent = $self->{'agent'};
    my $args = {};
    $args->{'reqInfo'} = Raritan::RPC::cert::ServerSSLCert_2_0_1::ReqInfo::encode($reqInfo);
    $args->{'days'} = 1 * $days;
    my $rsp = $agent->json_rpc($self->{'rid'}, 'generateSelfSignedKeyPair', $args);
    my $_ret_;
    $_ret_ = $rsp->{'_ret_'};
    return $_ret_;
}

sub deletePending($) {
    my ($self) = @_;
    my $agent = $self->{'agent'};
    my $args = {};
    my $rsp = $agent->json_rpc($self->{'rid'}, 'deletePending', $args);
}

use Raritan::RPC::cert::ServerSSLCert_2_0_1::Info;

sub getInfo($$) {
    my ($self, $info) = @_;
    my $agent = $self->{'agent'};
    my $args = {};
    my $rsp = $agent->json_rpc($self->{'rid'}, 'getInfo', $args);
    $$info = Raritan::RPC::cert::ServerSSLCert_2_0_1::Info::decode($agent, $rsp->{'info'});
}

sub installPendingKeyPair($) {
    my ($self) = @_;
    my $agent = $self->{'agent'};
    my $args = {};
    my $rsp = $agent->json_rpc($self->{'rid'}, 'installPendingKeyPair', $args);
    my $_ret_;
    $_ret_ = $rsp->{'_ret_'};
    return $_ret_;
}

Raritan::RPC::Registry::registerProxyClass('cert.ServerSSLCert', 2, 0, 1, 'Raritan::RPC::cert::ServerSSLCert_2_0_1');
1;
