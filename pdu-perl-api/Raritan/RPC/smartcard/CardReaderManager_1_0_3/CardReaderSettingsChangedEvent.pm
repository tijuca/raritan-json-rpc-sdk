# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright 2025 Raritan Inc. All rights reserved.
#
# This file was generated by IdlC from CardReaderManager.idl.

use strict;

package Raritan::RPC::smartcard::CardReaderManager_1_0_3::CardReaderSettingsChangedEvent;

use constant typeId => "smartcard.CardReaderManager_1_0_3.CardReaderSettingsChangedEvent:1.0.0";
use Raritan::RPC::smartcard::CardReaderManager_1_0_3::CardReaderSettings;
use Raritan::RPC::smartcard::CardReaderManager_1_0_3::CardReaderSettings;
use Raritan::RPC::event::UserEvent;

sub encode {
    my ($in) = @_;
    my $encoded = Raritan::RPC::event::UserEvent::encode($in);
    $encoded->{'oldSettings'} = Raritan::RPC::smartcard::CardReaderManager_1_0_3::CardReaderSettings::encode($in->{'oldSettings'});
    $encoded->{'newSettings'} = Raritan::RPC::smartcard::CardReaderManager_1_0_3::CardReaderSettings::encode($in->{'newSettings'});
    $encoded->{'position'} = "$in->{'position'}";
    return $encoded;
}

sub decode {
    my ($agent, $in) = @_;
    my $decoded = Raritan::RPC::event::UserEvent::decode($agent, $in);
    $decoded->{'oldSettings'} = Raritan::RPC::smartcard::CardReaderManager_1_0_3::CardReaderSettings::decode($agent, $in->{'oldSettings'});
    $decoded->{'newSettings'} = Raritan::RPC::smartcard::CardReaderManager_1_0_3::CardReaderSettings::decode($agent, $in->{'newSettings'});
    $decoded->{'position'} = $in->{'position'};
    return $decoded;
}

Raritan::RPC::Registry::registerCodecClass('smartcard.CardReaderManager_1_0_3.CardReaderSettingsChangedEvent', 1, 0, 0, 'Raritan::RPC::smartcard::CardReaderManager_1_0_3::CardReaderSettingsChangedEvent');
1;
