# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright 2025 Raritan Inc. All rights reserved.
#
# This file was generated by IdlC from CardReader.idl.

use strict;

package Raritan::RPC::smartcard::CardReader_1_0_4::CardEvent_2_0_0;

use constant typeId => "smartcard.CardReader_1_0_4.CardEvent:2.0.0";
use Raritan::RPC::idl::Event;
use Raritan::RPC::smartcard::CardReader_1_0_4::MetaData;

sub encode {
    my ($in) = @_;
    my $encoded = Raritan::RPC::idl::Event::encode($in);
    $encoded->{'metaData'} = Raritan::RPC::smartcard::CardReader_1_0_4::MetaData::encode($in->{'metaData'});
    return $encoded;
}

sub decode {
    my ($agent, $in) = @_;
    my $decoded = Raritan::RPC::idl::Event::decode($agent, $in);
    $decoded->{'metaData'} = Raritan::RPC::smartcard::CardReader_1_0_4::MetaData::decode($agent, $in->{'metaData'});
    return $decoded;
}

Raritan::RPC::Registry::registerCodecClass('smartcard.CardReader_1_0_4.CardEvent', 2, 0, 0, 'Raritan::RPC::smartcard::CardReader_1_0_4::CardEvent_2_0_0');
1;
