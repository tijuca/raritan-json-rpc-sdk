# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright 2025 Raritan Inc. All rights reserved.
#
# This file was generated by IdlC from Port.idl.

use strict;

package Raritan::RPC::portsmodel::Port_2_0_4::DeviceTypeId;

use constant UNSPECIFIED => 0;
use constant OTHER => 1;
use constant SENSOR_HUB => 2;
use constant BEEPER => 3;
use constant ASSET_STRIP => 4;
use constant POWER_CIM => 5;
use constant GATEWAY_SENSOR => 6;

1;
