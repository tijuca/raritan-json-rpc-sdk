# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright 2025 Raritan Inc. All rights reserved.
#
# This file was generated by IdlC from AssetStrip.idl.

use strict;

package Raritan::RPC::assetmgrmodel::AssetStrip::StripSettings;

use Raritan::RPC::assetmgrmodel::AssetStrip::LEDColor;
use Raritan::RPC::assetmgrmodel::AssetStrip::LEDColor;

sub encode {
    my ($in) = @_;
    my $encoded = {};
    $encoded->{'channelCount'} = 1 * $in->{'channelCount'};
    $encoded->{'name'} = "$in->{'name'}";
    $encoded->{'scanMode'} = 1 * $in->{'scanMode'};
    $encoded->{'defaultColorConnected'} = Raritan::RPC::assetmgrmodel::AssetStrip::LEDColor::encode($in->{'defaultColorConnected'});
    $encoded->{'defaultColorDisconnected'} = Raritan::RPC::assetmgrmodel::AssetStrip::LEDColor::encode($in->{'defaultColorDisconnected'});
    $encoded->{'numberingMode'} = 1 * $in->{'numberingMode'};
    $encoded->{'numberingOffset'} = 1 * $in->{'numberingOffset'};
    $encoded->{'orientationSensAvailable'} = ($in->{'orientationSensAvailable'}) ? JSON::true : JSON::false;
    $encoded->{'orientation'} = 1 * $in->{'orientation'};
    return $encoded;
}

sub decode {
    my ($agent, $in) = @_;
    my $decoded = {};
    $decoded->{'channelCount'} = $in->{'channelCount'};
    $decoded->{'name'} = $in->{'name'};
    $decoded->{'scanMode'} = $in->{'scanMode'};
    $decoded->{'defaultColorConnected'} = Raritan::RPC::assetmgrmodel::AssetStrip::LEDColor::decode($agent, $in->{'defaultColorConnected'});
    $decoded->{'defaultColorDisconnected'} = Raritan::RPC::assetmgrmodel::AssetStrip::LEDColor::decode($agent, $in->{'defaultColorDisconnected'});
    $decoded->{'numberingMode'} = $in->{'numberingMode'};
    $decoded->{'numberingOffset'} = $in->{'numberingOffset'};
    $decoded->{'orientationSensAvailable'} = ($in->{'orientationSensAvailable'}) ? 1 : 0;
    $decoded->{'orientation'} = $in->{'orientation'};
    return $decoded;
}

1;
