# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright 2025 Raritan Inc. All rights reserved.
#
# This file was generated by IdlC from SensorLogger.idl.

use strict;

package Raritan::RPC::sensors::Logger_3_0_0::Info;

sub encode {
    my ($in) = @_;
    my $encoded = {};
    $encoded->{'samplePeriod'} = 1 * $in->{'samplePeriod'};
    $encoded->{'maxTotalRecords'} = 1 * $in->{'maxTotalRecords'};
    $encoded->{'effectiveCapacity'} = 1 * $in->{'effectiveCapacity'};
    $encoded->{'oldestRecId'} = 1 * $in->{'oldestRecId'};
    $encoded->{'newestRecId'} = 1 * $in->{'newestRecId'};
    return $encoded;
}

sub decode {
    my ($agent, $in) = @_;
    my $decoded = {};
    $decoded->{'samplePeriod'} = $in->{'samplePeriod'};
    $decoded->{'maxTotalRecords'} = $in->{'maxTotalRecords'};
    $decoded->{'effectiveCapacity'} = $in->{'effectiveCapacity'};
    $decoded->{'oldestRecId'} = $in->{'oldestRecId'};
    $decoded->{'newestRecId'} = $in->{'newestRecId'};
    return $decoded;
}

1;
