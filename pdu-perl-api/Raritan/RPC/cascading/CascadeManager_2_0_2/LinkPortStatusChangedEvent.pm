# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright 2025 Raritan Inc. All rights reserved.
#
# This file was generated by IdlC from CascadeManager.idl.

use strict;

package Raritan::RPC::cascading::CascadeManager_2_0_2::LinkPortStatusChangedEvent;

use constant typeId => "cascading.CascadeManager_2_0_2.LinkPortStatusChangedEvent:1.0.0";
use Raritan::RPC::cascading::CascadeManager_2_0_2::LinkPortStatus;
use Raritan::RPC::cascading::CascadeManager_2_0_2::LinkPortStatus;
use Raritan::RPC::idl::Event;

sub encode {
    my ($in) = @_;
    my $encoded = Raritan::RPC::idl::Event::encode($in);
    $encoded->{'oldStatus'} = Raritan::RPC::cascading::CascadeManager_2_0_2::LinkPortStatus::encode($in->{'oldStatus'});
    $encoded->{'newStatus'} = Raritan::RPC::cascading::CascadeManager_2_0_2::LinkPortStatus::encode($in->{'newStatus'});
    return $encoded;
}

sub decode {
    my ($agent, $in) = @_;
    my $decoded = Raritan::RPC::idl::Event::decode($agent, $in);
    $decoded->{'oldStatus'} = Raritan::RPC::cascading::CascadeManager_2_0_2::LinkPortStatus::decode($agent, $in->{'oldStatus'});
    $decoded->{'newStatus'} = Raritan::RPC::cascading::CascadeManager_2_0_2::LinkPortStatus::decode($agent, $in->{'newStatus'});
    return $decoded;
}

Raritan::RPC::Registry::registerCodecClass('cascading.CascadeManager_2_0_2.LinkPortStatusChangedEvent', 1, 0, 0, 'Raritan::RPC::cascading::CascadeManager_2_0_2::LinkPortStatusChangedEvent');
1;
