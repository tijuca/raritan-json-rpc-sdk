# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright 2025 Raritan Inc. All rights reserved.
#
# This file was generated by IdlC from CascadeManager.idl.

use strict;

package Raritan::RPC::cascading::CascadeManager_2_0_0::PrimaryUnitSettings;

sub encode {
    my ($in) = @_;
    my $encoded = {};
    $encoded->{'caCertChain'} = "$in->{'caCertChain'}";
    $encoded->{'allowOffTimeRangeCerts'} = ($in->{'allowOffTimeRangeCerts'}) ? JSON::true : JSON::false;
    return $encoded;
}

sub decode {
    my ($agent, $in) = @_;
    my $decoded = {};
    $decoded->{'caCertChain'} = $in->{'caCertChain'};
    $decoded->{'allowOffTimeRangeCerts'} = ($in->{'allowOffTimeRangeCerts'}) ? 1 : 0;
    return $decoded;
}

1;
