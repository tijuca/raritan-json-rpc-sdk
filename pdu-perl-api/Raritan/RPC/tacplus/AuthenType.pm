# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright 2025 Raritan Inc. All rights reserved.
#
# This file was generated by IdlC from TacPlusServerSettings.idl.

use strict;

package Raritan::RPC::tacplus::AuthenType;

use constant ASCII => 0;
use constant PAP => 1;
use constant CHAP => 2;
use constant MSCHAP => 3;

1;
