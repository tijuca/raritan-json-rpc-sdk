# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright 2025 Raritan Inc. All rights reserved.
#
# This file was generated by IdlC from User.idl.

use strict;

package Raritan::RPC::usermgmt::SnmpV3SecLevel;

use constant NO_AUTH_NO_PRIV => 0;
use constant AUTH_NO_PRIV => 1;
use constant AUTH_PRIV => 2;

1;
