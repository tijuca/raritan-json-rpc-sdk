# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright 2025 Raritan Inc. All rights reserved.
#
# This file was generated by IdlC from DataPushService.idl.

use strict;

package Raritan::RPC::event::DataPushService_1_0_2::EntryStatusChangedEvent;

use constant typeId => "event.DataPushService_1_0_2.EntryStatusChangedEvent:1.0.0";
use Raritan::RPC::event::DataPushService_1_0_2::EntryStatus;
use Raritan::RPC::idl::Event;

sub encode {
    my ($in) = @_;
    my $encoded = Raritan::RPC::idl::Event::encode($in);
    $encoded->{'entryId'} = 1 * $in->{'entryId'};
    $encoded->{'newStatus'} = Raritan::RPC::event::DataPushService_1_0_2::EntryStatus::encode($in->{'newStatus'});
    return $encoded;
}

sub decode {
    my ($agent, $in) = @_;
    my $decoded = Raritan::RPC::idl::Event::decode($agent, $in);
    $decoded->{'entryId'} = $in->{'entryId'};
    $decoded->{'newStatus'} = Raritan::RPC::event::DataPushService_1_0_2::EntryStatus::decode($agent, $in->{'newStatus'});
    return $decoded;
}

Raritan::RPC::Registry::registerCodecClass('event.DataPushService_1_0_2.EntryStatusChangedEvent', 1, 0, 0, 'Raritan::RPC::event::DataPushService_1_0_2::EntryStatusChangedEvent');
1;
