# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright 2025 Raritan Inc. All rights reserved.
#
# This file was generated by IdlC from TransferSwitchBypassStateSensor.idl.

use strict;

package Raritan::RPC::pdumodel::TransferSwitchBypassStateSensor;

use parent qw(Raritan::RPC::sensors::StateSensor_4_0_7);

use constant typeId => "pdumodel.TransferSwitchBypassStateSensor:1.0.0";

sub new {
    my ($class, $agent, $rid, $typeId) = @_;
    $typeId = $typeId || Raritan::RPC::pdumodel::TransferSwitchBypassStateSensor::typeId;
    return $class->SUPER::new($agent, $rid, $typeId);
}

use constant STATE_SELECTED_INLET_MASK => 0x7;

use constant STATE_SELECTED_INLET_IS_ACTIVE => 0x8;

use constant STATE_FAULT => 0x10;

sub acknowledgeFault($) {
    my ($self) = @_;
    my $agent = $self->{'agent'};
    my $args = {};
    my $rsp = $agent->json_rpc($self->{'rid'}, 'acknowledgeFault', $args);
}

Raritan::RPC::Registry::registerProxyClass('pdumodel.TransferSwitchBypassStateSensor', 1, 0, 0, 'Raritan::RPC::pdumodel::TransferSwitchBypassStateSensor');
1;
