# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright 2025 Raritan Inc. All rights reserved.
#
# This file was generated by IdlC from VoltageMonitoringSensor.idl.

use strict;

package Raritan::RPC::pdumodel::VoltageMonitoringSensor_1_0_1;

use parent qw(Raritan::RPC::sensors::NumericSensor_4_0_6);

use constant typeId => "pdumodel.VoltageMonitoringSensor:1.0.1";

sub new {
    my ($class, $agent, $rid, $typeId) = @_;
    $typeId = $typeId || Raritan::RPC::pdumodel::VoltageMonitoringSensor_1_0_1::typeId;
    return $class->SUPER::new($agent, $rid, $typeId);
}

use Raritan::RPC::pdumodel::VoltageMonitoringSensor_1_0_1::Event;

sub getRecentEvents($) {
    my ($self) = @_;
    my $agent = $self->{'agent'};
    my $args = {};
    my $rsp = $agent->json_rpc($self->{'rid'}, 'getRecentEvents', $args);
    my $_ret_;
    $_ret_ = [];
    for (my $i0 = 0; $i0 <= $#{$rsp->{'_ret_'}}; $i0++) {
        $_ret_->[$i0] = Raritan::RPC::pdumodel::VoltageMonitoringSensor_1_0_1::Event::decode($agent, $rsp->{'_ret_'}->[$i0]);
    }
    return $_ret_;
}

sub clearRecentEvents($) {
    my ($self) = @_;
    my $agent = $self->{'agent'};
    my $args = {};
    my $rsp = $agent->json_rpc($self->{'rid'}, 'clearRecentEvents', $args);
}

use Raritan::RPC::pdumodel::VoltageMonitoringSensor_1_0_1::DipSwellThresholds;

sub getDipSwellThresholds($) {
    my ($self) = @_;
    my $agent = $self->{'agent'};
    my $args = {};
    my $rsp = $agent->json_rpc($self->{'rid'}, 'getDipSwellThresholds', $args);
    my $_ret_;
    $_ret_ = Raritan::RPC::pdumodel::VoltageMonitoringSensor_1_0_1::DipSwellThresholds::decode($agent, $rsp->{'_ret_'});
    return $_ret_;
}

use Raritan::RPC::pdumodel::VoltageMonitoringSensor_1_0_1::DipSwellThresholds;

sub setDipSwellThresholds($$) {
    my ($self, $thresholds) = @_;
    my $agent = $self->{'agent'};
    my $args = {};
    $args->{'thresholds'} = Raritan::RPC::pdumodel::VoltageMonitoringSensor_1_0_1::DipSwellThresholds::encode($thresholds);
    my $rsp = $agent->json_rpc($self->{'rid'}, 'setDipSwellThresholds', $args);
    my $_ret_;
    $_ret_ = $rsp->{'_ret_'};
    return $_ret_;
}

Raritan::RPC::Registry::registerProxyClass('pdumodel.VoltageMonitoringSensor', 1, 0, 1, 'Raritan::RPC::pdumodel::VoltageMonitoringSensor_1_0_1');
1;
